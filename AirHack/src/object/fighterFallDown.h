/*
# fighterFallDown.h

- 戦闘機クラス(撃墜された)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_FIGHTER_FALL_DOWN_H_
#define AIR_HACK_FIGHTER_FALL_DOWN_H_

#include "math/quaternion.h"
#include "math/vector3.h"

class Fighter;
class FighterFallDown
{
public:
    // @brief  : コンストラクタ
    // @param  : 戦闘機
    //--------------------------------------------------------------------
    FighterFallDown(const Fighter *const);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~FighterFallDown();

    // @brief  : 更新
    // @param  : 親(Fighter)
    //--------------------------------------------------------------------
    void Update(Fighter *);

private:
    // 定数
    //--------------------------------------------------------------------
    static const float      ADD_FALL_TIME;  // 落下時間
    static const Quaternion FALL_ROT;       // 落下方向
    static const float      FALL_LENGTH;    // 落下距離
    static const Quaternion ROLL_ADD;       // 回転量

    // メンバ変数
    //--------------------------------------------------------------------
    Vector3    m_PosOld;    // 落下始め位置
    Quaternion m_RotOld;    // 落下始め回転
    Quaternion m_RotZ;      // ローカルZ軸回転
    float      m_Time;      // 残り時間

    // @brief  : 回転の更新
    //--------------------------------------------------------------------
    void UpdateRotation(Fighter *);

    // @brief  : 位置の更新
    //--------------------------------------------------------------------
    void UpdatePosition(Fighter *);

};



#endif //  AIR_HACK_FIGHTER_FALL_DOWN_H_
/******************************************************************************
|   End of File
*******************************************************************************/