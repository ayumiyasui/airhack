/*
# scene.h

- シーンクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_SCENE_H_
#define AIR_HACK_SCENE_H_

#include "base/object.h"
#include "fade.h"

class Scene : public Object
{
public:
    // 定数
    //--------------------------------------------------------------------
    static const Color    FADE_COLOR;
    static const unsigned FADE_TIME;

    // @brief  : シーン切り替え
    //--------------------------------------------------------------------
    template < typename T > void ChengeNext( void )
    {
        if(IsNextScene()) return;
        m_Fade->FadeOut(FADE_COLOR,FADE_TIME);
        m_Next = new T(dynamic_cast<Object *>(Parent()),m_Fade);
    };

    // @brief  : フェード中か？
    //--------------------------------------------------------------------
    bool IsFade( void );

    // @brief  : シーン切り替え中か？
    //--------------------------------------------------------------------
    bool IsNextScene( void );

protected:
    // @brief  : コンストラクタ
    // @param  : 親オブジェクト
    //         : フェード
    //--------------------------------------------------------------------
    Scene( Object *,Fade * );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Scene();

    // @brief  : フェードし終わる && 次のシーンにいくなら処理
    //--------------------------------------------------------------------
    void UpdateScene( void );

private:
    // メンバ変数
    //--------------------------------------------------------------------
    Fade   *m_Fade;       // フェード
    Scene  *m_Next;       // 次のシーン

    // @brief  : Enableになった時の初期化
    //--------------------------------------------------------------------
    virtual void OnEnable( void ) = 0;

    // @brief  : 更新
    //--------------------------------------------------------------------
    virtual void Update( void )  = 0;
};


template < class T > class SceneWakeUp : public Object
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    SceneWakeUp() :
    Object(&TempNodeSingleton<Object>::Instance(),true)
    {
        m_Fade  = new Fade;
        m_Scene = new T(new Object(&TempNodeSingleton<Object>::Instance()), m_Fade);
    };
    
private:
    // メンバ変数
    //--------------------------------------------------------------------
    Scene *m_Scene; // シーン
    Fade  *m_Fade;  // フェード
    
    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~SceneWakeUp(){};
    
    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update( void ) override
    {
        m_Scene->IsUpdate = true;
        m_Fade->FadeIn(Scene::FADE_COLOR,Scene::FADE_TIME);
        this->IsDestroy = true;
    };
};



#endif //  AIR_HACK_SCENE_H_
/******************************************************************************
|   End of File
*******************************************************************************/