/*
# followNotRot.h

- リプレイ再生クラス - 注視点のみを追いかけ、視点は固定

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "followNotRot.h"
#include "../fighter.h"
#include "../../helper/drawLayerManager.h"
#include "renderer/3d/camera.h"
#include "math/math.h"

namespace play_replay {

// @brief  : コンストラクタ
// @param  : 戦闘機
//         : ローカル視点
//         : ローカル注視点
//         : 線形補間の割合
//--------------------------------------------------------------------
FollowNotRot::FollowNotRot(const Fighter *const _fighter, const Vector3 &_eye, const Vector3 &_at, float _t) :
    EYE(_eye),
    AT(_at),
    LEAP_TIME(_t)
{
    auto camera = DrawLayerManager::Instance().GetCamera();
    auto &mtx   = _fighter->Matrix();

    camera->Up  = Vector3::UP;

    camera->Near = 10.0f;
    camera->Far  = 100000.0f;
    camera->FovY = Math::PI / 3.0f;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
FollowNotRot::~FollowNotRot()
{
}

// @brief  : 更新
//--------------------------------------------------------------------
void FollowNotRot::Update(const PlayReplay *const _replay)
{
    auto  player     = _replay->GetPlayer();
    auto &player_pos = player->Position();
    auto camera = DrawLayerManager::Instance().GetCamera();
    Vector3 eye = camera->Eye;

    eye = Math::Leap(eye,player_pos+EYE,LEAP_TIME);

    // 適用
    camera->At  = player_pos+AT * Matrix3D(player->Rotation());
    camera->Eye = eye;
}



};    // namespace play_replay
/******************************************************************************
|   End of File
*******************************************************************************/