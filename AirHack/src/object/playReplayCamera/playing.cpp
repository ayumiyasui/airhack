/*
# playing.cpp

- リプレイ再生クラス - プレイカメラ再現クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "playing.h"
#include "../../game/playerCamera.h"
#include "../fighter.h"
#include "../../helper/drawLayerManager.h"
#include "renderer/3d/camera.h"
#include "math/math.h"

namespace play_replay {

// @brief  : コンストラクタ
// @param  : 戦闘機
//--------------------------------------------------------------------
Playing::Playing(const Fighter *const _fighter) :
    Follow(_fighter,PlayerCameraTPS::CAMERA_EYE,PlayerCameraTPS::CAMERA_AT,0.25f)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Playing::~Playing()
{
}



};    // namespace play_replay
/******************************************************************************
|   End of File
*******************************************************************************/