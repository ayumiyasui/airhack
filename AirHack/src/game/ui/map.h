/*
# map.h

- ゲーム中の広域マップクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_GAME_UI_MAP_H_
#define AIR_HACK_GAME_UI_MAP_H_

#include <list>
#include <string>
#include "renderer/2d/draw2d.h"
#include "base/object.h"
#include "math/vector2.h"
#include "math/quad.h"
#include "base/color.h"
class Game;
class World;
class Fighter;

namespace _2d {
    class Transform;
}; // namespace 2d

namespace _3d {
    class ITransform;
}; // namespace 3d

namespace ui {
// Mapクラス
//-----------------------------------------------------------------------------
class Map : public Object
{
public:
    // 内部クラス
    //--------------------------------------------------------------------
    class Icon : public Object
    {
    public:
        // @brief  : コンストラクタ
        // @param  : アイコンの色
        //         : 戦闘機(親クラス)
        //--------------------------------------------------------------------
        Icon(const Color &,Fighter *);

    private:
        // メンバ変数
        //--------------------------------------------------------------------
        const Fighter *const m_Tracked; // 追跡するfighter
              _2d::Transform  *      m_Icon;    // アイコン

        // @brief  : デストラクタ
        //--------------------------------------------------------------------
        ~Icon();

        // @brief  : 更新
        //--------------------------------------------------------------------
        void Update(void) override;

        // @brief  : 左上を0.0f,右下を1.0fとしたときの位置
        //--------------------------------------------------------------------
        Vector2 CreateWorldPosPar(void) const;
    };

    // @brief  : コンストラクタ
    // @param  : 親クラス(ゲームシーン)
    //--------------------------------------------------------------------
    Map(Game *);

private:
    // 定数
    //--------------------------------------------------------------------
    static const Vector2     BACK_POS;              // 背景の位置
    static const Quad        BACK_SIZE;             // 背景の大きさ
    static const std::string BACK_TEXTURE_NAME;     // 背景のテクスチャ名
    static const Quad        FIGHTER_SIZE;          // 戦闘機の大きさ
    static const std::string FIGHTER_TEXTURE_NAME;  // 戦闘機のテクスチャ名

    // メンバ変数
    //--------------------------------------------------------------------
    _2d::Transform  *m_BackGround;   // 背景

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Map();

};

};  // namespace ui
#endif //  AIR_HACK_GAME_UI_MAP_H_
/******************************************************************************
|   End of File
*******************************************************************************/