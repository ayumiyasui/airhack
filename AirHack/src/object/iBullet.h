/*
# iBullet.h

- 機銃の弾クラス(抽象)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_I_BULLET_H_
#define AIR_HACK_I_BULLET_H_

#include "base/object.h"
#include "variable/bitName.h"
#include "math/vector3.h"

class World;
class IBullet : public Object
{
protected:
    // @brief  : コンストラクタ
    // @param  : 撃った戦闘機ID
    //         : ワールド
    //--------------------------------------------------------------------
    IBullet(material::variable::u16, World *);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IBullet();

    // @brief  : 戦闘機との当たり判定
    // @param  : 自分の前の位置
    //         : 自分の現在の位置
    // @return : 当たった
    // @note   : 派生先Update内で使用。
    //--------------------------------------------------------------------
    bool CheakHitFighter(const Vector3 &, const Vector3 &) const;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    material::variable::u16 m_ShooterID;    // 撃った戦闘機のID
};



#endif //  AIR_HACK_I_BULLET_H_
/******************************************************************************
|   End of File
*******************************************************************************/