/*
# None.h

- ミサイル無しクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_MISSLE_NONE_H_
#define AIR_HACK_MISSLE_NONE_H_

#include "iMissle.h"
#include "iMissleLauncher.h"

namespace missle {
class NoneLauncher : public IMissleLauncher
{
public:
    // @brief  : コンストラクタ
    // @param  : 撃った戦闘機ID
    //         : 親クラス
    //--------------------------------------------------------------------
    NoneLauncher(const material::variable::u16 _id,World *_parent) :
        IMissleLauncher(_id,_parent){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~NoneLauncher(){};

    // @brief  : 撃つ
    // @param  : 初期位置
    //         : 方向
    //--------------------------------------------------------------------
    void Shoot(const Vector3 &_start, const Vector3 &_vector) override{};
};



};  // namespace missle
#endif //  AIR_HACK_MISSLE_NONE_H_
/******************************************************************************
|   End of File
*******************************************************************************/