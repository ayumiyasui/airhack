/*
# uiTime.h

- ゲーム中のタイムクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "time.h"

#include "../game.h"
#include "../../helper/drawLayerManager.h"
#include "../../helper/number.h"
#include "../../data/data.h"
#include "../../data/manager.h"
#include "../../data/score.h"
#include "app/app.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "renderer/2d/transfrom.h"
#include "renderer/2d/sprite2d.h"


namespace ui {
// 定数
//--------------------------------------------------------------------
const std::string Time::LABEL_TEXTURE_FILE("data/TEXTURE/time.tga");
const std::string Time::NUMBER_TEXTURE_FILE("data/TEXTURE/number.tga");
const Vector2     Time::LABEL_POS(20.0f,40.0f);
const Vector2     Time::LABEL_SIZE(181.0f*0.8f,24.0f*0.8f);
const Vector2     Time::NUMBER_SIZE((LABEL_SIZE.y*(80.0f/128.0f))*2.0f * 1.4f,LABEL_SIZE.y * 1.4f);
const Vector2     Time::NUMBER_MINUTE_POS( 5.0f + LABEL_POS.x + 80.0f, LABEL_POS.y+NUMBER_SIZE.y*0.4f);
const Vector2     Time::NUMBER_SECOND_POS(NUMBER_MINUTE_POS.x + 38.0f,NUMBER_MINUTE_POS.y);
const Vector2     Time::NUMBER_COMMA_POS(NUMBER_SECOND_POS.x + 38.0f,NUMBER_SECOND_POS.y);

// @brief  : コンストラクタ
// @param  : ゲーム時間
//         : Gameクラス(親クラス)
//--------------------------------------------------------------------
Time::Time(const int &_time,Game *_parent) :
    Object(_parent,true),
    m_Label(nullptr),
    m_Minute(nullptr),
    m_Second(nullptr),
    m_Comma(nullptr),
    m_GameTime(_time)
{
    auto factory  = App::Instance().Renderer()->Factory2D();
    auto layer = DrawLayerManager::Instance().Get2DLayer();
    const Color col_ui(0.0f,1.0f,0.0f,0.8f);

    // ラベルの作成
    m_Label = factory->CreateTransform(layer,true);
    m_Label->Position = LABEL_POS;
    auto sprite = factory->CreateSprite(
        Quad(0.0f,LABEL_SIZE.x,0.0f,LABEL_SIZE.y),
        Quad((1.0f/LABEL_SIZE.x)*0.5f, 1.0f, (1.0f/LABEL_SIZE.y)*0.5f, 1.0f),
        LABEL_TEXTURE_FILE,
        m_Label,true);
    sprite->PolygonColor = col_ui;

    // 数字の作成
    m_Minute = new DigitWithNumbers(NUMBER_TEXTURE_FILE,
        Quad(-NUMBER_SIZE.x*0.5f,NUMBER_SIZE.x*0.5f,-NUMBER_SIZE.y*0.5f,NUMBER_SIZE.y*0.5f),
        2,
        layer,true);
    m_Minute->Transform()->Position = NUMBER_MINUTE_POS;
    m_Minute->SetColor(col_ui);

    m_Second = new DigitWithNumbers(NUMBER_TEXTURE_FILE,
        Quad(-NUMBER_SIZE.x*0.5f,NUMBER_SIZE.x*0.5f,-NUMBER_SIZE.y*0.5f,NUMBER_SIZE.y*0.5f),
        2,
        layer,true);
    m_Second->Transform()->Position = NUMBER_SECOND_POS;
    m_Second->SetColor(col_ui);

    m_Comma = new DigitWithNumbers(NUMBER_TEXTURE_FILE,
        Quad(-NUMBER_SIZE.x*0.5f,NUMBER_SIZE.x*0.5f,-NUMBER_SIZE.y*0.5f,NUMBER_SIZE.y*0.5f),
        2,
        layer,true);
    m_Comma->Transform()->Position = NUMBER_COMMA_POS;
    m_Comma->SetColor(col_ui);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Time::~Time()
{
    m_Label->IsDestroy  = true;
    m_Minute->IsDestroy = true;
    m_Second->IsDestroy = true;
    m_Comma->IsDestroy = true;
}

// @brief  : ゲーム終了したか？
//--------------------------------------------------------------------
bool Time::GameIsEnd(void) const
{
    return m_GameTime < 0;
}

// @brief  : 更新
//--------------------------------------------------------------------
void Time::Update(void)
{
    // 時間を引く
    if(GameIsEnd()) return ;

    --m_GameTime;
    auto time = m_GameTime;

    // 分
    {
        auto i = (60*60);
        auto minute = time / i;
        m_Minute->Set(minute);
        time %= i;
    }

    // 秒
    {
        auto sec = time / 60;
        m_Second->Set(sec);
        time %= 60;
    }

    // コンマ
    {
        auto comma = (int)(time / 60.0f * 100.0f) ;
        m_Comma->Set(comma);
    }
}



};  // namespace ui
/******************************************************************************
|   End of File
*******************************************************************************/