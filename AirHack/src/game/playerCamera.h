/*
# playerCamera.h

- プレイヤーのカメラクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef _PLAYER_CAMERA_H_
#define _PLAYER_CAMERA_H_

#include "base/object.h"
#include "math/vector3.h"
#include "renderer/3d/camera.h"
#include "math/quaternion.h"
#include "player.h"

NAMESPACE_3D
    class Camera;
END_NAMESPACE_3D

class IPlayerCamera : public Object
{
protected:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //--------------------------------------------------------------------
    IPlayerCamera( Player * );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~IPlayerCamera();
};


// ３人称視点
class PlayerCameraTPS : public IPlayerCamera
{
public:
    // 定数
    //--------------------------------------------------------------------
    static const Vector3 CAMERA_AT;     // ローカルカメラ注視点
    static const Vector3 CAMERA_EYE;    // ローカルカメラ視点

    // @brief  : コンストラクタ
    // @param  : 親クラス
    //--------------------------------------------------------------------
    PlayerCameraTPS( Player * );

private:
    // メンバ変数
    //--------------------------------------------------------------------
    _3d::Camera *m_Camera;         // カメラクラス
    Quaternion   m_NowRotation;    // 現在のカメラ回転

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~PlayerCameraTPS();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(void) override;
};



#endif //  _PLAYER_CAMERA_STATE_H_
/******************************************************************************
|   End of File
*******************************************************************************/