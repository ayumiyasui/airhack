/*
# followAt.cpp

- リプレイ再生クラス - 注視点のみを追いかけ、視点は固定

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "followAt.h"
#include "../fighter.h"
#include "../../helper/drawLayerManager.h"
#include "renderer/3d/camera.h"
#include "math/math.h"

namespace play_replay {

// @brief  : コンストラクタ
// @param  : 戦闘機
//         : ローカル視点
//         : ローカル注視点
//         : 球面補完の割合
//--------------------------------------------------------------------
FollowAt::FollowAt(const Fighter *const _fighter, const Vector3 &_eye, const Vector3 &_at, float _t) :
    EYE(_eye),
    AT(_at),
    LEAP_TIME(_t)
{
    auto camera = DrawLayerManager::Instance().GetCamera();
    auto &mtx   = _fighter->Matrix();

    camera->Eye = EYE;
    camera->At  = AT * mtx;
    camera->Up  = Vector3::UP;

    camera->Near = 10.0f;
    camera->Far  = 100000.0f;
    camera->FovY = Math::PI / 3.0f;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
FollowAt::~FollowAt()
{
}

// @brief  : 更新
//--------------------------------------------------------------------
void FollowAt::Update(const PlayReplay *const _replay)
{
    auto  player     = _replay->GetPlayer();
    auto &player_pos = player->Position();
    auto camera = DrawLayerManager::Instance().GetCamera();
    Vector3 at = camera->At;

    at = Math::Leap(at,player_pos+AT,LEAP_TIME);

    // 適用
    camera->At  = at;
}




};    // namespace play_replay
/******************************************************************************
|   End of File
*******************************************************************************/