/*
# result.cpp

- リザルトシーンクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "result.h"

#include "app/app.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "renderer/2d/transfrom.h"
#include "renderer/2d/sprite2d.h"
#include "../helper/drawLayerManager.h"
#include "input/inputKeyboard.h"
#include "../title/title.h"
#include "drawScore.h"
#include "../data/manager.h"
#include "../data/dataKeyConfig.h"
#include "../object/playReplayCamera/overlookPlayer.h"


// 定数
//--------------------------------------------------------------------
const int         Result::WAIT_TIME         = 60 * 3;
const int         Result::GO_TO_RESULT_TIME = WAIT_TIME + 60 * 10;

// @brief  : コンストラクタ
// @param  : 親オブジェクト
//         : フェード
//--------------------------------------------------------------------
Result::Result( Object *_obj, Fade *_fade ) :
    Scene(_obj,_fade),
    m_GoToResultTime(GO_TO_RESULT_TIME),
    m_WaitTime(WAIT_TIME),
    m_KeyConfig(data::Manager::Instance().Get<data::KeyConfig>())
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Result::~Result()
{
    m_BGM->Stop();
}

// @brief  : Enableになった時の初期化
//--------------------------------------------------------------------
void Result::OnEnable( void )
{
    auto app   = &App::Instance();
    auto audio = app->Audio();

    m_Replay = new PlayReplay(this,new play_replay::OverLookPlayer());
    m_GoToResultTime = GO_TO_RESULT_TIME;   // 強制的にタイトルに戻るまでの時間
    m_WaitTime       = WAIT_TIME;           // 入力ができるようになるまでの時間

    new result::DrawScore(this);

    // BGM
    m_BGM = audio->LoadBGM("data/SOUND/result.wav");
    m_BGM->Play();
}

// @brief  : 更新
//--------------------------------------------------------------------
void Result::Update( void )
{
    App::Instance().Renderer()->Print("<<リザルトモード>>");

    // カウントを減らす
    //if(--m_GoToResultTime < 0) ChengeNext<Title>();
    //if(--m_WaitTime < 0)
    //{
        if(m_KeyConfig->Is(data::KeyConfig::OK))
        {
            ChengeNext<Title>();
        }
    //}
    //else
    //{
    //     if(m_KeyConfig->Is(data::KeyConfig::OK))
    //     {
    //        m_WaitTime = 0;
    //    }
    //}

    if(m_Replay->IsEnd())
    {
        m_Replay->Restart();
    }

    UpdateScene();
}


/******************************************************************************
|   End of File
*******************************************************************************/