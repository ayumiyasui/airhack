/*
# follow.cpp

- リプレイ再生クラス - ある角度から追いかける

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "follow.h"
#include "../../game/playerCamera.h"
#include "../fighter.h"
#include "../../helper/drawLayerManager.h"
#include "renderer/3d/camera.h"
#include "math/math.h"

namespace play_replay {

// @brief  : コンストラクタ
// @param  : 戦闘機
//         : ローカル視点
//         : ローカル注視点
//         : 球面補完の割合
//--------------------------------------------------------------------
Follow::Follow(const Fighter *const _fighter, const Vector3 &_eye, const Vector3 &_at, float _t) :
    EYE(_eye),
    AT(_at),
    SLEP_TIME(_t),
    m_NowRotation(_fighter->Rotation())
{
    auto camera = DrawLayerManager::Instance().GetCamera();
    auto &mtx   = _fighter->Matrix();

    camera->Eye = EYE * mtx;
    camera->At  = AT  * mtx;
    camera->Up  = Vector3::UP * Matrix3D(m_NowRotation);

    camera->Near = 10.0f;
    camera->Far  = 100000.0f;
    camera->FovY = Math::PI / 3.0f;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Follow::~Follow()
{
}

// @brief  : 更新
//--------------------------------------------------------------------
void Follow::Update(const PlayReplay *const _replay)
{
    auto  player     = _replay->GetPlayer();
    auto &player_rot = player->Rotation();
    auto camera = DrawLayerManager::Instance().GetCamera();

    auto quat = Quaternion::Sleap(m_NowRotation,player_rot,SLEP_TIME);
    m_NowRotation = ( -1.0f <= quat.w && quat.w <= 1.0f ) ? quat : m_NowRotation;
    Matrix3D rot(player_rot);
    Matrix3D rot_sleap(m_NowRotation);

    // 回転
    camera->Eye = EYE * rot_sleap + player->Position();
    camera->At  = AT  * rot + player->Position();
    camera->Up  = Vector3::UP * rot_sleap;
}



};    // namespace play_replay
/******************************************************************************
|   End of File
*******************************************************************************/