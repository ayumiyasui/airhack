/*
# enemy.cpp

- 敵クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "enemy.h"

#include <assert.h>
#include "game.h"
#include "player.h"
#include "app/app.h"
#include "../object/fighter.h"
#include "base/color.h"
#include "enemyAI/enemyFallDown.h"
#include "enemyManager.h"
#include "../data/manager.h"
#include "../object/fighterEffect/se.h"

// 敵クラス
//-----------------------------------------------------------------------------
// @brief  : コンストラクタ
// @param  : 最初のState
//         : 選択した機体
//         : オブジェクトクラス
//--------------------------------------------------------------------
Enemy::Enemy( enemy::IAI *_ai, Fighter *_fighter,int _score, Object *_parent ) :
    Object(_parent,true),
    m_Fighter(_fighter),
    m_AI(_ai),
    m_IsFallDown(false),
    m_Score(_score)
{
    auto &data_manager = data::Manager::Instance();
    assert(m_Fighter);
    assert(m_AI);
    m_Icon = new ui::Map::Icon(Color(1.0f,0.0f,0.0f,0.8f),_fighter);
    m_Replay = data_manager.Get<data::Replay>()->AddFighter(_fighter);

    // 効果音追加
    m_Fighter->ChengeEffect(new fighter::SE);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Enemy::~Enemy()
{
    delete m_AI;
    m_Fighter->IsDestroy = true;
}

// プロパティ
//--------------------------------------------------------------------
Fighter * const Enemy::Airframe(void) const{return m_Fighter;}
data::Replay::FighterData *const Enemy::Replay(void) const{return m_Replay;};

// @brief  : 更新
//--------------------------------------------------------------------
void Enemy::Update(void)
{
    m_AI->Update(this);
    if(m_IsFallDown)
    {
        delete m_AI;
        m_AI = new enemy::FallDown(m_Fighter);
        m_IsFallDown = false;
    }
}

// @brief  : 落下
//--------------------------------------------------------------------
void Enemy::FallDown(void)
{
    m_IsFallDown = true;
    ((EnemyManager *)Parent())->AddScore(m_Score);
    m_Replay->SetFallDown();
    m_Icon->IsDestroy = true;
}

// @brief  : 削除されるフラグが立ったとき
//--------------------------------------------------------------------
void Enemy::OnDestory(void)
{
}

/******************************************************************************
|   End of File
*******************************************************************************/