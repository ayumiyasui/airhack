/*
# gamePause.cpp

- ゲームシーンクラス(ポーズのみ)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "game.h"

#include "../object/world.h"
#include "../helper/drawLayerManager.h"
#include "player.h"
#include "enemyManager.h"
#include "ui/map.h"
#include "ui/score.h"
#include "ui/time.h"
#include "../title/title.h"
#include "../result/result.h"
#include "app/app.h"
#include "math/math.h"
#include "math/quad.h"
#include "math/vector2.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "renderer/2d/transfrom.h"
#include "renderer/2d/sprite2d.h"
#include "../data/manager.h"
#include "../data/dataKeyConfig.h"

// 内部クラス - ポーズクラス
//--------------------------------------------------------------------
const std::string Game::Pause::BACK_TEXTURE_FILE = "data/TEXTURE/pauseBg.tga";
const std::string Game::Pause::CURSOR_TEXTURE_FILE = "data/TEXTURE/pauseCursor.tga";
const Vector2     Game::Pause::WINDOW_POS(230.0f,32.0f);
const float       Game::Pause::CURSOR_POS_X = 60.0f;
const float       Game::Pause::CURSOR_POS_Y[MAX_MENU] = {
    35.0f + 95.0f, 100.0f + 95.0f, 163.0f + 95.0f
};
const Quad        Game::Pause::BG_SIZE(0.0f,660.0f,0.0f,566.0f);
const Quad        Game::Pause::CURSOR_SIZE(-15.0f,0.0f,-30.0f*0.5f,30.0f*0.5f);

Game::Pause::Pause(Game *_game) :
    Object(&TempNodeSingleton<Object>::Instance(),false),
    m_Game(_game),
    m_CursorNum(CONTINUE),
    m_SceneIsNext(false),
    m_GameIsDestroy(false),
    m_KeyConfig(data::Manager::Instance().Get<data::KeyConfig>())
{
    auto app     = &App::Instance();
    auto factory = app->Renderer()->Factory2D();
    auto audio   = app->Audio();

    m_Game->m_PauseIsDestory = false;

    // 背景の作成
    m_Back = factory->CreateTransform(DrawLayerManager::Instance().Get2DLayer(),false);
    m_Back->Position = WINDOW_POS;
    factory->CreateSprite(BG_SIZE,Quad(0.0f,1.0f,0.0f,1.0f),BACK_TEXTURE_FILE,m_Back,true);

    // カーソルの作成
    m_Cursor           = factory->CreateTransform(m_Back,true);
    m_Cursor->Position = Vector2(CURSOR_POS_X,CURSOR_POS_Y[m_CursorNum]);
    const Vector2 cursor_tex(0.5f/(CURSOR_SIZE.right-CURSOR_SIZE.left),0.5f/(CURSOR_SIZE.bottom-CURSOR_SIZE.top));
    factory->CreateSprite(CURSOR_SIZE,Quad(cursor_tex.x,1.0f,cursor_tex.y,1.0f),CURSOR_TEXTURE_FILE,m_Cursor,true);

    // カーソル音の作成
    m_SECursor = audio->LoadSE("data/SOUND/cursor.wav");
}
Game::Pause::~Pause()
{
    m_Back->IsDestroy = true;
    if(!m_GameIsDestroy) m_Game->m_PauseIsDestory = true;
    m_SECursor->Stop();
}
void Game::Pause::Update(void)
{
    if(m_Game->m_IsPausing)
    {
        m_Game->m_IsPausing = false;
        return;
    }

    // Pキーで元に戻る
    if(m_KeyConfig->Is(data::KeyConfig::PAUSE))
    {
        m_SECursor->Play();
        IsUpdate = false;
    }

    // カーソルを動かす
    if(m_KeyConfig->Is(data::KeyConfig::CURSOR_UP))
    {
        m_SECursor->Play();
        if(--m_CursorNum <  0) m_CursorNum = MAX_MENU -1;
    }
    if(m_KeyConfig->Is(data::KeyConfig::CURSOR_DOWN))
    {
        m_SECursor->Play();
        if(++m_CursorNum >= MAX_MENU) m_CursorNum = 0;
    }
    m_Cursor->Position = Vector2(CURSOR_POS_X,CURSOR_POS_Y[m_CursorNum]);

    // 選択する
    if(m_KeyConfig->Is(data::KeyConfig::OK))
    {
        switch(m_CursorNum)
        {
        case Pause::CONTINUE:
            IsUpdate = false;
            break;

        case Pause::RETRY:
            m_Game->ChengeNext<Game>();
            IsUpdate = false;
            m_SceneIsNext = true;
            break;

        case Pause::BACK_TITLE:
            m_Game->ChengeNext<Title>();
            IsUpdate = false;
            m_SceneIsNext = true;
            break;
        }
    }
}
void Game::Pause::OnEnable(void)
{
    if(m_SceneIsNext) return;
    m_Game->IsUpdate = false;
    m_Back->IsEnable = true;
    m_SECursor->Play();
}
void Game::Pause::OnDisable(void)
{
    if(m_SceneIsNext) return;
    m_Game->IsUpdate = true;
    m_Back->IsEnable = false;
    m_SECursor->Play();
}


/******************************************************************************
|   End of File
*******************************************************************************/