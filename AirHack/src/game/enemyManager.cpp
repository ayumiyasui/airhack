/*
# enemyManager.cpp

- 敵マネージャクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "enemyManager.h"
#include <algorithm>
#include "game.h"
#include "base/node.h"
#include "player.h"
#include "enemyAI/easyAI.h"
#include "enemyAI/enemyFallDown.h"
#include "enemyAI/rotation.h"
#include "enemyAI/easyAIShoot.h"
#include "../object/fighter.h"
#include "../object/world.h"
#include "math/math.h"
#include "ui/score.h"

// 敵マネージャクラス
//-----------------------------------------------------------------------------
// @brief  : コンストラクタ
// @param  : 作成する敵リスト
//         : 親クラス
//         : プレイヤークラス
//         : ワールドクラス
//--------------------------------------------------------------------
EnemyManager::EnemyManager(const std::vector<ENEMY_DATA> &_data_list, Game *_parent, Player *_player, World *_world, ui::Score *const _score) :
    Object(_parent,true),
    m_Score(_score)
{
    std::for_each(_data_list.begin(),_data_list.end(),[&](ENEMY_DATA _data){
        switch(_data.ai_type)
        {
        case TYPE_EASY:
            new Enemy(new enemy::EasyAI(_player),_world->CreateFighter(_data.fighter,this),_data.score,this);
            break;

        case TYPE_EASY_SHOOT:
            new Enemy(new enemy::EasyAIShoot(_player),_world->CreateFighter(_data.fighter,this),_data.score,this);
            
            break;

        default:
            new Enemy(new enemy::Rotation(Vector3(0.0f,1.0f,0.0f)),_world->CreateFighter(_data.fighter,this),_data.score,this);
            break;

        }
    });
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
EnemyManager::~EnemyManager()
{
}

// @brief  : スコアを足す
//--------------------------------------------------------------------
void EnemyManager::AddScore(unsigned int _add)
{
    m_Score->Add(_add);
}

// プロパティ
//--------------------------------------------------------------------
int  EnemyManager::Count(void) const
{
    int count = 0;
    auto i = Begin();
    while(i)
    {
        auto next = i->Next();
        ++count;
        i = next;
    }
    return count;
}
bool EnemyManager::IsAnnihilation(void) const{return Count() <= 0;}    // 全滅したか



/******************************************************************************
|   End of File
*******************************************************************************/