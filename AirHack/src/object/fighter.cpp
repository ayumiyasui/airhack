/*
# fighter.cpp

- 戦闘機クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "fighter.h"

#include "app/app.h"
#include "renderer/renderer.h"
#include "renderer/3d/factory.h"
#include "renderer/3d/camera.h"
#include "../helper/drawLayerManager.h"
#include "renderer/3d/transform3dQuat.h"
#include "math/vector3.h"
#include "math/matrix3d.h"
#include "collision/sphere.h"
#include "bullet/normal.h"
#include "world.h"
#include "locus.h"
#include "fighterFallDown.h"
#include "iFighterEffect.h"
#include "missle/None.h"

// 定数
//--------------------------------------------------------------------
const float Fighter::LOCUS_LENGTH_HARF = 6.0f*0.5f;    // 軌跡の幅
const Fighter::STATUS Fighter::CONST_STATUS[MAX_TYPE] =
{
    {
        "data/MODEL/player.pmd",
        0.02f, // ピッチがどれくらい上がるか？
        0.04f, // ロールがどれくらいになるか？
        0.01f, // ヨーがどれくらいになるか？
       10.0f,  // 通常の速さは？
       50.0f,  // 最高速度は？
        1.0f,  // 何フレーム毎に加速するのか？
        0.04f,  // ブレーキ数

        50.0f, // 当たり判定の大きさ
        100,   // 体力
        Color(0.0f,0.0f,1.0f,1.0f),

        Vector3(-70.0f,0.0f,40.0f),
        Vector3( 70.0f,0.0f,40.0f)
    },
    {
        "data/MODEL/enemy.pmd",
        0.01f, // ピッチがどれくらい上がるか？
        0.02f, // ロールがどれくらいになるか？
        0.005f, // ヨーがどれくらいになるか？
        5.0f,  // 通常の速さは？
       10.0f,  // 最高速度は？
        5.0f,  // 何フレーム毎に加速するのか？
        0.4f,  // ブレーキ数

        50.0f,                      // 当たり判定の大きさ
        3,                          // 体力
        Color(1.0f,0.0f,0.0f,1.0f), // イメージカラー

        Vector3(-70.0f,0.0f,40.0f),
        Vector3( 70.0f,0.0f,40.0f)
    },
    {
        "data/MODEL/UAV.pmd",
        0.04f, // ピッチがどれくらい上がるか？
        0.02f, // ロールがどれくらいになるか？
        0.01f, // ヨーがどれくらいになるか？
        1.0f,  // 通常の速さは？
        5.0f,  // 最高速度は？
        0.0f,  // 何フレーム毎に加速するのか？
        0.4f,  // ブレーキ数

        30.0f,                      // 当たり判定の大きさ
        2,                          // 体力
        Color(1.0f,0.0f,0.0f,1.0f), // イメージカラー

        Vector3(-50.0f,0.0f,15.0f),
        Vector3( 50.0f,0.0f,15.0f)
    },
};

// @brief  : コンストラクタ
// @param  : セッティング
//         : ワールドクラス
//         : 親クラス
//--------------------------------------------------------------------
Fighter::Fighter(const SETTING &_setting, World *_world, Object *_parent) :
    Object(_parent),
    m_Setting(_setting),
    m_Speed(CONST_STATUS[m_Setting.type].auto_speed),
    m_IsAccele(false),
    m_IsBrake(false),
    m_HitSphere(new collision::Sphere(CONST_STATUS[m_Setting.type].coll_squre_length,Vector3::ZERO)),
    m_Life(CONST_STATUS[m_Setting.type].life),
    m_World(_world),
    Speed(m_Speed),
    ID(m_Setting.id),
    Life(m_Life),
    ImageColor(CONST_STATUS[m_Setting.type].image_color),
    Type(m_Setting.type),
    m_FighterFallDown(nullptr),
    m_Effect(new fighter::IFighterEffect),
    m_EffectNext(nullptr),
    m_Missle(new missle::NoneLauncher(m_Setting.id,_world))
{
    // デフォルトのバレットマネージャ
    const bullet::BulletManager::SETTING set_bullet =
    {
        CONST_STATUS[m_Setting.type].image_color,
        CONST_STATUS[m_Setting.type].max_speed + 100.0f,
        30, // ライフ
        5   // 連続するまでのインターバル
    };

    m_Bullet = new bullet::BulletManager(set_bullet,m_Setting.id,_world);

    _3d::IFactory *factory = App::Instance().Renderer()->Factory3D();
    m_Body = factory->CreateTransformQuat(DrawLayerManager::Instance().GetCamera(),true);
    m_Body->Position = m_Setting.InitPos;
    m_Body->Rotation = m_Setting.InitRot;
    factory->CreatePMDNoBone(CONST_STATUS[m_Setting.type].file_name,m_Body,true);

    Vector3 add(LOCUS_LENGTH_HARF,0.0f,0.0f);
    m_LocusLeft  = new Locus(CONST_STATUS[m_Setting.type].locus_l_pos-add,CONST_STATUS[m_Setting.type].locus_l_pos+add);
    m_LocusRight = new Locus(CONST_STATUS[m_Setting.type].locus_r_pos-add,CONST_STATUS[m_Setting.type].locus_r_pos+add);
    m_LocusLeft->Reset(this);
    m_LocusRight->Reset(this);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Fighter::~Fighter()
{
    if(m_EffectNext) delete m_EffectNext;
    delete m_Effect;
    if(m_FighterFallDown) delete m_FighterFallDown;
    delete m_LocusRight;
    delete m_LocusLeft;
    m_Body->IsDestroy = true;
    delete m_HitSphere;
    m_World->DeleteFighter(this);
    delete m_Missle;
}

// プロパティ
//--------------------------------------------------------------------
const Matrix3D   &Fighter::Matrix(void)   const { return m_Body->Matrix; }
const Quaternion &Fighter::Rotation(void) const { return m_Body->Rotation; }
const Vector3    &Fighter::Position(void) const { return m_Body->Position; }
float             Fighter::ParLife(void)  const { return static_cast<float>(m_Life) / static_cast<float>(CONST_STATUS[m_Setting.type].life); };
const _3d::TransformQuat *const Fighter::Trans(void) const{ return m_Body; };  // 本体
void Fighter::Position(const Vector3 &_set){m_Body->Position = _set;};
void Fighter::Rotation(const Quaternion &_set){ m_Body->Rotation = _set;};

// @brief  : エフェクトの変更
//--------------------------------------------------------------------
void Fighter::ChengeEffect(fighter::IFighterEffect *_next)
{
    if(m_EffectNext) delete m_EffectNext;
    m_EffectNext = _next;
}

// @brief  : ミサイルのセット
//--------------------------------------------------------------------
void Fighter::ChengeMissle(IMissleLauncher *_value)
{
    delete m_Missle;
    m_Missle = _value;
}

// @brief  : 加速・減速
//--------------------------------------------------------------------
void Fighter::Accele(const material::variable::f8uNormal &_t)
{
    m_Speed += CONST_STATUS[m_Setting.type].add_acceleration * static_cast<float>(_t);
    if(m_Speed > CONST_STATUS[m_Setting.type].max_speed) m_Speed = CONST_STATUS[m_Setting.type].max_speed;
    m_IsAccele = true;
}
void Fighter::Brake(const material::variable::f8uNormal &_t)
{
    m_Speed += ( 0 - m_Speed ) * CONST_STATUS[m_Setting.type].brake * static_cast<float>(_t);
}

// @brief  : ピッチアップ・ピッチダウン
//--------------------------------------------------------------------
void Fighter::PitchUp(void)
{
    Pitch(1.0f);
}

void Fighter::PitchDown(void)
{
    Pitch(-1.0f);
}

void Fighter::Pitch(const material::variable::f8sNormal &_t)
{
    m_Effect->Pitch(_t);
    const Quaternion old = m_Body->Rotation;
    const Quaternion add(0.0f,CONST_STATUS[m_Setting.type].max_pitch * static_cast<float>(_t),0.0f);
    m_Body->Rotation = old * add;
}

// @brief  : ロールレフト・ロールライト
//--------------------------------------------------------------------
void Fighter::RollLeft(void)
{
    Roll(1.0f);
}
void Fighter::RollRight(void)
{
    Roll(-1.0f);
}
void Fighter::Roll(const material::variable::f8sNormal &_t)
{
    m_Effect->Roll(_t);
    const Quaternion old = m_Body->Rotation;
    const Quaternion add(0.0f,0.0f,CONST_STATUS[m_Setting.type].max_roll * static_cast<float>(_t));
    m_Body->Rotation = old * add;
}

// @brief  : ヨーレフト・ヨーライト
//--------------------------------------------------------------------
void Fighter::YawLeft(void)
{
    Yaw(-1.0f);
}
void Fighter::YawRight(void)
{
    Yaw(1.0f);
}
void Fighter::Yaw(const material::variable::f8sNormal &_t)
{
    m_Effect->Yaw(_t);
    const Quaternion old = m_Body->Rotation;
    const Quaternion add(-CONST_STATUS[m_Setting.type].max_yaw * static_cast<float>(_t),0.0f,0.0f);
    m_Body->Rotation = old * add;
}

// @brief  : 機銃を撃つ
//--------------------------------------------------------------------
void Fighter::Shoot(void)
{
    m_Effect->Shoot();
    Vector3 pos = m_Body->Position;
    Vector3 vec = CreateVecZ();

    pos += vec * 30.0f;

    m_Bullet->Shoot(pos,vec);
}

// @brief  : ミサイルの発射
//--------------------------------------------------------------------
void Fighter::Missle(void)
{
    m_Missle->Shoot(m_Body->Position,CreateVecZ());
}

// @brief  : 初期化
//--------------------------------------------------------------------
void Fighter::OnEnable(void)
{
    m_Body->IsEnable = true;
}
void Fighter::OnDisable(void)
{
    m_Body->IsEnable = false;
}

// @brief  : 更新
//--------------------------------------------------------------------
void Fighter::Update(void)
{
    if(Life <= 0)
    {
        FallDown();
        m_FighterFallDown->Update(this);
        const float alpha = m_Speed/CONST_STATUS[m_Setting.type].max_speed;
        m_LocusLeft->Update(this,alpha);
        m_LocusRight->Update(this,alpha);
        return;
    }

    // 位置の更新
    Vector3 pos = m_Body->Position;
    const Vector3 vec = CreateVecZ() * m_Speed;
    pos += vec;

    // ワールドの外には出さない
    {
        if(World::WORLD_SIZE.x *  0.5f < pos.x) pos.x = World::WORLD_SIZE.x *  0.5f;
        if(World::WORLD_SIZE.x * -0.5f > pos.x) pos.x = World::WORLD_SIZE.x * -0.5f;
        if(World::WORLD_SIZE.y         < pos.y) pos.y = World::WORLD_SIZE.y;
        if(                       0.0f > pos.y) pos.y = 0.0f;
        if(World::WORLD_SIZE.z *  0.5f < pos.z) pos.z = World::WORLD_SIZE.z *  0.5f;
        if(World::WORLD_SIZE.z * -0.5f > pos.z) pos.z = World::WORLD_SIZE.z * -0.5f;
    }

    // オートパイロット(移動のみ)
    m_Speed += (0.0f - m_Speed) * 0.03f;
    if(m_Speed <= CONST_STATUS[m_Setting.type].auto_speed) m_Speed = CONST_STATUS[m_Setting.type].auto_speed;

    // 位置の確定
    m_Body->Position = pos;

    m_IsAccele = false;
    m_IsBrake  = false;

    // あたり判定の位置の変更
    m_HitSphere->SetPos(pos);

    // 回転のノーマライズ
    {
        Quaternion rot = m_Body->Rotation;
        m_Body->Rotation = rot.Normalize();
    }

    const float alpha = m_Speed/CONST_STATUS[m_Setting.type].max_speed;
    m_LocusLeft->Update(this,alpha);
    m_LocusRight->Update(this,alpha);
    m_Effect->Update(this);
    if(m_EffectNext)
    {
        delete m_Effect;
        m_Effect = m_EffectNext;
        m_EffectNext = nullptr;
    }
}

// @brief  : 墜落
//--------------------------------------------------------------------
void Fighter::FallDown(void)
{
    m_Life = 0;
    if(!m_FighterFallDown)
    {
        m_FighterFallDown = new FighterFallDown(this);
        m_Effect->FallDown();
    }
}

// @brief  : ベクトルの計算
//--------------------------------------------------------------------
Vector3 Fighter::CreateVecZ(void) const
{
    const Matrix3D rot(static_cast<Quaternion>(m_Body->Rotation));
    return Vector3(0.0f,0.0f,-1.0f) * rot;
}

// @brief  : 弾に当たったのか？
// @param  : レイの始め
//         : レイの終わり
// @return : 当たったのか？
//--------------------------------------------------------------------
bool Fighter::IsHitBullet( const Vector3 &_ray_start, const Vector3 &_ray_end ) const
{
    // あたり判定
    return m_HitSphere->IsHitRay(_ray_start,_ray_end);
}

// @brief  : 弾に当たったのか？&当たったら消える
// @param  : レイの始め
//         : レイの終わり
// @return : 当たったのか？
//--------------------------------------------------------------------
bool Fighter::CheakHitBullet( const Vector3 &_ray_start, const Vector3 &_ray_end )
{
    // そもそも体力がなければ何もしない
    if(m_Life <= 0) return false;

    // 当たっていなければそのまま返す
    if(!IsHitBullet(_ray_start,_ray_end)) return false;

    // 当たったら体力を減らす
    m_Effect->HitBullet();
    --m_Life;
    return true;
}

// @brief  : ミサイルに当たったのか？&当たったら？
// @param  : レイの始め
//         : レイの終わり
// @return : 当たったのか？
//--------------------------------------------------------------------
bool Fighter::CheakHitMissle(const Vector3 &_ray_start, const Vector3 &_ray_end)
{
    // そもそも体力がなければ何もしない
    if(m_Life <= 0) return false;

    // 当たっていなければそのまま返す
    if(!IsHitBullet(_ray_start,_ray_end)) return false;

    // 当たったら体力を減らす
    m_Effect->HitBullet();
    m_Life -= CONST_STATUS[m_Setting.type].life / 5;
    return true;
}

// @brief  : リセット
//--------------------------------------------------------------------
void Fighter::Reset(void)
{
    m_Speed = CONST_STATUS[m_Setting.type].auto_speed;
    m_IsAccele = false;
    m_IsBrake = false;
    m_Life = CONST_STATUS[m_Setting.type].life;
    m_Body->Position = m_Setting.InitPos;
    m_Body->Rotation = m_Setting.InitRot;
    m_LocusLeft->Reset(this);
    m_LocusRight->Reset(this);
}


/******************************************************************************
|   End of File
*******************************************************************************/