/*
# dataKeyConfig.h

- キーコンフィングのデータ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "dataKeyConfig.h"
#if defined(WIN32) || defined(WIN64)
#include "input/inputKeyboard.h"
#elif __APPLE__
#include "input/inputMac.h"
#endif

namespace data {

// 定数
//--------------------------------------------------------------------
#if defined(WIN32) || defined(WIN64)
const BYTE KeyConfig::INIT_KEY[MAX_KEY_CONFIG_LIST] =
{
    DIK_SPACE,
    DIK_P,

    DIK_W,
    DIK_S,

    DIK_S,
    DIK_W,
    DIK_A,
    DIK_D,
    DIK_Q,
    DIK_E,
    DIK_SPACE,
    DIK_UP,
    DIK_DOWN
};
#elif __APPLE__
const unsigned short KeyConfig::INIT_KEY[MAX_KEY_CONFIG_LIST] =
{
    kVK_Space,
    kVK_ANSI_P,
    
    kVK_ANSI_W,
    kVK_ANSI_S,
    
    kVK_ANSI_S,
    kVK_ANSI_W,
    kVK_ANSI_A,
    kVK_ANSI_D,
    kVK_ANSI_Q,
    kVK_ANSI_E,
    kVK_Space,
    kVK_UpArrow,
    kVK_DownArrow,
};
#endif

// @brief  : コンストラクタ
//--------------------------------------------------------------------
KeyConfig::KeyConfig() :
    IData(TYPE_KEY_CONFIG)
{
    std::memcpy(m_Data.key,INIT_KEY,sizeof(m_Data.key));
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
KeyConfig::~KeyConfig()
{
}

// @brief  : データのセーブ
// @param  : ファイルポインタ
//--------------------------------------------------------------------
void KeyConfig::Save(FILE *_file)
{
#if defined(WIN32) || defined(WIN64)
    m_Data.fmt = 'W';
#elif __APPLE__
    m_Data.fmt = 'M';
#endif
    fwrite((const void *)&m_Data,sizeof(m_Data),1,_file);
}

// @brief  : データのロード
// @param  : ファイルポインタ
//--------------------------------------------------------------------
void KeyConfig::Load(FILE *_file)
{
    fread((void *)&m_Data,sizeof(m_Data),1,_file);
#if defined(WIN32) || defined(WIN64)
    if(m_Data.fmt != 'W')
    {
        std::memcpy(m_Data.key,INIT_KEY,sizeof(m_Data.key));
    }
#elif __APPLE__
    if(m_Data.fmt != 'M')
    {
        std::memcpy(m_Data.key,INIT_KEY,sizeof(m_Data.key));
    }
#endif
}

// @brief  : キーチェック
// @param  : キー
//--------------------------------------------------------------------
bool KeyConfig::Is(KEY_CONFIG_LIST _list) const
{
#if defined(WIN32) || defined(WIN64)
    if((int)_list < (int)MAX_KEY_TRIGGER) return Keyboard::Instance().GetTrigger(m_Data.key[_list]);
    if((int)_list < (int)MAX_KEY_REPEAT ) return Keyboard::Instance().GetRepeat(m_Data.key[_list]);
    if((int)_list < (int)MAX_KEY_PRESS  ) return Keyboard::Instance().GetPress(m_Data.key[_list]);
#elif __APPLE__
    if((int)_list < (int)MAX_KEY_TRIGGER) return InputMac::Instance().GetKeyboardTrigger(m_Data.key[_list]);
    if((int)_list < (int)MAX_KEY_REPEAT ) return InputMac::Instance().GetKeyboardRepeat(m_Data.key[_list]);
    if((int)_list < (int)MAX_KEY_PRESS  ) return InputMac::Instance().GetKeyboardPress(m_Data.key[_list]);
#endif
    return false;
}

//// @brief  : キー設定をする
//// @param  : 設定したいキー
////--------------------------------------------------------------------
//void KeyConfig::Set(KEY_CONFIG_LIST)
//{
//}

//// @brief  : キー設定の取得
//// @param  : 設定したいキ
////--------------------------------------------------------------------
//#if defined(WIN32) || defined(WIN64)
//const BYTE &KeyConfig::Get(KEY_CONFIG_LIST) const;
//#elif __APPLE__
//#endif
//


};  // namespace data
/******************************************************************************
|   End of File
*******************************************************************************/