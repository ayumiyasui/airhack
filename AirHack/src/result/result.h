/*
# result.h

- リザルトシーンクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_RESULT_SCENE_H_
#define AIR_HACK_RESULT_SCENE_H_

#include <string>
#include "../helper/scene.h"
#include "renderer/2d/draw2d.h"
#include "math/vector2.h"
#include "audio/iAudio.h"

namespace _2d {
    class Transform;
};

namespace data {
    class KeyConfig;
};

class PlayReplay;

class Result : public Scene
{
public:
    // @brief  : コンストラクタ
    // @param  : 親オブジェクト
    //         : フェード
    //--------------------------------------------------------------------
    Result( Object *,Fade * );

protected:
    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Result();

private:
    // 定数
    //--------------------------------------------------------------------
    static const int         WAIT_TIME;
    static const int         GO_TO_RESULT_TIME;

    // メンバ変数
    //--------------------------------------------------------------------
    int             m_GoToResultTime;   // 強制的にタイトルに戻るまでの時間
    int             m_WaitTime;         // 入力ができるようになるまでの時間
    data::KeyConfig *m_KeyConfig;       // キーコンフィング
    PlayReplay      *m_Replay;          // リプレイ
    material::IAudio::ISource *m_BGM;   // BGM

    // @brief  : Enableになった時の初期化
    //--------------------------------------------------------------------
    void OnEnable( void ) override;

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update( void ) override;
};



#endif //  AIR_HACK_RESULT_SCENE_H_
/******************************************************************************
|   End of File
*******************************************************************************/