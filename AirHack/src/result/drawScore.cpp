/*
# drawScore.cpp

- リザルト用スコア表示クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "drawScore.h"
#include "result.h"
#include "../helper/number.h"
#include "../helper/drawLayerManager.h"
#include "../data/score.h"
#include "../data/manager.h"
#include "app/app.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "renderer/2d/transfrom.h"
#include "../data/dataReplay.h"

namespace result {
// 定数
//--------------------------------------------------------------------
const std::string DrawScore::TEXTURE_NAME("data/TEXTURE/result.tga");
const std::string DrawScore::NUMBER_NAME("data/TEXTURE/number.tga");   // テクスチャ名
const Vector2     DrawScore::BACK_POS(0.0f,524.65f);      // 数字表示位置
const Vector2     DrawScore::BACK_SIZE(1120.0f,115.0f);     // 数字の大きさ
const Vector2     DrawScore::NUMBER_POS(350.0f,0.0f);    // 数字表示位置
const Vector2     DrawScore::HIGH_SCORE_POS(700.0f,0.0);    // 数字表示位置
const Vector2     DrawScore::NUMBER_SIZE(120.0f,39.0f);   // 数字の大きさ


// @brief  : コンストラクタ
// @param  : 親オブジェクト
//--------------------------------------------------------------------
DrawScore::DrawScore(Result *_parent) :
    Object(_parent)
{
    auto renderer = App::Instance().Renderer();
    auto factory  = renderer->Factory2D();
    auto layer    = DrawLayerManager::Instance().Get2DLayer();
    auto data     = data::Manager::Instance().Get<data::Score>();

    // 背景
    m_Back = factory->CreateTransform(layer,true);
    m_Back->Position = BACK_POS;
    factory->CreateSprite(
        Quad(0.0f,BACK_SIZE.x,BACK_SIZE.y*-0.5f,BACK_SIZE.y*0.5f),
        Quad(0.5f/BACK_SIZE.x,1.0f,0.5f/BACK_SIZE.y,1.0f),
        TEXTURE_NAME,
        m_Back,true);

    // プレイヤーのスコア
    m_Score = new DigitWithNumbers(NUMBER_NAME,
        Quad(-NUMBER_SIZE.x*0.5f,NUMBER_SIZE.x*0.5f,-NUMBER_SIZE.y*0.5f,NUMBER_SIZE.y*0.5f),
        5,
        m_Back,true);
    m_Score->Transform()->Position = NUMBER_POS;
    m_Score->SetColor(Color(0.0f,1.0f,0.0f,1.0f));

    // ハイスコア更新
    m_Score->Set(data->GetUserData());
    bool is_update = data->UpdateHighScore();
    if(is_update) m_Score->SetColor(Color(1.0f,1.0f,0.0f,1.0f));

    // ハイスコアに応じてリプレイデータを更新
    auto replay = data::Manager::Instance().Get<data::Replay>();
#ifndef _DEBUG
    if(is_update) replay->Save();
#else
    replay->Save();
#endif

    // ハイスコア
    float scale = 0.85f;
    m_HighScore = new DigitWithNumbers(NUMBER_NAME,
        Quad(-NUMBER_SIZE.x*0.5f *scale,NUMBER_SIZE.x*0.5f*scale,-NUMBER_SIZE.y*0.5f*scale,NUMBER_SIZE.y*0.5f*scale),
        5,
        m_Back,true);
    m_HighScore->Transform()->Position = HIGH_SCORE_POS;
    m_HighScore->SetColor(Color(0.0f,1.0f,0.0f,1.0f));

    m_HighScore->Set(data->GetHighScore());
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
DrawScore::~DrawScore()
{
    m_Back->IsDestroy = true;
}



};  // namespace result
/******************************************************************************
|   End of File
*******************************************************************************/