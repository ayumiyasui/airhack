/*
# dataReplay.h

- リプレイデータ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_DATA_REPLAY_H_
#define AIR_HACK_DATA_REPLAY_H_

#include <string>
#include <vector>
#include "variable/bitName.h"
#include "variable/f8Normal.h"
#include "../object/fighter.h"


namespace data {

class Replay
{
public:
    // ファイターデータ
    //--------------------------------------------------------------------
    class FighterData
    {
    public:
    #pragma pack(push,1)    //アラインメント制御
        struct SETTING      // 初期設定
        {
            material::variable::u16 id;
            Fighter::TYPE           type;
        };

        struct FRAME_DATA   // フレームデータ
        {
            material::variable::u8 accele;  // アクセル
            material::variable::u8 brake;   // ブレーキ
            material::variable::s8 pitch;   // ピッチ
            material::variable::s8 roll;    // ロール
            material::variable::s8 yaw;     // ヨー

            material::variable::u8 byte;    // フラグ用(Shoot,fallDown)
        };
    #pragma pack(pop)     // アライメント制御の終了

        FighterData(const SETTING &_setting,material::variable::u16 _time,const Replay *const _parent);
        ~FighterData();

        void Set(const FRAME_DATA &);
        void SetAccele(material::variable::f8uNormal);
        void SetBrake(material::variable::f8uNormal);
        void SetPitch(material::variable::f8sNormal);
        void SetRoll(material::variable::f8sNormal);
        void SetYaw(material::variable::f8sNormal);
        void SetFallDown(void);
        void SetShoot(void);

        float GetAccele(void) const;
        float GetBrake(void) const;
        float GetPitch(void) const;
        float GetRoll(void) const;
        float GetYaw(void) const;
        bool GetFallDown(void) const;
        bool GetShoot(void) const;

        template <typename T> const T &Get(void) const;
        const SETTING &GetSetting(void) const;
        const FRAME_DATA *GetData(void) const{return m_Data;};

    private:
        SETTING             m_Setting;   // 設定
        FRAME_DATA         *m_Data;      // フレームデータ
        const Replay *const  m_Replay;     // リプレイ

    };

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    Replay();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Replay();

    // @brief  : 再生終了
    //--------------------------------------------------------------------
    bool IsEndUpdate(void) const{return m_NowTime >= m_Time;}

    // @brief  : リセット
    //--------------------------------------------------------------------
    void Reset(void);
    void ResetTime(void){m_NowTime = 0;}

    // @brief  : ステージデータの保存
    // @param  : 初期時間
    //--------------------------------------------------------------------
    void SetStage(material::variable::u16);

    // @brief  : ファイターの保存
    // @param  : ファイターデータ
    // @return : リプレイ用データ管理クラス
    //--------------------------------------------------------------------
    FighterData *const AddFighter(const Fighter *const);

    // @brief  : ファイターリストの取得
    // @return : ファイターリスト
    //--------------------------------------------------------------------
    const std::vector<FighterData *> &GetFighterList(void) const{return m_Fighter;};

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(void);

    // @brief  : データのセーブ
    //--------------------------------------------------------------------
    void Save(void);

    // @brief  : データのロード
    //--------------------------------------------------------------------
    void Load(void);

private:
    // 定数
    //--------------------------------------------------------------------
    static const std::string FILE_NAME; // ファイル名

    // メンバ変数
    //--------------------------------------------------------------------
    material::variable::u16     m_Time;        // ステージ時間
    std::vector<FighterData *>  m_Fighter;     // ファイターデータ
    int                         m_NowTime;     // 経過時間
};

};  // namespace data
#endif // AIR_HACK_DATA_REPLAY_H_
/******************************************************************************
|   End of File
*******************************************************************************/