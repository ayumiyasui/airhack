/*
# controlCamera.cpp

- リプレイ用カメラコントロールクラス(title)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "controlCamera.h"
#include <time.h>
#include <stdlib.h>
#include "../object/fighter.h"
#include "../object/playReplayCamera/noneCamera.h"
#include "../object/playReplayCamera/follow.h"
#include "../object/playReplayCamera//followAt.h"
#include "../object/playReplayCamera/followNotRot.h"
#include "../object/playReplayCamera/playing.h"

namespace title {

// 定数
//--------------------------------------------------------------------
const int ControlCamera::NEXT_STATE_TIME = 60 * 5;   // 次のステートまでの時間

// @brief  : コンストラクタ
// @param  : 戦闘機
//--------------------------------------------------------------------
ControlCamera::ControlCamera(const Fighter *const _fighter) :
    m_Fighter(_fighter),
    m_State(new play_replay::CameraNone()),
    m_Time(NEXT_STATE_TIME)
{
    srand((unsigned int)time(NULL));
    ChengeState();
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
ControlCamera::~ControlCamera()
{
    delete m_State;
}

// @brief  : 更新
//--------------------------------------------------------------------
void ControlCamera::Update(const PlayReplay *const _replay)
{
    m_State->Update(_replay);

    if(--m_Time <= 0)
    {
        m_Time = NEXT_STATE_TIME;
        ChengeState();
    };
}

// @brief  : ステート変更
//--------------------------------------------------------------------
void ControlCamera::ChengeState(void)
{
    delete m_State;
    switch(rand()%MAX_STATE)
    {
    case FOLLOW_PLAYER:
        m_State = new play_replay::Playing(m_Fighter);
        break;

    case FOLLOW_RIGHT:
        m_State = new play_replay::FollowNotRot(m_Fighter,Vector3(500.0f,0.0f,0.0f),Vector3(0.0f,0.0f,100.0f),0.1f);
        break;

    case FOLLOW_LEFT:
        m_State = new play_replay::Follow(m_Fighter,Vector3(-300.0f,0.0f,0.0f),Vector3(0.0f,0.0f,-100.0f),0.1f);
        break;

    case FOLLOW_BACK:
        m_State = new play_replay::Follow(m_Fighter,Vector3(0.0f,150.0f,-200.0f),Vector3(0.0f,0.0f,50.0f),1.0f);
        break;

    case LOOKUP_UNDER:
        m_State = new play_replay::FollowAt(m_Fighter,m_Fighter->Position(),Vector3(0.0f,0.0f,1.0f),1.0f);
        break;
    }
}



};    // namespace title
/******************************************************************************
|   End of File
*******************************************************************************/