/*
# fighter.h

- 戦闘機クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_I_FIGHTER_H_
#define AIR_HACK_I_FIGHTER_H_

#include <string>
#include "base/object.h"
#include "base/tempGetter.h"
#include "renderer/3d/transform3dQuat.h"
#include "base/color.h"
#include "variable/f8Normal.h"

class IBulletManager;
class IMissleLauncher;
class World;
namespace collision {
    class Sphere;
};

class Locus;
class FighterFallDown;
namespace fighter {
    class IFighterEffect;
};
class Fighter : public Object
{
public:
    // 戦闘機の種類
    //--------------------------------------------------------------------
    enum TYPE
    {
        TYPE_PLAYER,
        TYPE_ENEMY,
        TYPE_UAV,

        MAX_TYPE
    };

    // 戦闘機設定
    //--------------------------------------------------------------------
    struct SETTING
    {
        SETTING() : InitRot(Quaternion::IDENTITY){};
        TYPE       type;            // 戦闘機の種類
        material::variable::u16 id; // ID

        Vector3    InitPos;         // 初期位置
        Quaternion InitRot;         // 初期回転
    };

    // プロパティ
    //--------------------------------------------------------------------
    TempGetter<float>          Speed;               // スピード
    TempGetter<material::variable::u16> ID;         // ID
    TempGetter<material::variable::u8>  Life;       // 体力
    TempGetter<const Color>          ImageColor;    // イメージカラー
    TempGetter<TYPE>          Type;    // 戦闘機の種類
    const Matrix3D            &Matrix(void) const;
    const Quaternion          &Rotation(void) const;
    void                       Rotation(const Quaternion &);
    const Vector3             &Position(void) const;
    void                       Position(const Vector3 &);
    float                      ParLife(void) const;  // 体力の割合
    const _3d::TransformQuat *const Trans(void) const;  // 本体
 
    // @brief  : コンストラクタ
    // @param  : セッティング
    //         : ワールドクラス
    //         : 親クラス
    //--------------------------------------------------------------------
    Fighter(const SETTING &, World *, Object *);

    // @brief  : 加速・減速
    //--------------------------------------------------------------------
    void Accele(const material::variable::f8uNormal &);
    void Brake(const material::variable::f8uNormal &);

    // @brief  : ピッチアップ・ピッチダウン
    //--------------------------------------------------------------------
    void PitchUp(void);
    void PitchDown(void);
    void Pitch(const material::variable::f8sNormal &);

    // @brief  : ロールレフト・ロールライト
    //--------------------------------------------------------------------
    void RollLeft(void);
    void RollRight(void);
    void Roll(const material::variable::f8sNormal &);

    // @brief  : ヨーレフト・ヨーライト
    //--------------------------------------------------------------------
    void YawLeft(void);
    void YawRight(void);
    void Yaw(const material::variable::f8sNormal &);

    // @brief  : 機銃を撃つ
    //--------------------------------------------------------------------
    void Shoot(void);

    // @brief  : ミサイルの発射
    //--------------------------------------------------------------------
    void Missle(void);

    // @brief  : 墜落
    //--------------------------------------------------------------------
    void FallDown(void);

    // @brief  : ベクトルの計算
    //--------------------------------------------------------------------
    Vector3 CreateVecZ(void) const;

    // @brief  : 弾に当たったのか？&当たったら消える
    // @param  : レイの始め
    //         : レイの終わり
    // @return : 当たったのか？
    //--------------------------------------------------------------------
    bool CheakHitBullet(const Vector3 &_ray_start, const Vector3 &_ray_end);

    // @brief  : ミサイルに当たったのか？&当たったら？
    // @param  : レイの始め
    //         : レイの終わり
    // @return : 当たったのか？
    //--------------------------------------------------------------------
    bool CheakHitMissle(const Vector3 &_ray_start, const Vector3 &_ray_end);

    // @brief  : 弾に当たったのか？
    // @param  : レイの始め
    //         : レイの終わり
    // @return : 当たったのか？
    //--------------------------------------------------------------------
    bool IsHitBullet( const Vector3 &, const Vector3 & ) const;

    // @brief  : リセット
    //--------------------------------------------------------------------
    void Reset(void);

    // @brief  : エフェクトの変更
    //--------------------------------------------------------------------
    void ChengeEffect(fighter::IFighterEffect *);

    // @brief  : ミサイルのセット
    //--------------------------------------------------------------------
    void ChengeMissle(IMissleLauncher *_value);

private:
    // 戦闘機設定
    //--------------------------------------------------------------------
    struct STATUS
    {
        std::string file_name;          // ファイル名
        float       max_pitch;          // ピッチがどれくらい上がるか？
        float       max_roll;           // ロールがどれくらいになるか？
        float       max_yaw;            // ヨーがどれくらいになるか？
        float       auto_speed;         // 通常の速さは？
        float       max_speed;          // 最高速度は？
        float       add_acceleration;   // 何フレーム毎に加速するのか？
        float       brake;              // ブレーキ数

        float       coll_squre_length;  // 当たり判定の大きさ
        material::variable::u8 life;    // 体力
        Color       image_color;        // イメージカラー

        Vector3    locus_l_pos;         // 軌跡始め左
        Vector3    locus_r_pos;         // 軌跡始め右
    };

    // 定数
    //--------------------------------------------------------------------
    static const float  LOCUS_LENGTH_HARF;          // 軌跡の幅
    static const STATUS CONST_STATUS[MAX_TYPE];     // 戦闘機のステータス

    // メンバ変数
    //--------------------------------------------------------------------
    SETTING             m_Setting;      // セッティング
    _3d::TransformQuat *m_Body;         // 本体
    float               m_Speed;        // 現在のスピード
    bool                m_IsAccele;     // アクセルを踏んだのか？
    bool                m_IsBrake;      // ブレーキを踏んだのか？
    IBulletManager     *m_Bullet;       // 機銃
    collision::Sphere  *m_HitSphere;    // 球当たり判定
    unsigned char       m_Life;         // 体力
    World              *m_World;
    Locus              *m_LocusLeft;    // 軌跡(左)
    Locus              *m_LocusRight;   // 軌跡(右)
    FighterFallDown    *m_FighterFallDown;  // 落下
    fighter::IFighterEffect *m_Effect;  // エフェクト
    fighter::IFighterEffect *m_EffectNext;  // エフェクト
    IMissleLauncher         *m_Missle;      // ミサイル

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Fighter();

    // @brief  : 初期化
    //--------------------------------------------------------------------
    void OnEnable(void)  override;
    void OnDisable(void) override;

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(void) override;
};


#endif //  AIR_HACK_I_FIGHTER_H_
/******************************************************************************
|   End of File
*******************************************************************************/