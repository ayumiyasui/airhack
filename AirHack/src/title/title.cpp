/*
# title.h

- タイトルシーンクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "title.h"
#include "app/app.h"
#include "../game/game.h"
#include "../object/world.h"
#include "../data/manager.h"
#include "../data/dataKeyConfig.h"
#include "../data/dataReplay.h"
#include "../object/playReplayCamera/noneCamera.h"
#include "controlCamera.h"
#include "logo.h"
#include "press.h"

using namespace material;
using namespace title;

// @brief  : コンストラクタ
// @param  : 親オブジェクト
//         : フェード
//--------------------------------------------------------------------
Title::Title( Object *_obj, Fade *_fade ) :
    Scene(_obj,_fade),
    m_KeyConfig(data::Manager::Instance().Get<data::KeyConfig>())
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Title::~Title()
{
    m_BGM->Stop();
}

// @brief  : Enableになった時の初期化
//--------------------------------------------------------------------
void Title::OnEnable( void )
{
    auto app   = &App::Instance();
    auto audio = app->Audio();

    // ロゴの生成
    new Logo(this);

    // Press作成
    new Press(this);

    // リプレイ再生
    auto data_replay = data::Manager::Instance().Get<data::Replay>();
    data_replay->Reset();
    data_replay->Load();
    m_PlayReplay = new PlayReplay(this,new play_replay::CameraNone());
    m_PlayReplay->ChengeCamera(new ControlCamera(m_PlayReplay->GetPlayer()));

    // BGMを鳴らす
    m_BGM = audio->LoadBGM("data/SOUND/title.wav");
    m_BGM->Play();
}

// @brief  : 更新
//--------------------------------------------------------------------
void Title::Update( void )
{
    App::Instance().Renderer()->Print("<<タイトルモード>>");

    if(m_KeyConfig->Is(data::KeyConfig::OK))
    {
        ChengeNext<Game>();
    }

    if(m_PlayReplay->IsEnd())
    {
        m_PlayReplay->Restart();
    }

    UpdateScene();
}


/******************************************************************************
|   End of File
*******************************************************************************/