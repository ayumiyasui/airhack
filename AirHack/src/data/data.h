/*
# data.h

- 保存データ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIRHACK_DATA_H_
#define AIRHACK_DATA_H_

#include <stdio.h>
#include "variable/bitName.h"
#include "base/tempNodeSingleton.h"

namespace data {

// データの種類
enum TYPE
{
    TYPE_SCORE = 0,
    TYPE_KEY_CONFIG,

    MAX_TYPE
};

// データクラス(抽象)
class IData
{
public:
    // @brief  : コンストラクタ
    // @param  : 種類
    //--------------------------------------------------------------------
    IData(TYPE _type) : m_Type(_type){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IData(){};

    // プロパティ
    //--------------------------------------------------------------------
    operator TYPE () const{ return m_Type; };

    // @brief  : データのセーブ
    // @param  : ファイルポインタ
    //--------------------------------------------------------------------
    virtual void Save(FILE *) = 0;

    // @brief  : データのロード
    // @param  : ファイルポインタ
    //--------------------------------------------------------------------
    virtual void Load(FILE *) = 0;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    TYPE m_Type;    // 種類
};


};  // namespace data
#endif //  AIRHACK_DATA_H_
/******************************************************************************
|   End of File
*******************************************************************************/