/*
# dataManager.cpp

- 保存データマネージャ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#define _CRT_SECURE_NO_WARNINGS
#include "manager.h"
#include <assert.h>
#include <stdio.h>
#include "score.h"
#include "dataKeyConfig.h"
#include "dataReplay.h"

namespace data {
// 定数
//--------------------------------------------------------------------
const std::string Manager::SAVE_DATA("data/sd.airhack");

// @brief  : コンストラクタ
//--------------------------------------------------------------------
Manager::Manager()
{
    memset(m_List,0,sizeof(m_List));

    m_List[TYPE_SCORE] = new Score();
    m_List[TYPE_KEY_CONFIG] = new KeyConfig();
    m_Replay = new Replay();
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Manager::~Manager()
{
    delete m_Replay;
    ForEach([](IData *_data) {
        delete _data;
    });
}

// @brief  : セーブ
//--------------------------------------------------------------------
void Manager::Save(void)
{
    FILE *file = fopen(SAVE_DATA.c_str(),"wb");
    ForEach([&](IData *_data) {
        if(_data) _data->Load(file);
    });
    fclose(file);
}

// @brief  : ロード
//--------------------------------------------------------------------
void Manager::Load(void)
{
    FILE *file = fopen(SAVE_DATA.c_str(),"rb");
    if(file)
    {
        ForEach([&](IData *_data) {
            if(_data) _data->Load(file);
        });
        fclose(file);
    }
}

// @brief  : データの取得
//--------------------------------------------------------------------
template < typename T > T *Manager::Get(void) const
{
    assert(false);
    return nullptr;
}
template<> Score *Manager::Get<data::Score>(void) const
{
    return (Score *)m_List[TYPE_SCORE];
}
template<> KeyConfig *Manager::Get<data::KeyConfig>(void) const
{
    return (KeyConfig *)m_List[TYPE_KEY_CONFIG];
}
template<> Replay *Manager::Get<data::Replay>(void) const
{
    return m_Replay;
}

// @brief  : ループ処理
//--------------------------------------------------------------------
void Manager::ForEach(std::function<void (IData *)> _function)
{
    for(int i=0 ; i < MAX_TYPE ; ++i)
    {
        _function(m_List[i]);
    }
}

};  // namespace data
/******************************************************************************
|   End of File
*******************************************************************************/