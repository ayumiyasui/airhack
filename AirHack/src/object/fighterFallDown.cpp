/*
# fighterFallDown.cpp

- 戦闘機クラス(撃墜された)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "fighterFallDown.h"
#include "math/math.h"
#include "fighter.h"
#include "exprosion.h"

// 定数
//--------------------------------------------------------------------
const float      FighterFallDown::ADD_FALL_TIME = -1.0f/(60.0f*3.0f);    // 落下時間
const Quaternion FighterFallDown::FALL_ROT(0.0f,-Math::PI*0.5f,0.0f);    // 落下方向
const float      FighterFallDown::FALL_LENGTH  = 2000.0f;                // 落下距離
const Quaternion FighterFallDown::ROLL_ADD(0.0f,0.0f,Math::PI*0.05f);    // 回転量

// @brief  : コンストラクタ
// @param  : 戦闘機
//--------------------------------------------------------------------
FighterFallDown::FighterFallDown(const Fighter *const _fighter) :
    m_PosOld(_fighter->Position()),
    m_RotOld(_fighter->Rotation()),
    m_RotZ(Quaternion::IDENTITY),
    m_Time(1.0f)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
FighterFallDown::~FighterFallDown()
{
}

// @brief  : 更新
// @param  : 親(Fighter)
//--------------------------------------------------------------------
void FighterFallDown::Update(Fighter *_fighter)
{
    // 時間になったら本体の更新を止める
    m_Time += ADD_FALL_TIME;
    if(m_Time < 0.0f)
    {
        m_Time   = 1.0f;
        _fighter->IsUpdate = false;
        Exprosion::Create(_fighter->Position(), Color(1.0f,0.1f,0.1f,1.0f), 50, 200, (Object *)_fighter->Parent());
        return;
    }

    // 回転の更新
    UpdateRotation(_fighter);

    // 位置の更新
    UpdatePosition(_fighter);
}

// @brief  : 回転の更新
//--------------------------------------------------------------------
void FighterFallDown::UpdateRotation(Fighter *_fighter)
{
    // �@落下
    Quaternion rot = Quaternion::Sleap(m_RotOld,FALL_ROT,1.0f-m_Time);

    // �AZ軸回転
    m_RotZ *= ROLL_ADD;

    // 合成
    rot *= m_RotZ;

    // 回転の適用
    _fighter->Rotation(rot);
}

// @brief  : 位置の更新
//--------------------------------------------------------------------
void FighterFallDown::UpdatePosition(Fighter *_fighter)
{
    // 目標位置の更新
    const Vector3 vec_z = _fighter->CreateVecZ();
    const Vector3 target(m_PosOld + vec_z * FALL_LENGTH);

    // 線形補完
    _fighter->Position(Math::Leap(m_PosOld,target,1.0f-m_Time));
}




/******************************************************************************
|   End of File
*******************************************************************************/