/*
# press.cpp

- Press表示クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "press.h"
#include <string>
#include "title.h"
#include "app/app.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "renderer/2d/transfrom.h"
#include "renderer/2d/sprite2d.h"
#include "../helper/drawLayerManager.h"

namespace title {

// @brief  : コンストラクタ
// @param  : 親オブジェクト
//--------------------------------------------------------------------
Press::Press(Title *_parent) :
    Object(_parent,true),
    m_AlphaSpeed(0.0f)
{
    const std::string BG_NAME("data/TEXTURE/pressBg.tga");
    const std::string PRESS_NAME("data/TEXTURE/press.tga");
    const Quad        POLYGON_SIZE(-256.0f,0.0f,-56.0f,0.0f);
    const Quad        TEXTURE_SIZE(0.5f/-POLYGON_SIZE.left,1.0f,0.5f/-POLYGON_SIZE.top,1.0f);
    auto renderer = App::Instance().Renderer();
    auto factory  = renderer->Factory2D();

    // Transform作成
    m_Trans = factory->CreateTransform(DrawLayerManager::Instance().Get2DLayer(),true);
    m_Trans->Position = Vector2(renderer->Width,renderer->Height + POLYGON_SIZE.top);

    // Sprite作成
    factory->CreateSprite(POLYGON_SIZE,TEXTURE_SIZE,BG_NAME,m_Trans,true);
    m_Sprite = factory->CreateSprite(POLYGON_SIZE,TEXTURE_SIZE,PRESS_NAME,m_Trans,true);
}
// @brief  : デストラクタ
//--------------------------------------------------------------------
Press::~Press()
{
    m_Trans->IsDestroy = true;
}

// @brief  : 更新
//--------------------------------------------------------------------
void Press::Update(void)
{
    auto renderer = App::Instance().Renderer();

    // ブルブルさせる
    Vector2 pos = m_Trans->Position;
    {
        const Vector2 BASE(renderer->Width,renderer->Height -56.0f);
        const Vector2 MAX_MOVE(2.0f,2.0f);
        auto randf = [](){return ((float)rand())/(float)(RAND_MAX);};

        pos.x = MAX_MOVE.x * randf() + BASE.x;
        pos.y = MAX_MOVE.y * randf() + BASE.y;
    }

    // アルファ値の変更
    Color color = m_Sprite->PolygonColor;
    {

        m_AlphaSpeed += (0.0f - color.a) * 0.001f;
        color.a += m_AlphaSpeed;

    }

    // 適用
    m_Trans->Position = pos;
    m_Sprite->PolygonColor = color;
}



};  // namespace title
/******************************************************************************
|   End of File
*******************************************************************************/