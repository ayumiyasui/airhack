/*
# overlookPlayer.cpp

- リプレイ再生クラス - 俯瞰カメラ(プレイヤーに注目)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "overlookPlayer.h"
#include "../fighter.h"
#include "../../helper/drawLayerManager.h"
#include "math/math.h"

namespace play_replay {

// @brief  : 定数
//--------------------------------------------------------------------
const Vector3 OverLookPlayer::EYE(0.0f,500.0f,-300.0f);   // プレイヤーからの相対位置

// @brief  : コンストラクタ
//--------------------------------------------------------------------
OverLookPlayer::OverLookPlayer()
{
    // カメラの取得
    auto camera = DrawLayerManager::Instance().GetCamera();
    camera->Eye = EYE;
    camera->At  = Vector3::ZERO;
    camera->Up  = Vector3(0.0f,5.0f,7.0f).Normalize();

    camera->Near = 10.0f;
    camera->Far  = 100000.0f;
    camera->FovY = Math::PI / 2.0f;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
OverLookPlayer::~OverLookPlayer()
{
}

// @brief  : 更新
//--------------------------------------------------------------------
void OverLookPlayer::Update(const PlayReplay *const _replay)
{
    // プレイヤーの取得
    auto player = _replay->GetPlayer();
    const Vector3 player_pos = player->Position();

    // カメラの取得
    auto camera = DrawLayerManager::Instance().GetCamera();

    // カメラの注視点を作成
    camera->At = player_pos;

    // 視点の確定
    const Vector3 eye_old = camera->Eye;
    camera->Eye = player_pos+EYE;
}


};    // namespace play_replay
/******************************************************************************
|   End of File
*******************************************************************************/