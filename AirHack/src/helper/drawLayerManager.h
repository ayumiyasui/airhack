/*
# drawLayerManager.h

- 描画階層マネージャクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __DRAWLAYERMANAGER_H__
#define __DRAWLAYERMANAGER_H__

#include "base/tempSingleton.h"
#include "renderer/2d/camera.h"
#include "renderer/3d/camera.h"

class DrawLayerManager : public TempSingleton<DrawLayerManager>
{
public:
    // @brief  : フェードレイヤの取得
    //--------------------------------------------------------------------
    IDraw *GetFadeLayer( void ){ return m_Fade; };

    // @brief  : 2Dレイヤの取得
    //--------------------------------------------------------------------
    IDraw *Get2DLayer( void ){ return m_Master; };

    // @brief  : カメラの取得
    //--------------------------------------------------------------------
    _3d::Camera *GetCamera( void ){ return m_3D; };

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempSingleton<DrawLayerManager>;

    // メンバ変数
    //--------------------------------------------------------------------
    _3d::Camera *m_3D;            // 3Dレイヤ
    _2d::Camera *m_2D;            // 2Dレイヤ
        IDraw *m_Master;          // 2Dマスタレイヤ
        IDraw *m_Fade;            // フェードレイヤ

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    DrawLayerManager();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~DrawLayerManager();
};


#endif //  __DRAWLAYERMANAGER_H__
/******************************************************************************
|   End of File
*******************************************************************************/