/*
# skyBox.h

- 空クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef _SKYBOX_H_
#define _SKYBOX_H_

#include "base/object.h"
#include "base/tempGetter.h"
#include "renderer/3d/draw3d.h"

class World;
NAMESPACE_3D
    class DirectionalLight;
    class TransLookCamera;
    class Transform;
END_NAMESPACE_3D

class SkyBox : public Object
{
public:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //--------------------------------------------------------------------
    SkyBox( World * );

private:
    // メンバ変数
    //--------------------------------------------------------------------
    _3d::DirectionalLight *m_Light;
    _3d::TransLookCamera  *m_Sun;       // 太陽
    _3d::Transform        *m_Cloud;     // 雲の平行移動

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~SkyBox();
};



#endif //  _SKYBOX_H_
/******************************************************************************
|   End of File
*******************************************************************************/