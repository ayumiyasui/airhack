/*
# normal.h

- 機銃の弾クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_BULLET_H_
#define AIR_HACK_BULLET_H_

#include "base/object.h"
#include "../iBullet.h"
#include "../iBulletManager.h"
#include "renderer/3d/draw3d.h"
#include "base/color.h"
#include "math/vector3.h"

NAMESPACE_3D
    class TransLookCamera;
END_NAMESPACE_3D

class World;
namespace bullet {

class BulletManager : public IBulletManager
{
public:
    // 構造体
    //--------------------------------------------------------------------
    struct SETTING
    {
        Color    Col;      // カラー
        float    Speed;    // スピード
        unsigned Life;     // 体力
        int   ShootRepeat;   // 撃つタイミング
    };

    // @brief  : コンストラクタ
    // @param  : セッティング
    //         : ID
    //         : 親
    //--------------------------------------------------------------------
    BulletManager( const SETTING &,material::variable::u16, World * );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~BulletManager();

    // @brief  : 発射
    // @param  : 位置
    //         : ベクトル
    //--------------------------------------------------------------------
    void Shoot( const Vector3 &, const Vector3 & ) override;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    const SETTING   m_SETTING;      // セッティング
    int             m_ShootTime;    // 打つタイミング
    World          *m_World;        // ワールド
};


class Bullet : public IBullet
{
protected:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend BulletManager;

    // @brief  : コンストラクタ
    // @param  : 位置
    //         : ベクトル(毎フレーム足す量)
    //         : カラー
    //         : ライフ
    //         : 撃った人のID
    //         : World
    //--------------------------------------------------------------------
    Bullet( const Vector3 &, const Vector3 &,const Color &, unsigned ,material::variable::u16, World * );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Bullet();

private:
    // メンバ変数
    //--------------------------------------------------------------------
    _3d::TransLookCamera *m_Trans;
    Vector3               m_Vec;
    unsigned int          m_Life;

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update( void ) override;
};


};  // end namespace bullet
#endif //  AIR_HACK_BULLET_H_
/******************************************************************************
|   End of File
*******************************************************************************/