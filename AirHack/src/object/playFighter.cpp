/*
# playFighter.cpp

- リプレイ再生用戦闘機

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "playFighter.h"
#include "playReplay.h"

// @brief  : コンストラクタ
// @param  : リプレイデータ
//         : 操作する戦闘機
//         : リプレイクラス(親)
//--------------------------------------------------------------------
PlayFighter::PlayFighter(data::Replay::FighterData *const _replay, Fighter *const _fighter, PlayReplay *_parent) :
    Object(_parent,true),
    m_Replay(_replay),
    m_Fighter(_fighter)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
PlayFighter::~PlayFighter()
{
}

// @brief  : 更新
//--------------------------------------------------------------------
void PlayFighter::Update(void)
{
    m_Fighter->Accele(m_Replay->GetAccele());
    m_Fighter->Brake(m_Replay->GetBrake());
    m_Fighter->Pitch(m_Replay->GetPitch());
    m_Fighter->Yaw(m_Replay->GetYaw());
    m_Fighter->Roll(m_Replay->GetRoll());
    if(m_Replay->GetShoot()) m_Fighter->Shoot();
    if(m_Replay->GetFallDown()) m_Fighter->FallDown();
}

// @brief  : オブジェクト更新開始時
//--------------------------------------------------------------------
void PlayFighter::OnEnable(void)
{
    m_Fighter->IsUpdate = true;
    m_Fighter->Reset();
}

// @brief  : オブジェクト更新終了時
//--------------------------------------------------------------------
void PlayFighter::OnDisable(void)
{
    m_Fighter->IsUpdate = false;
}



/******************************************************************************
|   End of File
*******************************************************************************/