/*
# iMissle.h

- ミサイルクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_MISSLE_LAUNCHER_H_
#define AIR_HACK_MISSLE_LAUNCHER_H_

#include "iMissle.h"

class World;
class IMissleLauncher
{
public:
    // メンバ定数
    //--------------------------------------------------------------------
    const material::variable::u16 ShooterID;

    // @brief  : コンストラクタ
    // @param  : 撃った戦闘機ID
    //         : 親クラス
    //--------------------------------------------------------------------
    IMissleLauncher(const material::variable::u16 _id,World *_parent) :
        m_World(_parent), ShooterID(_id){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IMissleLauncher(){};

    // @brief  : ワールドのポインタを取得
    //--------------------------------------------------------------------
    World *const GetWorld(void) const{return m_World;};

    // @brief  : 撃つ
    // @param  : 初期位置
    //         : 方向
    //--------------------------------------------------------------------
    virtual void Shoot(const Vector3 &_start, const Vector3 &_vector) = 0;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    World *m_World; // ワールド座標
};



#endif //  AIR_HACK_MISSLE_LAUNCHER_H_
/******************************************************************************
|   End of File
*******************************************************************************/