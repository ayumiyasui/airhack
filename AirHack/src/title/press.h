/*
# press.h

- Press表示クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_TITLE_PRESS_H_
#define AIR_HACK_TITLE_PRESS_H_

#include "base/object.h"
#include "renderer/2d/transfrom.h"
#include "renderer/2d/sprite2d.h"

class Title;
namespace title {

class Press : public Object
{
public:
    // @brief  : コンストラクタ
    // @param  : 親オブジェクト
    //--------------------------------------------------------------------
    Press(Title *);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Press();

private:
    // メンバ変数
    //--------------------------------------------------------------------
    _2d::Transform *m_Trans;
    _2d::Sprite    *m_Sprite;
    float           m_AlphaSpeed;   // アルファ値が変わる速さ

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(void) override;
};


};  // namespace title
#endif //  AIR_HACK_TITLE_PRESS_H_
/******************************************************************************
|   End of File
*******************************************************************************/