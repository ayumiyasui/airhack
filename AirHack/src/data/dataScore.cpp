/*
# dataScore.cpp

- スコアデータ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "score.h"

namespace data {
// @brief  : コンストラクタ
//--------------------------------------------------------------------
Score::Score() :
    IData(TYPE_SCORE)
{
    m_UserData.value = 0;
    m_HighScore.value = 0;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Score::~Score()
{
}

// @brief ユーザーデータの取得
//--------------------------------------------------------------------
material::variable::u16 Score::GetUserData(void) const
{
    return m_UserData.value;
}

// @brief ハイスコアの取得
//--------------------------------------------------------------------
material::variable::u16 Score::GetHighScore(void) const
{
    return m_HighScore.value;
}

// @brief ユーザーデータの保存
//--------------------------------------------------------------------
void Score::SetUserData(material::variable::u16 _value)
{
    m_UserData.value = _value;
}

// @brief  : ハイスコア更新チェック
// @return : ハイスコア更新ならtrue
// @note   : ハイスコアの中身も変わる
//--------------------------------------------------------------------
bool Score::UpdateHighScore(void)
{
    bool is_high_score = m_UserData.value > m_HighScore.value;
    if(is_high_score) m_HighScore.value = m_UserData.value;
    return is_high_score;
}


// @brief  : データのセーブ
// @param  : ファイルポインタ
//--------------------------------------------------------------------
void Score::Save(FILE *_file)
{
    fwrite((const void *)&m_HighScore,sizeof(m_HighScore),1,_file);
}

// @brief  : データのロード
// @param  : ファイルポインタ
//--------------------------------------------------------------------
void Score::Load(FILE *_file)
{
    fread((void *)&m_HighScore,sizeof(m_HighScore),1,_file);
}


};  // namespace data
/******************************************************************************
|   End of File
*******************************************************************************/