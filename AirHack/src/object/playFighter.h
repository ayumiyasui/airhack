/*
# playFighter.h

- リプレイ再生用戦闘機

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_PLAY_FIGHTER_H_
#define AIR_HACK_PLAY_FIGHTER_H_

#include "../data/dataReplay.h"
#include "../object/fighter.h"


class PlayReplay;
class PlayFighter : public Object
{
public:
    // @brief  : コンストラクタ
    // @param  : リプレイデータ
    //         : 操作する戦闘機
    //         : リプレイクラス(親)
    //--------------------------------------------------------------------
    PlayFighter(data::Replay::FighterData *const, Fighter *const, PlayReplay *);

private:
    // メンバ変数
    //--------------------------------------------------------------------
    data::Replay::FighterData *const m_Replay;  // リプレイ
    Fighter                   *const m_Fighter; // 戦闘機

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~PlayFighter();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(void) override;

    // @brief  : オブジェクト更新開始時
    //--------------------------------------------------------------------
    void OnEnable(void) override;

    // @brief  : オブジェクト更新終了時
    //--------------------------------------------------------------------
    void OnDisable(void) override;
};



#endif // AIR_HACK_PLAY_FIGHTER_H_
/******************************************************************************
|   End of File
*******************************************************************************/