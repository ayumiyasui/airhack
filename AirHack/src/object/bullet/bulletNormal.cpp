/*
# bulletNormal.cpp

- 機銃の弾クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "normal.h"

#include "../world.h"
#include "../fighter.h"
#include "../../helper/drawLayerManager.h"
#include "app/app.h"
#include "renderer/renderer.h"
#include "renderer/3d/factory.h"
#include "renderer/3d/camera.h"
#include "renderer/3d/transLookCamera3d.h"
#include "renderer/3d/plate.h"
#include "renderer/3d/material.h"
#include "../exprosion.h"

namespace bullet {


// @brief  : コンストラクタ
// @param  : 設定
//         : ID
//         : 親(ワールド)
//--------------------------------------------------------------------
BulletManager::BulletManager( const SETTING &_setting, material::variable::u16 _id, World *_world ) :
    IBulletManager(_id),
    m_SETTING(_setting),
    m_ShootTime(m_SETTING.ShootRepeat),
    m_World(_world)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
BulletManager::~BulletManager()
{
}

// @brief  : 発射
// @param  : 位置
//         : ベクトル
//         : スピード
//--------------------------------------------------------------------
void BulletManager::Shoot( const Vector3 &_pos, const Vector3 &_vec )
{
    if(--m_ShootTime > 0) return;
    m_ShootTime = m_SETTING.ShootRepeat;

    // ベクトル計算
    const Vector3 vec = _vec * m_SETTING.Speed;

    // 作成
    new Bullet(_pos,vec,m_SETTING.Col,m_SETTING.Life,SHOOTER_ID,m_World);
}

// @brief  : コンストラクタ
// @param  : 位置
//         : ベクトル(毎フレーム足す量)
//         : ライフ
//         : 親のScene
//--------------------------------------------------------------------
Bullet::Bullet( const Vector3 &_pos, const Vector3 &_vec,const Color &_col, unsigned _life,material::variable::u16 _id, World *_world ) :
    IBullet(_id,_world),
    m_Vec(_vec),
    m_Life(_life)
{
    _3d::IFactory *factory = App::Instance().Renderer()->Factory3D();
    _3d::Camera   *cam     = DrawLayerManager::Instance().GetCamera();

    m_Trans = factory->CreateTransformLookCamera(cam,cam,true);
    m_Trans->Position      = _pos;

    _3d::Plate          *plate = factory->CreatePlate("data/TEXTURE/circle.tga",Quad(-10.0f,10.0f,-10.0f,10.0f),m_Trans,true);
    _3d::MaterialNormal *mat   = plate->Mat();

    mat->Diffuse    = _col;
    mat->Emissive   = _col;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Bullet::~Bullet()
{
    m_Trans->IsDestroy = true;
    m_Trans->IsEnable  = false;
}

// @brief  : 更新
//--------------------------------------------------------------------
void Bullet::Update( void )
{
    // ライフがなければ消える
    if(--m_Life <= 0)
    {
        IsUpdate  = false;
        IsDestroy = true;

        return;
    }

    // 位置移動
    const Vector3 pos_old = m_Trans->Position;
    m_Trans->Position = pos_old + m_Vec;

    // 戦闘機との当たり判定
    bool is_hit = CheakHitFighter(pos_old,m_Trans->Position);

    // 当たったら消える
    if(is_hit)
    {
        IsDestroy = true;
        return;
    }
}



};  // end namespace bullet
/******************************************************************************
|   End of File
*******************************************************************************/