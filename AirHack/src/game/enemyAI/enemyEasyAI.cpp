/*
# enemyEasyAI.cpp

- 敵クラス(簡易AI)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "easyAI.h"

#include <assert.h>
#include <math.h>
#include "../player.h"
#include "../../object/fighter.h"
#include "math/math.h"
#include "math/vector2.h"
#include "math/vector3.h"

namespace enemy {

// プレイヤーを追いかけるだけのクラス
//--------------------------------------------------------------------
class PlayerChase : public EasyAI::IState
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    PlayerChase(){};

    // @brief  : 更新
    // @param  : 親(AI)
    //         : 親の親(Enemy)
    //--------------------------------------------------------------------
    void Update(EasyAI *_ai,Enemy *_enemy) override
    {
        // 自分が倒れていたら終了
        if(_enemy->Airframe()->Life <= 0)
        {
            _enemy->FallDown();
            return;
        }

        // プレイヤーを追いかける
        Chase(
            _enemy->Airframe()->Position(),
            _ai->ChaseObject()->Position(),
            _enemy->Airframe(),
            _enemy->Replay()
        );
    };

private:
    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~PlayerChase(){};

    // @brief  : 追いかける
    // @param  : 自分の位置
    //         : 相手の位置
    //         : 操縦する戦闘機
    //--------------------------------------------------------------------
    void Chase(const Vector3 &_position, const Vector3 &_chase_pos, Fighter * const _fighter, data::Replay::FighterData *const _replay )
    {
        // 自分と相手のベクトルを作成
        const Vector3 vec_scal( _chase_pos - _position );

        // 自分のベクトルを取得
        Vector3 vec_z;

        // ピッチを決める
        vec_z = _fighter->CreateVecZ();
        {
            const Vector2 p = Vector2(vec_z.z,vec_z.y).Normalize();
            const Vector2 target = Vector2(vec_scal.z,vec_scal.y).Normalize();
            const float t = Vector2::RotationAngle(p,target)/(Math::PI);

            if(-1.0f <= t && t <= 1.0f)
            {
                _fighter->Pitch(t);
                _replay->SetPitch(t);
            }
        }

        // XY軸を見てロールを決める
        vec_z = _fighter->CreateVecZ();
        {
            const Vector2 p = Vector2(vec_z.x,vec_z.y).Normalize();
            const Vector2 target = Vector2(vec_scal.x,vec_scal.y).Normalize();
            const float t = Vector2::RotationAngle(p,target)/(2.0f*Math::PI);

            if(-1.0f <= t && t <= 1.0f)
            {
                _fighter->Roll(t);
                _replay->SetRoll(t);
            }
        }

        // ヨーを決める
        vec_z = _fighter->CreateVecZ();
        {
            const Vector2 p = Vector2(vec_z.x,vec_z.z).Normalize();
            const Vector2 target = Vector2(vec_scal.x,vec_scal.z).Normalize();
            const float t = Vector2::RotationAngle(p,target)/(2.0f*Math::PI);

            if(-1.0f <= t && t <= 1.0f)
            {
                _fighter->Yaw(t);
                _replay->SetYaw(t);
            }
        }
    };
};

// @brief  : コンストラクタ
// @param  : プレイヤーのポインタ
//--------------------------------------------------------------------
EasyAI::EasyAI(const Player *_player) :
    m_Player(_player),
    m_State(new PlayerChase())
{
    assert(m_Player);
    assert(m_State);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
EasyAI::~EasyAI()
{
    delete m_State;
}

// プロパティ
//--------------------------------------------------------------------
const Player *EasyAI::ChaseObject(void) const{ return m_Player; };  // 追いかける対象

// @brief  : 更新
// @param  : 親(enemy)
//--------------------------------------------------------------------
void EasyAI::Update(Enemy *_enemy)
{
    m_State->Update(this,_enemy);
}

// @brief  : ステート変更
//--------------------------------------------------------------------
void EasyAI::ChengeState(IState *_next)
{
    delete m_State;
    m_State = _next;
}



};  // namespace enemy
/******************************************************************************
|   End of File
*******************************************************************************/