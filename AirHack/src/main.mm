/*
 # main.m
 
 - メイン
 
 @author : Ayumi Yasui
 *-----------------------------------------------------------------------------*/

#import <Cocoa/Cocoa.h>
#include "main.h"
#include "title/title.h"
#include "game/game.h"
#include "app/app.h"
#include "renderer/opengl1.0/opengl1.0.h"
#include "audio/openAL.h"

using namespace material;
// @brief  : メイン関数
//------------------------------------------------------------------------------
int main(int argc, const char * argv[])
{
#if DEBUG
    new SceneWakeUp<Game>();
#else
    new SceneWakeUp<Title>();
#endif
    return App::Instance().Run(&OpenGL::Instance(),new openal::Audio());
}


/*************************************************************************
 |   End of File
 **************************************************************************/