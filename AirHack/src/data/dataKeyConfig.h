/*
# dataKeyConfig.h

- キーコンフィングのデータ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIRHACK_DATA_KEY_CONFIG_H_
#define AIRHACK_DATA_KEY_CONFIG_H_

#include "variable/bitName.h"
#if defined(WIN32) || defined(WIN64)
#include "input/input.h"
#elif __APPLE__
#include "input/inputMac.h"
#endif
#include "data.h"

namespace data {

class KeyConfig : public IData
{
public:
    // キー
    //--------------------------------------------------------------------
    enum KEY_CONFIG_LIST
    {
        // キートリガー
        OK = 0,
        PAUSE,
        MAX_KEY_TRIGGER,

        // キーリピート
        CURSOR_UP = MAX_KEY_TRIGGER,
        CURSOR_DOWN,
        MAX_KEY_REPEAT,

        // キープレス
        PITCH_UP = MAX_KEY_REPEAT,
        PITCH_DOWN,
        ROLL_LEFT,
        ROLL_RIGHT,
        YAW_LEFT,
        YAW_RIGHT,
        SHOOT,
        ACCELE,
        BRAKE,
        MAX_KEY_PRESS,

        MAX_KEY_CONFIG_LIST = MAX_KEY_PRESS
    };

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    KeyConfig();

    // @brief  : データのセーブ
    // @param  : ファイルポインタ
    //--------------------------------------------------------------------
    void Save(FILE *) override;

    // @brief  : データのロード
    // @param  : ファイルポインタ
    //--------------------------------------------------------------------
    void Load(FILE *) override;

    // @brief  : キーチェック
    // @param  : キー
    //--------------------------------------------------------------------
    bool Is(KEY_CONFIG_LIST) const;

    //// @brief  : キー設定をする
    //// @param  : 設定したいキー
    ////--------------------------------------------------------------------
    //void Set(KEY_CONFIG_LIST);

//    // @brief  : キー設定の取得
//    // @param  : 設定したいキ
//    //--------------------------------------------------------------------
//#if defined(WIN32) || defined(WIN64)
//    const BYTE &Get(KEY_CONFIG_LIST) const;
//#elif __APPLE__
//#endif

private:
    // 定数
    //--------------------------------------------------------------------
#if defined(WIN32) || defined(WIN64)
    static const BYTE INIT_KEY[MAX_KEY_CONFIG_LIST]; // 初期キーコンフィング
#elif __APPLE__
    static const unsigned short INIT_KEY[MAX_KEY_CONFIG_LIST]; // 初期キーコンフィング
#endif

    // セーブデータ
    //--------------------------------------------------------------------
    struct DATA
    {
        material::variable::s8 fmt; // M:OSX W:Windows
#if defined(WIN32) || defined(WIN64)
        BYTE key[MAX_KEY_CONFIG_LIST];
#elif __APPLE__
        unsigned short key[MAX_KEY_CONFIG_LIST];
#endif
    };

    // メンバ変数
    //--------------------------------------------------------------------
    DATA m_Data;    // セーブデータ

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~KeyConfig();
};


};  // namespace data
#endif //  AIRHACK_DATA_KEY_CONFIG_H_
/******************************************************************************
|   End of File
*******************************************************************************/