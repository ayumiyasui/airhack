/*
# locus.cpp

- 戦闘機の軌跡クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "locus.h"
#include "app/app.h"
#include "renderer/renderer.h"
#include "renderer/vertex/factory.h"
#include "../helper/drawLayerManager.h"
#include "math/matrix3d.h"
#include "renderer/effect/factory.h"
#include "renderer/vertex/draw.h"

using namespace material;
using namespace material::vertex;

// @brief  : コンストラクタ
// @param  :  ローカル位置�@
//         :  ローカル位置�A
//--------------------------------------------------------------------
Locus::Locus(const Vector3 &_p1, const Vector3 &_p2) :
    LOCAL_POS_1(_p1),
    LOCAL_POS_2(_p2),
    LIFE(60*3),
    ADD_LIFE(-1.0f/LIFE),
    m_Color(1.0f,1.0f,1.0f,1.0f)
{
    auto renderer    = App::Instance().Renderer();
    auto factory_vtx = renderer->Vertex();
    auto factory_eff = renderer->Effect();
    auto layer       = DrawLayerManager::Instance().GetCamera();

    // 頂点の作成
    m_VertexBuffer = factory_vtx->Create(LIFE*2);
    m_VertexBuffer->Edit(
        [&](IVertex3dCol::VERTEX &_vtx,const variable::u32 _i){
            _vtx.pos = Vector3::ZERO;
            _vtx.nor = Vector3::UP;
            _vtx.col = m_Color;
            _vtx.col.a = 0.0f;
            _vtx.tex = Vector2::ZERO;
    });

    // 描画オブジェクトの作成
    auto curring = factory_eff->CreateCurring(layer);
    curring->Set(effect::Curring::TYPE_NONE);
    auto vertex = new DrawVertex(m_VertexBuffer,IVertex::TYPE_TRIANGLE_STRIP,curring);
    m_Draw = curring;
}

// @brief  : コンストラクタ
// @param  : ローカル位置�@
//         : ローカル位置�A
//         : 消えるまでの時間(フレーム)
//         : 色
//--------------------------------------------------------------------
Locus::Locus(const Vector3 &_p1, const Vector3 &_p2, int _life, const Color &_col) :
    LOCAL_POS_1(_p1),
    LOCAL_POS_2(_p2),
    LIFE(_life),
    ADD_LIFE(-1.0f/LIFE),
    m_Color(_col)
{
    auto renderer    = App::Instance().Renderer();
    auto factory_vtx = renderer->Vertex();
    auto factory_eff = renderer->Effect();
    auto layer       = DrawLayerManager::Instance().GetCamera();

    // 頂点の作成
    m_VertexBuffer = factory_vtx->Create(LIFE*2);
    m_VertexBuffer->Edit(
        [&](IVertex3dCol::VERTEX &_vtx,const variable::u32 _i){
            _vtx.pos = Vector3::ZERO;
            _vtx.nor = Vector3::UP;
            _vtx.col = m_Color;
            _vtx.col.a = 0.0f;
            _vtx.tex = Vector2::ZERO;
    });

    // 描画オブジェクトの作成
    auto curring = factory_eff->CreateCurring(layer);
    curring->Set(effect::Curring::TYPE_NONE);
    auto vertex = new DrawVertex(m_VertexBuffer,IVertex::TYPE_TRIANGLE_STRIP,curring);
    m_Draw = curring;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Locus::~Locus()
{
    m_Draw->IsDestroy = true;
}

// @brief  : 更新
// @param  : 親の戦闘機
//         : 初期アルファ値
//--------------------------------------------------------------------
void Locus::Update(const Fighter *const _fighter, float _alpha)
{
    // 行列の作成
    const auto  rot = Matrix3D(_fighter->Rotation());
    const auto &pos = _fighter->Position();
    const auto  mtx =  Matrix3D(
        1.0f,0.0f,0.0f,pos.x,
        0.0f,1.0f,0.0f,pos.y,
        0.0f,0.0f,1.0f,pos.z,
        0.0f,0.0f,0.0f,1.0f
        ) * rot;

    Update(mtx,_alpha);
}

// @brief  : 更新
// @param  : 行列
//         : 初期アルファ値
//--------------------------------------------------------------------
void Locus::Update(const Matrix3D &_mtx, float _alpha)
{
    Vector3 pos_old_1 = LOCAL_POS_1 * _mtx;
    Vector3 pos_old_2 = LOCAL_POS_2 * _mtx;

    // 一つずつずらしながら頂点を更新
    bool switch_p = false;
    auto edit_pos = [&](const Vector3 &_vec,const variable::u32 &_i) -> Vector3{
        Vector3 &select = (switch_p) ? pos_old_1 : pos_old_2;
        Vector3 re = select;
        select = _vec;
        switch_p = !switch_p;
        return re;
    };
    m_VertexBuffer->EditPos(edit_pos);

    auto edit_col = [&,this](const Color &_col, const variable::u32 &_i) -> Color{
        Color re = m_Color;
        re.a = _alpha;
        _alpha = _col.a + ADD_LIFE;
        return re;
    };
    m_VertexBuffer->EditCol(edit_col);
}


// @brief  : リセット
//--------------------------------------------------------------------
void Locus::Reset(const Fighter *const _fighter)
{
    auto edit_pos = [_fighter](const Vector3 &_vec,const variable::u32 &_i) -> Vector3{
        Vector3 re = _fighter->Position();
        return re;
    };
    m_VertexBuffer->EditPos(edit_pos);

    auto edit_col = [this](const Color &_col, const variable::u32 &_i) -> Color{
        Color col = m_Color;
        col.a = 0.0f;
        return col;
    };
    m_VertexBuffer->EditCol(edit_col);
}



/******************************************************************************
|   End of File
*******************************************************************************/