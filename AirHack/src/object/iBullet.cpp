/*
# iBullet.cpp

- 機銃の弾クラス(抽象)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "iBullet.h"
#include "world.h"

// @brief  : コンストラクタ
// @param  : 撃った戦闘機ID
//         : ワールド
//--------------------------------------------------------------------
IBullet::IBullet(material::variable::u16 _id, World *_parent) :
    Object(_parent,true),
    m_ShooterID(_id)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
IBullet::~IBullet()
{
}

// @brief  : 戦闘機との当たり判定
// @param  : 自分の前の位置
//         : 自分の現在の位置
// @return : 当たった
// @note   : 派生先Update内で使用。
//--------------------------------------------------------------------
bool IBullet::CheakHitFighter(const Vector3 &_start, const Vector3 &_end) const
{
    // 親 = ワールドのはずなので取得
    auto *world = dynamic_cast<World *>(Parent());

    // 当たったのかを保存する
    bool is_hit = false;

    // 自分の打った弾なら当たらない
    auto is_othter_fighter =
        [this](Fighter *_fighter) ->bool{
            return (_fighter->ID != m_ShooterID);
        };

    // 戦闘機との当たり判定
    auto cheak_hit_bullet =
        [&_start,&_end](Fighter *_fighter) ->bool{
            return _fighter->CheakHitBullet(_start,_end);
        };

    // ループ処理
    world->FighterForEach(
        [&](Fighter *_fighter) {
            if(!is_hit)
            {
                if(is_othter_fighter(_fighter))
                {
                    is_hit = cheak_hit_bullet(_fighter);
                }
            }
        }
    );

    return is_hit;
}



/******************************************************************************
|   End of File
*******************************************************************************/