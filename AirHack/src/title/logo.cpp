/*
# logo.cpp

- タイトルシーンで使用するロゴクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "logo.h"
#include <string>
#include "title.h"
#include "app/app.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "renderer/2d/transfrom.h"
#include "renderer/2d/sprite2d.h"
#include "../helper/drawLayerManager.h"

class Title;
namespace title {

// @brief  : コンストラクタ
// @param  : 親オブジェクト
//--------------------------------------------------------------------
Logo::Logo(Title *_parent) :
    Object(_parent,true)
{
    const std::string TEXTURE_NAME("data/TEXTURE/title.tga");
    const Quad        POLYGON_SIZE(0.0f,256.0f,0.0f,83.0f);
    const Quad        TEXTURE_SIZE(0.5f/POLYGON_SIZE.right,1.0f,0.5f/POLYGON_SIZE.bottom,1.0f);
    auto renderer = App::Instance().Renderer();
    auto factory  = renderer->Factory2D();

    // Transform作成
    m_Trans = factory->CreateTransform(DrawLayerManager::Instance().Get2DLayer(),true);
    m_Trans->Position = Vector2(0.0f,0.0f);

    // Sprite作成
    factory->CreateSprite(POLYGON_SIZE,TEXTURE_SIZE,TEXTURE_NAME,m_Trans,true);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Logo::~Logo()
{
    m_Trans->IsDestroy = true;
}

// @brief  : 更新
//--------------------------------------------------------------------
void Logo::Update(void)
{
    // ブルブルさせる
    const Vector2 MAX_MOVE(-2.0f,-2.0f);
    auto randf = [](){return ((float)rand())/(float)(RAND_MAX);};

    Vector2 pos = m_Trans->Position;
    pos.x = MAX_MOVE.x * randf();
    pos.y = MAX_MOVE.y * randf();

    // 適用
    m_Trans->Position = pos;
}



};  // namespace title
/******************************************************************************
|   End of File
*******************************************************************************/