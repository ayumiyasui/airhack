/*
# follow.h

- リプレイ再生クラス - ある角度から追いかける

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_PLAY_REPLAY_FOLLOW_H_
#define AIR_HACK_PLAY_REPLAY_FOLLOW_H_

#include "../playReplay.h"
#include "math/quaternion.h"
#include "math/vector3.h"

class Fighter;
namespace play_replay {

class Follow : public ICameraState
{
public:
    // メンバ定数
    //--------------------------------------------------------------------
    const Vector3 EYE;      // 視点
    const Vector3 AT;       // 注視点
    const float SLEP_TIME;  // 球面補間の割合

    // @brief  : コンストラクタ
    // @param  : 戦闘機
    //         : ローカル視点
    //         : ローカル注視点
    //         : 球面補間の割合
    //--------------------------------------------------------------------
    Follow(const Fighter *const _fighter, const Vector3 &_eye, const Vector3 &_at, float _t);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Follow();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(const PlayReplay *const) override;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    Quaternion m_NowRotation;   // 現在の補完
};



};    // namespace play_replay
#endif //  AIR_HACK_PLAY_REPLAY_FOLLOW_H_
/******************************************************************************
|   End of File
*******************************************************************************/