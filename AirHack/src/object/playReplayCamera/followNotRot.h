/*
# followNotRot.h

- リプレイ再生クラス - 注視点のみを追いかけ、視点は固定

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_PLAY_REPLAY_FOLLOW_NOT_ROT_H_
#define AIR_HACK_PLAY_REPLAY_FOLLOW_NOT_ROT_H_

#include "../playReplay.h"
#include "math/vector3.h"

class Fighter;
namespace play_replay {

class FollowNotRot : public ICameraState
{
public:
    // メンバ定数
    //--------------------------------------------------------------------
    const Vector3 EYE;      // 視点
    const Vector3 AT;       // 注視点
    const float LEAP_TIME;  // 線形補間の割合

    // @brief  : コンストラクタ
    // @param  : 戦闘機
    //         : ローカル視点
    //         : ローカル注視点
    //         : 線形補間の割合
    //--------------------------------------------------------------------
    FollowNotRot(const Fighter *const _fighter, const Vector3 &_eye, const Vector3 &_at, float _t);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~FollowNotRot();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(const PlayReplay *const) override;
};



};    // namespace play_replay
#endif //  AIR_HACK_PLAY_REPLAY_FOLLOW_NOT_ROT_H_
/******************************************************************************
|   End of File
*******************************************************************************/