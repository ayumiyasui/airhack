/*
# game.cpp

- ゲームシーンクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "game.h"

#include "../object/world.h"
#include "../helper/drawLayerManager.h"
#include "player.h"
#include "enemyManager.h"
#include "ui/map.h"
#include "ui/score.h"
#include "ui/time.h"
#include "ui/uiLife.h"
#include "../title/title.h"
#include "../result/result.h"
#include "app/app.h"
#include "math/math.h"
#include "math/quad.h"
#include "math/vector2.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "renderer/2d/transfrom.h"
#include "renderer/2d/sprite2d.h"
#include "../data/manager.h"
#include "../data/dataKeyConfig.h"
#include "../object/stageConstant.h"

// @brief  : コンストラクタ
// @param  : 親オブジェクト
//         : フェード
//--------------------------------------------------------------------
Game::Game( Object *_parent,Fade *_fade ) :
    Scene(_parent,_fade),
    m_Pause(nullptr),
    m_IsPausing(false),
    m_PauseIsDestory(true)
{
    auto &data_manager = data::Manager::Instance();
    m_Replay = data_manager.Get<data::Replay>();
    m_KeyConfig = data_manager.Get<data::KeyConfig>();
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Game::~Game()
{
    if(!m_PauseIsDestory)
    {
        m_Pause->IsDestroy       = true;
        m_Pause->m_GameIsDestroy = true;
    }
    m_BGM->Stop();
}

// @brief  : Enableになった時の初期化
//--------------------------------------------------------------------
void Game::OnEnable( void )
{
    if(m_Pause) return;

    auto app   = &App::Instance();
    auto audio = app->Audio();

    // ポーズクラスの作成
    m_Pause = new Pause(this);

    // マップの作製
    new ui::Map(this);

    // スコアの作成
    auto score = new ui::Score(this);

    // タイマーの作成
    auto stage = &StageConstant::Instance();
    const material::variable::u16 time = stage->GetTime();
    m_Time = new ui::Time(time,this);

    // ワールドの作成
    auto world = new World(this);

    // リプレイデータのリセット
    m_Replay->Reset();
    m_Replay->SetStage(time);

    // プレイヤーの作成
    Player *player;
    {
        auto player_fighter = stage->GetPlayer();
        auto fighter = world->CreateFighter(player_fighter.front(),this);
        player = new Player(world,fighter);

        // ui
        new ui::Life(fighter,this);
    }

    // 敵の作成
    auto enemy_list = stage->GetEnemy();
    m_Enemy = new EnemyManager(enemy_list,this,player,world,score);

    m_BGM = audio->LoadBGM("data/SOUND/game.wav");
    m_BGM->Play();
}

// @brief  : 更新
//--------------------------------------------------------------------
void Game::Update( void )
{
    if(m_KeyConfig->Is(data::KeyConfig::PAUSE))
    {
        m_Pause->IsUpdate = true;
        m_IsPausing = true;
    }

    // リプレイカウントの更新
    m_Replay->Update();

    // 数が0になったらリザルトへ
    if(m_Enemy->IsAnnihilation()) ChengeNext<Result>();

    // カウントが0になったらリザルトへ
    if(m_Time->GameIsEnd()) ChengeNext<Result>();

    UpdateScene();
}



/******************************************************************************
|   End of File
*******************************************************************************/