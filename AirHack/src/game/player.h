/*
# player.h

- プレイヤークラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "base/object.h"
#include "renderer/3d/draw3d.h"
#include "../data/dataReplay.h"
#include "renderer/2d/transfrom.h"

class Fighter;
class Vector3;
class Matrix3D;
class Quaternion;

NAMESPACE_3D
    class Camera;
END_NAMESPACE_3D

namespace data {
    class KeyConfig;
};

class IPlayerCamera;

class Player : public Object
{
public:
    // 定数
    //--------------------------------------------------------------------
    static const float ADD_VIRTUAL_CONTROL;  // バーチャルコントロールに入力する値
    static const float AUTO_VIRTUAL_CONTROL_LEAP; // バーチャルコントロールに入力する値

    // プロパティ
    //--------------------------------------------------------------------
    const Matrix3D   &Matrix(void)   const;
    const Quaternion &Rotation(void) const;
    const Vector3    &Position(void) const;

    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 選択した機体
    //--------------------------------------------------------------------
    Player( Object *, Fighter * );

    // @brief  : プレイヤーの機体を取得する
    //--------------------------------------------------------------------
    const Fighter *const GetFighter(void) const{return m_Fighter;};

private:
    // メンバ変数
    //--------------------------------------------------------------------
    Fighter                    *m_Fighter;    // 飛行機
    IPlayerCamera              *m_Camera;     // カメラ操作
    data::KeyConfig            *m_KeyConfig;  // キーコンフィング
    data::Replay::FighterData  *m_Replay;     // リプレイ
    Vector3                     m_VirtualController;    // バーチャルコントローラ
    _2d::Transform            *      m_Focus;   // 弾を撃つ場所

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Player();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(void) override;

    // @brief  : 操縦
    //--------------------------------------------------------------------
    void Control(void);

    // @brief  : カメラ操作
    //--------------------------------------------------------------------
    void Camera(void);
};



#endif //  _PLAYER_H_
/******************************************************************************
|   End of File
*******************************************************************************/