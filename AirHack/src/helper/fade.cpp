/*
# fade.h

- フェードクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "fade.h"

#include "app/app.h"
#include "app/win32.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "renderer/2d/transfrom.h"
#include "renderer/2d/sprite2d.h"
#include "math/vector2.h"
#include "drawLayerManager.h"

// @brief  : コンストラクタ
// @note   : オブジェクト生成はしないこと
//--------------------------------------------------------------------
Fade::Fade() :
    Object(&TempNodeSingleton<Object>::Instance(),false),
    m_Trans(nullptr),
    m_Sprite(nullptr),
    m_Status(FADE_NONE),
    Status(m_Status)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Fade::~Fade()
{
    if(m_Trans) m_Trans->IsDestroy = true;
}

// @brief  : フェードアウト
// @note   : だんだん隠れる
//--------------------------------------------------------------------
void Fade::FadeOut( const Color &_col, unsigned _time )
{
    if(IsUpdate) return;
    IsUpdate = true;
    m_Trans->IsEnable        = true;
    m_Sprite->PolygonColor    = _col;
    m_Sprite->PolygonColor->a = 0.0f;
    m_AddAlpha  = _col.a / _time;
    m_Time      = _time;
    m_Status    = FADE_OUT;
}

// @brief  : フェードイン
// @note   : だんだん現れる
//--------------------------------------------------------------------
void Fade::FadeIn( const Color &_col, unsigned _time )
{
    if(IsUpdate) return;
    IsUpdate = true;
    m_Trans->IsEnable        = true;
    m_Sprite->PolygonColor    = _col;
    m_Sprite->PolygonColor->a = 1.0f;
    m_AddAlpha  = -_col.a / _time;
    m_Time      = _time;
    m_Status    = FADE_IN;
}

// @brief  : Enableになった時の初期化
//--------------------------------------------------------------------
void Fade::OnEnable( void )
{
    const IRenderer     *renderer  = App::Instance().Renderer();
    const _2d::IFactory *factory2d = renderer->Factory2D();
    if(!m_Trans)
    {
        m_Trans = factory2d->CreateTransform(DrawLayerManager::Instance().GetFadeLayer(),false);
        m_Trans->Position = Vector2::ZERO;
        m_Trans->Rotation  = 0.0f;
    }
    if(!m_Sprite)
    {
        m_Sprite = factory2d->CreateSprite(
        Quad(0.0f,renderer->Width,0.0f,renderer->Height),
        Quad(0.0f,1.0f,0.0f,1.0f),
        std::string(),
        m_Trans ,true);
    }
    m_Trans->IsEnable = true;
}

// @brief  : 更新
//--------------------------------------------------------------------
void Fade::Update( void )
{
    --m_Time;
    if(m_Time <= 0)
    {
        // 色の設定
        if(m_Status == FADE_IN)
        {
            m_Sprite->PolygonColor->a = 0.0f;
            m_Status = FADE_NONE;
            m_Trans->IsEnable = false;
            IsUpdate = false;
        }
        else
        {
            m_Sprite->PolygonColor->a = 1.0f;
            m_Status = FADE_NONE;
            IsUpdate = false;
        }
        Color col = m_Sprite->PolygonColor;
        m_Sprite->PolygonColor = col;
        return;
    }

    // 色の設定
    Color col = m_Sprite->PolygonColor;
    col.a += m_AddAlpha;
    m_Sprite->PolygonColor = col;
}



/******************************************************************************
|   End of File
*******************************************************************************/