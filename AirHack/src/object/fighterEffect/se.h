/*
# se.h

- 戦闘機演出クラス(SE)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_FIGHTER_SE_H_
#define AIR_HACK_FIGHTER_SE_H_


#include "../iFighterEffect.h"
#include "audio/iAudio.h"


namespace fighter {

class SE : public IFighterEffect
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    SE();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~SE();

    // @brief  : 機銃を撃つ
    //--------------------------------------------------------------------
    void Shoot(void) override;

    // @brief  : 機銃に当てられた
    //--------------------------------------------------------------------
    void HitBullet(void) override;

    // @brief  : やられる
    //--------------------------------------------------------------------
    void FallDown(void) override;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    material::IAudio::ISource *m_Shoot;
    material::IAudio::ISource *m_HitBullet;
    material::IAudio::ISource *m_FallDown;
};



};  // namespace fighter
#endif //  AIR_HACK_FIGHTER_SE_H_
/******************************************************************************
|   End of File
*******************************************************************************/