/*
# playerCamera.h

- プレイヤーのカメラクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "playerCamera.h"

#include "player.h"
#include "math/vector3.h"
#include "math/matrix3d.h"
#include "math/quaternion.h"
#include "../helper/drawLayerManager.h"
#include "math/math.h"

// @brief  : コンストラクタ
// @param  : 親クラス
//--------------------------------------------------------------------
IPlayerCamera::IPlayerCamera( Player *_parent ) :
    Object(_parent,true)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
IPlayerCamera::~IPlayerCamera()
{
}


// ３人称視点
// 定数
//--------------------------------------------------------------------
const Vector3 PlayerCameraTPS::CAMERA_AT( 0.0f, -5.0f,-1700.0f);    // ローカルカメラ注視点
const Vector3 PlayerCameraTPS::CAMERA_EYE(0.0f, 50.0f,  200.0f);    // ローカルカメラ視点

// @brief  : コンストラクタ
// @param  : 親クラス
//--------------------------------------------------------------------
PlayerCameraTPS::PlayerCameraTPS( Player *_parent ) :
    IPlayerCamera(_parent),
    m_Camera(DrawLayerManager::Instance().GetCamera()),
    m_NowRotation(_parent->Rotation())
{
    // プレイヤーの取得
    const Player *player = dynamic_cast<Player *>(Parent());

    // 行列の取得
    const Matrix3D &mat = player->Matrix();

    m_NowRotation = player->Rotation();

    // 回転行列にする
    Matrix3D rot(m_NowRotation);

    m_Camera->Eye = CAMERA_EYE * mat;
    m_Camera->At  = CAMERA_AT  * mat;
    m_Camera->Up  = Vector3::UP * rot;

    m_Camera->Near = 10.0f;
    m_Camera->Far  = 100000.0f;
    m_Camera->FovY = Math::PI / 3.0f;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
PlayerCameraTPS::~PlayerCameraTPS()
{
}

// @brief  : 更新
//--------------------------------------------------------------------
void PlayerCameraTPS::Update(void)
{
    // プレイヤーの取得
    const Player *player = dynamic_cast<Player *>(Parent());

    // 行列の取得
    const auto &player_rot = player->Rotation();
    auto quat =  Quaternion::Sleap(m_NowRotation,player_rot,0.25f).Normalize();
    m_NowRotation = ( -1.0f <= quat.w && quat.w <= 1.0f ) ? quat : m_NowRotation;
    Matrix3D rot(player_rot);
    Matrix3D rot_sleap(m_NowRotation);

    // 回転
    m_Camera->Eye = CAMERA_EYE  * rot_sleap + player->Position();
    m_Camera->At  = CAMERA_AT   * rot + player->Position();
    m_Camera->Up  = Vector3::UP * rot_sleap;
}



/******************************************************************************
|   End of File
*******************************************************************************/