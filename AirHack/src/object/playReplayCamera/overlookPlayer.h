/*
# overlookPlayer.h

- リプレイ再生クラス - 俯瞰カメラ(プレイヤーに注目)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_PLAY_REPLAY_OVERLOOK_PLAYER_H_
#define AIR_HACK_PLAY_REPLAY_OVERLOOK_PLAYER_H_

#include "../playReplay.h"
#include "math/vector3.h"

namespace play_replay {

class OverLookPlayer : public ICameraState
{
public:
    // @brief  : 定数
    //--------------------------------------------------------------------
    static const Vector3 EYE;   // プレイヤーからの相対位置

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    OverLookPlayer();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~OverLookPlayer();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(const PlayReplay *const);
};

};    // namespace play_replay


#endif //  AIR_HACK_PLAY_REPLAY_OVERLOOK_PLAYER_H_
/******************************************************************************
|   End of File
*******************************************************************************/