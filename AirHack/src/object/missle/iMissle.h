/*
# iMissle.h

- ミサイルクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_MISSLE_H_
#define AIR_HACK_MISSLE_H_

#include "variable/bitName.h"
#include "base/object.h"
#include "math/vector3.h"
#include "../world.h"

class IMissle : public Object
{
public:
    // メンバ定数
    //--------------------------------------------------------------------
    const material::variable::u16 ShooterID;

    // @brief  : 戦闘機との当たり判定
    // @param  : 自分の前の位置
    //         : 自分の現在の位置
    // @return : 当たった
    // @note   : 派生先Update内で使用。
    //--------------------------------------------------------------------
    bool CheakHitFighter(const Vector3 &_start, const Vector3 &_end) const;

protected:
    // @brief  : コンストラクタ
    // @param  : 撃った戦闘機ID
    //         : 親クラス
    //--------------------------------------------------------------------
    IMissle(const material::variable::u16 _id,World *_parent);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IMissle();
};



#endif //  AIR_HACK_MISSLE_H_
/******************************************************************************
|   End of File
*******************************************************************************/