/*
# world.cpp

- ワールドクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "world.h"

#include <algorithm>
#include "field.h"
#include "skyBox.h"

// 定数
//--------------------------------------------------------------------
const Vector3 World::WORLD_SIZE(100000.0f,100000.0f,100000.0f);       // ワールド範囲

// @brief  : コンストラクタ
// @param  : 親クラス
//--------------------------------------------------------------------
World::World(Object *_parent) :
    Object(_parent,true)
{
    new SkyBox(this);
    new Field(this);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
World::~World()
{
    m_FighterList.clear();
}

// @brief  : ファイターの作成
//--------------------------------------------------------------------
Fighter *World::CreateFighter(const Fighter::SETTING &_setting, Object *_parent)
{
    // ファイターの作成
    auto new_fighter = new Fighter(_setting,this,_parent);

    // リストに追加
    m_FighterList.push_back(new_fighter);

    return new_fighter;
}

// @brief  : ファイターの削除
//--------------------------------------------------------------------
void World::DeleteFighter(Fighter *_fighter)
{
    _fighter->IsDestroy = true;
    m_FighterList.remove(_fighter);
}

// @brief  : リストのファイターをすべてループで処理する
//--------------------------------------------------------------------
void World::FighterForEach( std::function<void (Fighter *)> _function )
{
    if(m_FighterList.empty()) return;
    std::for_each(m_FighterList.begin(),m_FighterList.end(),
        [&](Fighter *_fighter) {
            _function(_fighter);
        }
    );
}



/******************************************************************************
|   End of File
*******************************************************************************/