/*
# easyAIShoot.cpp

- 敵クラス(簡易AI - 弾を発射する敵)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "easyAIShoot.h"
#include "../player.h"
#include "math/math.h"


namespace enemy {
// 簡易AI
//-----------------------------------------------------------------------------
// @brief  : コンストラクタ
// @param  : プレイヤーのポインタ
//--------------------------------------------------------------------
EasyAIShoot::EasyAIShoot(const Player *_player) :
    m_Player(_player)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
EasyAIShoot::~EasyAIShoot()
{
}

// @brief  : 更新
// @param  : 親(enemy)
//--------------------------------------------------------------------
void EasyAIShoot::Update(Enemy *_enemy)
{
    // 自分が倒れていたら終了
    if(_enemy->Airframe()->Life <= 0)
    {
        _enemy->FallDown();
        return;
    }

    // プレイヤーを追いかける
    Chase(
        _enemy->Airframe()->Position(),
        m_Player->GetFighter()->Position(),
        _enemy->Airframe(),
        _enemy->Replay()
    );

    // プレイヤーに向かって撃つ
    Shoot(_enemy);
}

// @brief  : 追いかける
// @param  : 自分の位置
//         : 相手の位置
//         : 操縦する戦闘機
//--------------------------------------------------------------------
void EasyAIShoot::Chase(const Vector3 &_position, const Vector3 &_chase_pos, Fighter * const _fighter, data::Replay::FighterData *const _replay )
{
    // 自分と相手のベクトルを作成
    const Vector3 vec_scal( _chase_pos - _position );

    // 自分のベクトルを取得
    Vector3 vec_z;

    // ピッチを決める
    vec_z = _fighter->CreateVecZ();
    {
        const Vector2 p = Vector2(vec_z.z,vec_z.y).Normalize();
        const Vector2 target = Vector2(vec_scal.z,vec_scal.y).Normalize();
        const float t = Vector2::RotationAngle(p,target)/(Math::PI);

        if(-1.0f <= t && t <= 1.0f)
        {
            _fighter->Pitch(t);
            _replay->SetPitch(t);
        }
    }

    // XY軸を見てロールを決める
    vec_z = _fighter->CreateVecZ();
    {
        const Vector2 p = Vector2(vec_z.x,vec_z.y).Normalize();
        const Vector2 target = Vector2(vec_scal.x,vec_scal.y).Normalize();
        const float t = Vector2::RotationAngle(p,target)/(2.0f*Math::PI);

        if(-1.0f <= t && t <= 1.0f)
        {
            _fighter->Roll(t);
            _replay->SetRoll(t);
        }
    }

    // ヨーを決める
    vec_z = _fighter->CreateVecZ();
    {
        const Vector2 p = Vector2(vec_z.x,vec_z.z).Normalize();
        const Vector2 target = Vector2(vec_scal.x,vec_scal.z).Normalize();
        const float t = Vector2::RotationAngle(p,target)/(2.0f*Math::PI);

        if(-1.0f <= t && t <= 1.0f)
        {
            _fighter->Yaw(t);
            _replay->SetYaw(t);
        }
    }
};

// @brief  : プレイヤーに向かって撃つ
//--------------------------------------------------------------------
void EasyAIShoot::Shoot(Enemy * _enemy)
{
    auto player_fighter = m_Player->GetFighter();
    auto enemy_fighter = _enemy->Airframe();
    auto enemy_pos = enemy_fighter->Position();
    auto player_pos = player_fighter->Position();

    // プレイヤーを撃って良いか
    const bool can_shoot_player = player_fighter->IsHitBullet(enemy_pos,player_pos);
    if(can_shoot_player) enemy_fighter->Shoot();
}


};  // namespace enemy
/******************************************************************************
|   End of File
*******************************************************************************/