/*
# playing.h

- リプレイ再生クラス - プレイカメラ再現クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_PLAY_REPLAY_PLAYING_H_
#define AIR_HACK_PLAY_REPLAY_PLAYING_H_

#include "follow.h"

class Fighter;
namespace play_replay {

class Playing : public Follow
{
public:
    // @brief  : コンストラクタ
    // @param  : 戦闘機
    //--------------------------------------------------------------------
    Playing(const Fighter *const);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Playing();
};



};    // namespace play_replay
#endif //  AIR_HACK_PLAY_REPLAY_PLAYING_H_
/******************************************************************************
|   End of File
*******************************************************************************/