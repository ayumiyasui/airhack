/*
# logo.h

- タイトルシーンで使用するロゴクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_TITLE_LOGO_H_
#define AIR_HACK_TITLE_LOGO_H_

#include "base/object.h"
#include "renderer/2d/transfrom.h"

class Title;
namespace title {

class Logo : public Object
{
public:
    // @brief  : コンストラクタ
    // @param  : 親オブジェクト
    //--------------------------------------------------------------------
    Logo(Title *);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Logo();

private:
    // メンバ変数
    //--------------------------------------------------------------------
    _2d::Transform *m_Trans;

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update( void ) override;
};


};  // namespace title
#endif //  AIR_HACK_TITLE_LOGO_H_
/******************************************************************************
|   End of File
*******************************************************************************/