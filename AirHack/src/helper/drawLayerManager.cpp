/*
# drawLayerManager.cpp

- 描画階層マネージャクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "drawLayerManager.h"

#include "app/app.h"
#include "app/win32Macro.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "renderer/3d/factory.h"
#include "renderer/2d/camera.h"
#include "renderer/3d/camera.h"

// @brief  : コンストラクタ
//--------------------------------------------------------------------
DrawLayerManager::DrawLayerManager()
{
    // ファクトリクラスの所得
    _2d::IFactory *factory_2d = App::Instance().Renderer()->Factory2D();
    _3d::IFactory *factory_3d = App::Instance().Renderer()->Factory3D();

    // 第一層
    m_3D = factory_3d->CreateCamera(&TempNodeSingleton<IDraw>::Instance(),true);
    m_2D = factory_2d->CreateCamera(&TempNodeSingleton<IDraw>::Instance(),true);

    // 第二層
    m_Master = new IDraw(m_2D,true);
    m_Fade   = new IDraw(m_2D,true);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
DrawLayerManager::~DrawLayerManager()
{
}


/******************************************************************************
|   End of File
*******************************************************************************/