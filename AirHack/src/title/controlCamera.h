/*
# controlCamera.h

- リプレイ用カメラコントロールクラス(title)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_TITLE_CONTROL_CAMERA_H_
#define AIR_HACK_TITLE_CONTROL_CAMERA_H_

#include "../object/playReplay.h"

class Fighter;
namespace title {

class ControlCamera : public play_replay::ICameraState
{
public:
    // @brief  : コンストラクタ
    // @param  : 戦闘機
    //--------------------------------------------------------------------
    ControlCamera(const Fighter *const);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~ControlCamera();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(const PlayReplay *const);

private:
    // 定数
    //--------------------------------------------------------------------
    enum STATE
    {
        FOLLOW_PLAYER =0,
        FOLLOW_RIGHT,
        FOLLOW_LEFT,
        FOLLOW_BACK,
        LOOKUP_UNDER,

        MAX_STATE
    };
    static const int NEXT_STATE_TIME;   // 次のステートまでの時間

    // メンバ変数
    //--------------------------------------------------------------------
    ICameraState  *      m_State;      // カメラステート
    const Fighter *const m_Fighter;    // ついていく戦闘機
    int                  m_Time;       // どれくらいの時間までか

    // @brief  : ステート変更
    //--------------------------------------------------------------------
    void ChengeState(void);
};



};    // namespace title
#endif //  AIR_HACK_TITLE_CONTROL_CAMERA_H_
/******************************************************************************
|   End of File
*******************************************************************************/