/*
# enemyRotation.cpp

- 敵クラス(一定範囲を回転しているだけ)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "rotation.h"
#include "../../object/fighter.h"


namespace enemy {
// 回転するだけ
//-----------------------------------------------------------------------------
// @brief  : コンストラクタ
// @param  : 回転量
//--------------------------------------------------------------------
Rotation::Rotation(const Vector3 &_rot)
{
    Vector3 vec = _rot.Normalize();
    m_Yaw   = vec.y;
    m_Pitch = vec.x;
    m_Roll  = vec.z;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Rotation::~Rotation()
{
}

// @brief  : 更新
// @param  : 親(enemy)
//--------------------------------------------------------------------
void Rotation::Update(Enemy *_enemy)
{
    // 自分が倒れていたら終了
    if(_enemy->Airframe()->Life <= 0)
    {
        _enemy->FallDown();
        return;
    }

    auto fighter = _enemy->Airframe();
    fighter->Yaw(m_Yaw);
    fighter->Pitch(m_Pitch);
    fighter->Roll(m_Roll);
}



};  // namespace enemy
/******************************************************************************
|   End of File
*******************************************************************************/