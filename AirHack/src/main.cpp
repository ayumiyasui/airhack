/*
# main.cpp

- メインヘッダ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#if defined(WIN32) || defined(WIN64)

//--- インクルード ------------------------------------------------------------
#include "main.h"
#include "app/app.h"
#include "renderer/directx9/directx9.h"
#include "renderer/opengl1.0/opengl1.0.h"
#include "audio/xAudio.h"
#include "renderer/draw.h"
#include "title/title.h"
#include "game/game.h"
#include "result/result.h"
#include "data/manager.h"

//*****************************************************************************
// @brief  : メイン関数
// @param  : HINSTANCE    アプリケーションのメモリのコントロール用
//           HINSTANCE    メモリの排他制御(Win32アプリケーションではNULL)
//           LPSTR        アプリケーションのコマンドラインががある、文字列の場所
//           int          ウィンドウの表示状態。最大化とか最小化とか。
// @return : int          アプリケーションの終了結果
//-----------------------------------------------------------------------------
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{
    data::Manager::Instance().Load();
    auto &renderer = DirectX9::Instance();
#if defined(_DEBUG) || defined(DEBUG)
    new SceneWakeUp<Title>();
#else
    new SceneWakeUp<Title>();
#endif
    auto re = App::Instance().Run(&renderer,new material::xaudio::Audio(),nCmdShow);
    data::Manager::Instance().Save();

    return re;
}


#endif // defined(WIN32) || defined(WIN64)
/*************************************************************************
|   End of File
**************************************************************************/