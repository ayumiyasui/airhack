/*
# uiLife.cpp

- 体力表示クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "uiLife.h"
#include "../game.h"
#include "../../object/fighter.h"
#include "../../helper/drawLayerManager.h"
#include "app/app.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "math/math.h"

namespace ui {
// 定数
//--------------------------------------------------------------------
const std::string Life::LABEL_TEX_NAME("data/TEXTURE/labelLife.tga");
const Vector2     Life::LABEL_SIZE(40.0f,16.0f);
const Vector2     Life::LABEL_POS(70.0f,560.0f);
const Quad        Life::LABEL_POLYGON(LABEL_SIZE.x*-0.5f,LABEL_SIZE.x*0.5f,5.0f,5.0f+LABEL_SIZE.y);
const Vector2     Life::LIFE_SIZE(26.0f,164.0f);
const Quad        Life::LIFE_MAX_POLYGON(LIFE_SIZE.x*-0.5f,LIFE_SIZE.x*0.5f,-LIFE_SIZE.y,0.0f);
const float       Life::LIFE_LEAP = 0.2f;

// @brief  : コンストラクタ
// @param  : 体力を表示する戦闘機
//         : 親クラス
//--------------------------------------------------------------------
Life::Life(const Fighter *const _fighter, Game *_parent) :
    Object(_parent,true),
    m_Fighter(_fighter),
    m_Transform(nullptr),
    m_Gauge(nullptr)
{
    auto app      = &App::Instance();
    auto renderer = app->Renderer();
    auto factory  = renderer->Factory2D();
    const Quad  tex(0.0f,1.0f,0.0f,1.0f);
    const Color col(0.0f,1.0f,0.0f,1.0f);

    // Transform
    m_Transform = factory->CreateTransform(DrawLayerManager::Instance().Get2DLayer(),true);
    m_Transform->Position = LABEL_POS;

    // ゲージ背景
    auto back = factory->CreateSprite(LIFE_MAX_POLYGON,tex,std::string(),m_Transform,true);
    back->PolygonColor = Color(col.r,col.g,col.b,0.5f);

    // ラベル
    auto label = factory->CreateSprite(LABEL_POLYGON,tex,LABEL_TEX_NAME,m_Transform,true);
    label->PolygonColor = col;

    // ゲージ
    m_Gauge = factory->CreateSprite(LIFE_MAX_POLYGON,tex,std::string(),m_Transform,true);
    m_Gauge->PolygonColor = Color(col.r,col.g,col.b,0.5f);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Life::~Life()
{
    m_Transform->IsDestroy = true;
}

// @brief  : 更新
//--------------------------------------------------------------------
void Life::Update(void)
{
    // ライフ%の取得
    const float par = m_Fighter->ParLife();

    // 目標y座標の作成
    const float target_y = (LIFE_MAX_POLYGON.top - LIFE_MAX_POLYGON.bottom) * par + LIFE_MAX_POLYGON.bottom;

    // ポリゴンの設定
    Quad polygon = m_Gauge->PolygonQuad;
    polygon.top  = Math::Leap(polygon.top,target_y,LIFE_LEAP);
    m_Gauge->SetPolygonQuad(polygon);
}



};  // namespace ui
/******************************************************************************
|   End of File
*******************************************************************************/