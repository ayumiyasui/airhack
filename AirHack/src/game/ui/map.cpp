/*
# map.cpp

- ゲーム中の広域マップクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "map.h"
#include "../game.h"
#include "../../object/world.h"
#include "../../object/fighter.h"
#include "renderer/2d/transfrom.h"
#include "renderer/3d/iTransform.h"

#include "app/app.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "renderer/2d/sprite2d.h"
#include "renderer/3d/transform3dQuat.h"
#include "math/quaternion.h"
#include "math/vector2.h"
#include "math/vector3.h"
#include "../../helper/drawLayerManager.h"

namespace ui {
// Mapクラス
//-----------------------------------------------------------------------------

// 内部クラス
//--------------------------------------------------------------------
// @brief  : コンストラクタ
// @param  : アイコンの色
//         : 戦闘機(親クラス)
//--------------------------------------------------------------------
Map::Icon::Icon(const Color &_color,Fighter *_fighter) :
    Object(_fighter,true),
    m_Tracked(_fighter)
{
    auto parent_2d = DrawLayerManager::Instance().Get2DLayer();
    auto factory   = App::Instance().Renderer()->Factory2D();
    m_Icon = factory->CreateTransform(parent_2d,true);
    auto sprite = factory->CreateSprite(Map::FIGHTER_SIZE,Quad(0.0f,1.0f,0.0f,1.0f),
        Map::FIGHTER_TEXTURE_NAME,m_Icon,true);
    sprite->PolygonColor = _color;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Map::Icon::~Icon()
{
    m_Icon->IsDestroy = true;
}

// @brief  : 更新
//--------------------------------------------------------------------
void Map::Icon::Update(void)
{
    // 同期
    // 位置変更
    {
        // 位置を-1.0~1.0単位に変換
        const Vector2 pos_v = CreateWorldPosPar();

        // Mapに変換
        const Vector2 size(Map::BACK_SIZE.right-Map::BACK_SIZE.left,Map::BACK_SIZE.bottom-Map::BACK_SIZE.top);
        const Vector2 pos(
            size.x*pos_v.x + Map::BACK_POS.x,
            size.y*pos_v.y + Map::BACK_POS.y);

        // 位置変更
        m_Icon->Position = pos;
    }

    // 回転変更
    {
        // ベクトルZの取得
        const auto vec_z = m_Tracked->CreateVecZ();

        // ベクトルをXZに分解
        const Vector2 xz(-vec_z.z,vec_z.x);

        // 回転
        m_Icon->Rotation = xz.Angle();
    }
}

// @brief  : 左上を0.0f,右下を1.0fとしたときの位置
//--------------------------------------------------------------------
Vector2 Map::Icon::CreateWorldPosPar(void) const
{
    auto pos = m_Tracked->Position();
    const float x = pos.x / World::WORLD_SIZE.x;
    const float y = pos.z / World::WORLD_SIZE.z;

    return Vector2(x,y);
}

// マップ
// 定数
//--------------------------------------------------------------------
const Vector2     Map::BACK_POS(874.0f+219.0f*0.5f,381.0f+219.0f*0.5f);              // 背景の位置
const Quad        Map::BACK_SIZE(-219.0f*0.5f,219.0f*0.5f,-219.0f*0.5f,219.0f*0.5f);             // 背景の大きさ
const std::string Map::BACK_TEXTURE_NAME("data/TEXTURE/gameMap.tga");     // 背景のテクスチャ名
const Quad        Map::FIGHTER_SIZE(-15.8f*0.4f,15.8f*0.4f,-23.75f*0.4f,23.75f*0.4f);          // 戦闘機の大きさ
const std::string Map::FIGHTER_TEXTURE_NAME("data/TEXTURE/gameMapCursor.tga");  // 戦闘機のテクスチャ名

// @brief  : コンストラクタ
// @param  : 親クラス(ゲームシーン)
//--------------------------------------------------------------------
Map::Map(Game *_parent) :
    Object(_parent,true)
{
    auto parent_2d = DrawLayerManager::Instance().Get2DLayer();
    auto factory   = App::Instance().Renderer()->Factory2D();
    m_BackGround = factory->CreateTransform(parent_2d,true);
    factory->CreateSprite(Map::BACK_SIZE,Quad(0.0f,1.0f,0.0f,1.0f),
        Map::BACK_TEXTURE_NAME,m_BackGround,true);

    m_BackGround->Position = BACK_POS;
}


// @brief  : デストラクタ
//--------------------------------------------------------------------
Map::~Map()
{
    m_BackGround->IsDestroy = true;
}



};  // namespace ui
/******************************************************************************
|   End of File
*******************************************************************************/