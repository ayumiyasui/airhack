/*
# drawScore.h

- リザルト用スコア表示クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_RESULT_DRAW_SCORE_H_
#define AIR_HACK_RESULT_DRAW_SCORE_H_

#include <string>
#include "base/object.h"
#include "math/vector2.h"
class Result;
class DigitWithNumbers;
namespace _2d {
    class Transform;
};

namespace result {
class DrawScore : public Object
{
public:
    // @brief  : コンストラクタ
    // @param  : 親オブジェクト
    //--------------------------------------------------------------------
    DrawScore(Result *);

private:
    // 定数
    //--------------------------------------------------------------------
    static const std::string TEXTURE_NAME;  // テクスチャ名
    static const std::string NUMBER_NAME;   // テクスチャ名
    static const Vector2     BACK_POS;      // 数字表示位置
    static const Vector2     BACK_SIZE;     // 数字の大きさ
    static const Vector2     NUMBER_POS;    // 数字表示位置
    static const Vector2     HIGH_SCORE_POS;    // 数字表示位置
    static const Vector2     NUMBER_SIZE;   // 数字の大きさ

    // メンバ変数
    //--------------------------------------------------------------------
    _2d::Transform   *m_Back;       // 背景
    DigitWithNumbers *m_Score;      // 数字表示
    DigitWithNumbers *m_HighScore;  // 数字表示

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~DrawScore();
};



};  // namespace result
#endif //  AIR_HACK_RESULT_DRAW_SCORE_H_
/******************************************************************************
|   End of File
*******************************************************************************/