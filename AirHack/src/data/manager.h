/*
# manager.h

- 保存データマネージャ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIRHACK_DATA_MANAGER_H_
#define AIRHACK_DATA_MANAGER_H_

#include <string>
#include <functional>
#include "data.h"
#include "base/tempSingleton.h"

namespace data {
    class Replay;

// データマネージャ
class Manager : public TempSingleton<Manager>
{
public:
    // @brief  : データの取得
    //--------------------------------------------------------------------
    template <typename T> T *Get(void) const;

    // @brief  : セーブ
    //--------------------------------------------------------------------
    void Save(void);

    // @brief  : ロード
    //--------------------------------------------------------------------
    void Load(void);

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempSingleton<Manager>;

    // 定数
    //--------------------------------------------------------------------
    static const std::string SAVE_DATA;

    // メンバ変数
    //--------------------------------------------------------------------
    IData  *m_List[MAX_TYPE]; // データリスト
    Replay *m_Replay;         // リプレイ

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    Manager();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Manager();

    // @brief  : ループ処理
    //--------------------------------------------------------------------
    void ForEach(std::function<void (IData *)>);
};


};  // namespace data
#endif //  AIRHACK_DATA_MANAGER_H_
/******************************************************************************
|   End of File
*******************************************************************************/