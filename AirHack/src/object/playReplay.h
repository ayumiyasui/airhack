/*
# playReplay.h

- リプレイ再生クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_PLAY_REPLAY_H_
#define AIR_HACK_PLAY_REPLAY_H_

#include "base/object.h"
namespace data {
    class Replay;
};

class World;
class Fighter;
namespace play_replay {
    class ICameraState;
};
class PlayReplay : public Object
{
public:
    // @brief  : コンストラクタ
    // @param  : 親オブジェクト
    //         : カメラステート
    //--------------------------------------------------------------------
    PlayReplay(Object *, play_replay::ICameraState *);

    // @brief  : 再生終了か？
    //--------------------------------------------------------------------
    bool IsEnd(void) const;

    // @brief  : 最初から再生
    //--------------------------------------------------------------------
    void Restart(void);

    // @brief  : リプレイデータの取得
    //--------------------------------------------------------------------
    data::Replay *GetData(void) const;

    // @brief  : カメラの変更
    //--------------------------------------------------------------------
    void ChengeCamera(play_replay::ICameraState *);

    // @brief  : プレイヤーの取得
    //--------------------------------------------------------------------
    Fighter *GetPlayer(void) const;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    data::Replay *m_Replay;
    World        *m_World;
    play_replay::ICameraState *m_Camera;        // カメラの更新
    play_replay::ICameraState *m_NextCamera;    // カメラの更新
    Fighter      *m_Player;                     // プレイヤー

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~PlayReplay();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(void) override;
};

namespace play_replay {

// リプレイ用カメラステート
//-----------------------------------------------------------------------------
class ICameraState
{
public:
    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~ICameraState(){};

    // @brief  : 更新
    //--------------------------------------------------------------------
    virtual void Update(const PlayReplay *const) = 0;
};

};    // namespace play_replay


#endif //  AIR_HACK_PLAY_REPLAY_H_
/******************************************************************************
|   End of File
*******************************************************************************/