/*
# time.h

- ゲーム中のタイムクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIRHACK_GAME_UI_TIME_H_
#define AIRHACK_GAME_UI_TIME_H_

#include <string>
#include "base/object.h"
#include "math/vector2.h"
class Game;
class DigitWithNumbers;

namespace _2d {
    class Transform;
};

namespace ui {
class Time : public Object
{
public:
    // @brief  : コンストラクタ
    // @param  : ゲーム時間
    //         : Gameクラス(親クラス)
    //--------------------------------------------------------------------
    Time(const int &,Game *);

    // @brief  : ゲーム終了したか？
    //--------------------------------------------------------------------
    bool GameIsEnd(void) const;

protected:
    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Time();

private:
    // 定数
    //--------------------------------------------------------------------
    static const std::string LABEL_TEXTURE_FILE;
    static const std::string NUMBER_TEXTURE_FILE;
    static const Vector2     LABEL_POS;
    static const Vector2     LABEL_SIZE;
    static const Vector2     NUMBER_MINUTE_POS;
    static const Vector2     NUMBER_SECOND_POS;
    static const Vector2     NUMBER_COMMA_POS;
    static const Vector2     NUMBER_SIZE;

    // メンバ変数
    //--------------------------------------------------------------------
    _2d::Transform   *m_Label;      // 「Time」
    DigitWithNumbers *m_Minute;     // 分
    DigitWithNumbers *m_Second;     // 秒
    DigitWithNumbers *m_Comma;      // コンマ
    int               m_GameTime;   // ゲーム時間

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(void) override;
};


};  // namespace ui
#endif //  AIRHACK_GAME_UI_SCORE_H_
/******************************************************************************
|   End of File
*******************************************************************************/