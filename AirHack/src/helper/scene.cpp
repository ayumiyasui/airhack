/*
# scene.h

- シーンクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "scene.h"

#include "base/color.h"

// 定数
//--------------------------------------------------------------------
const Color    Scene::FADE_COLOR(1.0f,1.0f,1.0f,1.0f);
const unsigned Scene::FADE_TIME = 30;

// @brief  : コンストラクタ
// @param  : 親オブジェクト
//         : フェード
//--------------------------------------------------------------------
Scene::Scene( Object *_parent,Fade *_fade ) :
    Object(_parent,false),
    m_Fade(_fade),
    m_Next(nullptr)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Scene::~Scene()
{
}

// @brief  : 更新
//--------------------------------------------------------------------
void Scene::UpdateScene( void )
{
    if(IsNextScene() && !IsFade())
    {
        this->IsDestroy = true;
        this->IsUpdate = false;
        m_Next->IsUpdate = true;
        m_Fade->FadeIn(FADE_COLOR,FADE_TIME);
    }
}

// @brief  : フェード中か？
//--------------------------------------------------------------------
bool Scene::IsFade( void )
{
    return m_Fade->Status != Fade::FADE_NONE;
}

// @brief  : シーン切り替え中か？
//--------------------------------------------------------------------
bool Scene::IsNextScene( void )
{
    return (m_Next) ? true : false;
}



/******************************************************************************
|   End of File
*******************************************************************************/