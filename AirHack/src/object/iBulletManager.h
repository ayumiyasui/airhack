/*
# bullet.h

- 機銃の弾クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_I_BULLET_MANAGER_H_
#define AIR_HACK_I_BULLET_MANAGER_H_

#include "variable/bitName.h"

class IBulletManager
{
public:
    // メンバ定数
    //--------------------------------------------------------------------
    const material::variable::u16 SHOOTER_ID;

    // @brief  : コンストラクタ
    // @param  : 撃つ人のID
    //--------------------------------------------------------------------
    IBulletManager(material::variable::u16 _id) : SHOOTER_ID(_id){};;

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IBulletManager(){};

    // @brief  : 発射
    // @param  : 撃つ位置
    //         : 撃つ方向ベクトル
    //--------------------------------------------------------------------
    virtual void Shoot(const Vector3 &_pos,const Vector3 &_vec) = 0;
};



#endif //  AIR_HACK_I_BULLET_MANAGER_H_
/******************************************************************************
|   End of File
*******************************************************************************/