/*
# field.cpp

- フィールドクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "field.h"
#include "app/app.h"
#include "math/math.h"
#include "renderer/renderer.h"
#include "renderer/3d/factory.h"
#include "renderer/3d/camera.h"
#include "renderer/3d/transform.h"
#include "renderer/3d/plate.h"
#include "renderer/3d/material.h"
#include "../helper/drawLayerManager.h"
#include "world.h"



// @brief  : コンストラクタ
// @param  : 親クラス
//--------------------------------------------------------------------
Field::Field( World *_parent ) :
    Object(_parent,true)
{
    const float WIDTH_HARF  = _parent->WORLD_SIZE.x * 0.5f;
    const float HEIGHT_HARF = _parent->WORLD_SIZE.z * 0.5f;
    const Quad  SIZE(-WIDTH_HARF,WIDTH_HARF,-HEIGHT_HARF,HEIGHT_HARF);

    _3d::IFactory *factory = App::Instance().Renderer()->Factory3D();

    m_Trans = factory->CreateTransform(DrawLayerManager::Instance().GetCamera(),true);
    _3d::Plate *plate = factory->CreatePlate("data/TEXTURE/cloudField.tga",SIZE,m_Trans,true);
    plate->Mat()->Diffuse  = Color(1.0f,1.0f,1.0f,1.0f);
    plate->Mat()->Ambient  = Color(1.0f,1.0f,1.0f,1.0f);
    plate->Mat()->Specular = Color(1.0f,1.0f,1.0f,0.01f);

    m_Trans->Position->y = -500.0f;
    m_Trans->Rotation    = Vector3(-Math::PI*0.5f,0.0f,0.0f);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Field::~Field()
{
    m_Trans->IsDestroy = true;
}



/******************************************************************************
|   End of File
*******************************************************************************/