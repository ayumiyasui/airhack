/*
# enemyFallDown.h

- 敵落下クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_ENEMY_FALL_DOWN_H_
#define AIR_HACK_ENEMY_FALL_DOWN_H_

#include "../enemy.h"
#include "math/vector3.h"
#include "math/quaternion.h"

namespace enemy {

// 敵落下クラス
//-----------------------------------------------------------------------------
class FallDown : public IAI
{
public:
    // @brief  : コンストラクタ
    // @param  : 戦闘機
    //--------------------------------------------------------------------
    FallDown(const Fighter *const);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~FallDown();

    // @brief  : 更新
    // @param  : 親(enemy)
    //--------------------------------------------------------------------
    void Update(Enemy *) override;

private:
    // 定数
    //--------------------------------------------------------------------
    static const float      ADD_FALL_TIME;  // 落下時間

    // メンバ変数
    //--------------------------------------------------------------------
    float      m_Time;      // 残り時間

};


};  // namespace enemy
#endif //  AIR_HACK_ENEMY_FALL_DOWN_H_
/******************************************************************************
|   End of File
*******************************************************************************/