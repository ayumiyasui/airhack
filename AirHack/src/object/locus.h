/*
# locus.h

- 戦闘機の軌跡クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_LOCUS_H_
#define AIR_HACK_LOCUS_H_

#include "fighter.h"
#include "renderer/vertex/iVertex3dCol.h"
#include "renderer/draw.h"
#include "math/vector3.h"

class Locus
{
public:
    // @brief  : コンストラクタ
    // @param  :  ローカル位置�@
    //         :  ローカル位置�A
    //--------------------------------------------------------------------
    Locus(const Vector3 &, const Vector3 &);

    // @brief  : コンストラクタ
    // @param  : ローカル位置�@
    //         : ローカル位置�A
    //         : 消えるまでの時間(フレーム)
    //         : 色
    //--------------------------------------------------------------------
    Locus(const Vector3 &, const Vector3 &, int, const Color &);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Locus();

    // @brief  : 更新
    // @param  : 親の戦闘機
    //         : 初期アルファ値
    //--------------------------------------------------------------------
    void Update(const Fighter *const, float);

    // @brief  : 更新
    // @param  : 初期変換行列
    //         : 初期アルファ値
    //--------------------------------------------------------------------
    void Update(const Matrix3D &, float);

    // @brief  : リセット
    //--------------------------------------------------------------------
    void Reset(const Fighter *const);

private:
    // メンバ定数
    //--------------------------------------------------------------------
    const Vector3 LOCAL_POS_1;          // 位置
    const Vector3 LOCAL_POS_2;          // 位置
    const int     LIFE;                 // どれ位で消えるのか
    const float   ADD_LIFE;             // 毎フレームライフに足す量

    // メンバ変数
    //--------------------------------------------------------------------
    material::vertex::IVertex3dCol *m_VertexBuffer; // 頂点バッファ
    IDraw                          *m_Draw;         // 描画オブジェクト
    Color                           m_Color;        // 色
};



#endif //  AIR_HACK_LOCUS_H_
/******************************************************************************
|   End of File
*******************************************************************************/