/*
# noneCamera.h

- リプレイ再生クラス - 何もしないカメラ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_PLAY_REPLAY_NONE_CAMERA_H_
#define AIR_HACK_PLAY_REPLAY_NONE_CAMERA_H_

#include "../playReplay.h"

namespace play_replay {

class CameraNone : public ICameraState
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    CameraNone(){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~CameraNone(){};

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(const PlayReplay *const) override{};
};

};    // namespace play_replay


#endif //  AIR_HACK_PLAY_REPLAY_NONE_CAMERA_H_
/******************************************************************************
|   End of File
*******************************************************************************/