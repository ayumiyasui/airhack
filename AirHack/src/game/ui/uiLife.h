/*
# uiLife.h

- 体力表示クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_UI_LIFE_H_
#define AIR_HACK_UI_LIFE_H_

#include <string>
#include "base/object.h"
#include "renderer/2d/transfrom.h"
#include "renderer/2d/sprite2d.h"

class Game;
class Fighter;
namespace ui {
class Life : public Object
{
public:
    // 定数
    //--------------------------------------------------------------------
    static const std::string LABEL_TEX_NAME;
    static const Vector2     LABEL_SIZE;
    static const Vector2     LABEL_POS;
    static const Quad        LABEL_POLYGON;
    static const Vector2     LIFE_SIZE;
    static const Quad        LIFE_MAX_POLYGON;
    static const float       LIFE_LEAP;

    // @brief  : コンストラクタ
    // @param  : 体力を表示する戦闘機
    //         : 親クラス
    //--------------------------------------------------------------------
    Life(const Fighter *const, Game *);

private:
    // メンバ変数
    //--------------------------------------------------------------------
    const Fighter *const m_Fighter;     // 戦闘機
    _2d::Transform     * m_Transform;   // 変形
    _2d::Sprite        * m_Gauge;       // 体力ゲージ

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Life();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(void) override;
};



};  // namespace ui
#endif //  AIR_HACK_UI_LIFE_H_
/******************************************************************************
|   End of File
*******************************************************************************/