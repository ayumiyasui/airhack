/*
# score.h

- スコアデータ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIRHACK_GAME_UI_SCORE_H_
#define AIRHACK_GAME_UI_SCORE_H_

#include <string>
#include "base/object.h"
#include "math/vector2.h"
class Game;
class DigitWithNumbers;

namespace _2d {
    class Transform;
};

namespace ui {
class Score : public Object
{
public:
    // @brief  : コンストラクタ
    // @param  : Gameクラス(親クラス)
    //--------------------------------------------------------------------
    Score(Game *);

    // @brief  : スコアを加算する
    //--------------------------------------------------------------------
    void Add(unsigned int);

protected:
    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Score();

private:
    // 定数
    //--------------------------------------------------------------------
    static const int         NUMBER_DIGIT;
    static const std::string LABEL_TEXTURE_FILE;
    static const std::string NUMBER_TEXTURE_FILE;
    static const Vector2     LABEL_POS;
    static const Vector2     LABEL_SIZE;
    static const Vector2     NUMBER_POS;
    static const Vector2     NUMBER_SIZE;

    // メンバ変数
    //--------------------------------------------------------------------
    unsigned int      m_UserData;    // データ
    _2d::Transform   *m_Label;       // 「Score」
    DigitWithNumbers *m_Number;      // 数字
};


};  // namespace ui
#endif //  AIRHACK_GAME_UI_SCORE_H_
/******************************************************************************
|   End of File
*******************************************************************************/