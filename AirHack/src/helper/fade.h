/*
# fade.h

- フェードクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __FADE_H__
#define __FADE_H__

#include "base/object.h"
#include "renderer/2d/draw2d.h"
#include "base/tempGetter.h"

class Color;

NAMESPACE_2D
    class Transform;
    class Sprite;
END_NAMESPACE_2D

class Fade : public Object
{
public:
    // 列挙体
    //--------------------------------------------------------------------
    enum STATUS
    {
        FADE_NONE = 0,
        FADE_IN,
        FADE_OUT
    };

    // プロパティ
    //--------------------------------------------------------------------
    TempGetter<STATUS> Status;

    // @brief  : コンストラクタ
    // @note   : オブジェクト生成はしないこと
    //--------------------------------------------------------------------
    Fade();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Fade();

    // @brief  : フェードアウト
    // @note   : だんだん隠れる
    //--------------------------------------------------------------------
    void FadeOut( const Color &_col, unsigned _time );

    // @brief  : フェードイン
    // @note   : だんだん現れる
    //--------------------------------------------------------------------
    void FadeIn( const Color &_col, unsigned _time );

private:
    // メンバ変数
    //--------------------------------------------------------------------
    _2d::Transform *m_Trans;      // 座標変換クラス
    _2d::Sprite    *m_Sprite;      // スプライトクラス
    float           m_AddAlpha;    // 足すアルファ
    unsigned        m_Time;        // 時間
    STATUS          m_Status;      // ステータス

    // @brief  : Enableになった時の初期化
    //--------------------------------------------------------------------
    void OnEnable( void ) override;

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update( void ) override;
};



#endif //  __FADE_H__
/******************************************************************************
|   End of File
*******************************************************************************/