/*
# field.h

- フィールドクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef _FIELD_H_
#define _FIELD_H_

#include "base/object.h"
#include "base/tempGetter.h"
#include "renderer/3d/draw3d.h"

class World;
NAMESPACE_3D
    class Transform;
END_NAMESPACE_3D

class Field : public Object
{
public:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //--------------------------------------------------------------------
    Field( World * );

private:
    // メンバ変数
    //--------------------------------------------------------------------
    _3d::Transform *m_Trans;     // 移動

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Field();
};



#endif //  _FIELD_H_
/******************************************************************************
|   End of File
*******************************************************************************/