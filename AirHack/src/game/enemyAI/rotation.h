/*
# rotation.h

- 敵クラス(一定範囲を回転しているだけ)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_ENEMY_AI_ROTATION_H_
#define AIR_HACK_ENEMY_AI_ROTATION_H_

#include "../enemy.h"
#include "math/vector3.h"

namespace enemy {
// 回転するだけ
//-----------------------------------------------------------------------------
class Rotation : public IAI
{
public:
    // @brief  : コンストラクタ
    // @param  : 回転量
    //--------------------------------------------------------------------
    Rotation(const Vector3 &);

    // @brief  : 更新
    // @param  : 親(enemy)
    //--------------------------------------------------------------------
    void Update(Enemy *) override;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    float m_Yaw;     // 回転量(ヨー)
    float m_Pitch;   // 回転量(ピッチ)
    float m_Roll;    // 回転量(ロール)

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Rotation();
};



};  // namespace enemy
#endif //  AIR_HACK_ENEMY_AI_ROTATION_H_
/******************************************************************************
|   End of File
*******************************************************************************/