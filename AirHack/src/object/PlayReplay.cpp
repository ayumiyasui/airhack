/*
# playReplay.cpp

- リプレイ再生クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "playReplay.h"
#include <algorithm>
#include "math/math.h"
#include "../game/enemyManager.h"
#include "../data/manager.h"
#include "../data/dataReplay.h"
#include "world.h"
#include "playFighter.h"
#include "stageConstant.h"

// @brief  : コンストラクタ
// @param  : 親オブジェクト
//--------------------------------------------------------------------
PlayReplay::PlayReplay(Object *_parent, play_replay::ICameraState * _camera) :
    Object(_parent,true),
    m_Replay(data::Manager::Instance().Get<data::Replay>()),
    m_Camera(_camera),
    m_NextCamera(nullptr),
    m_Player(nullptr)
{
    auto world = new World(this);
    auto stage = &StageConstant::Instance();

    // リプレイ戦闘機リストの作成
    const auto &list = m_Replay->GetFighterList();

    // ステージファイター情報リストの取得
    const auto &stage_player = stage->GetPlayer();
    const auto &stage_enemy  = stage->GetEnemy();

    std::for_each(list.begin(),list.end(),[&](data::Replay::FighterData *_data){
        auto setting = _data->GetSetting();
        auto player = std::find_if(stage_player.begin(),stage_player.end(),[setting](Fighter::SETTING _setting){
            return setting.id == _setting.id;
        });
        if(player != stage_player.end())
        {
            m_Player = world->CreateFighter(*player,this);
            new PlayFighter(_data,m_Player,this);
            
        }
        else
        {
            auto enemy = std::find_if(stage_enemy.begin(),stage_enemy.end(),[setting](EnemyManager::ENEMY_DATA _setting){
                return setting.id == _setting.fighter.id;
            });
            if(enemy != stage_enemy.end())
            {
                new PlayFighter(_data,world->CreateFighter(enemy->fighter,this),this);
            }
        }
    });
    
    if(!m_Player)
    {
        Fighter::SETTING set;
        m_Player = world->CreateFighter(set,this);
    }
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
PlayReplay::~PlayReplay()
{
    if(m_NextCamera) delete m_NextCamera;
    delete m_Camera;
}

// @brief  : 更新
//--------------------------------------------------------------------
void PlayReplay::Update(void)
{
    if(m_Replay->IsEndUpdate()) this->IsUpdate = false;
    m_Replay->Update();
    m_Camera->Update(this);
    if(m_NextCamera){delete m_Camera; m_Camera = m_NextCamera; m_NextCamera = nullptr;}
}

// @brief  : 再生終了か？
//--------------------------------------------------------------------
bool PlayReplay::IsEnd(void) const
{
    return m_Replay->IsEndUpdate();
}

// @brief  : 最初から再生
//--------------------------------------------------------------------
void PlayReplay::Restart(void)
{
    m_Replay->ResetTime();
    ForEach([](Node *_node){
        ((Object* )_node)->IsUpdate = true;
    });
}

// @brief  : リプレイデータの取得
//--------------------------------------------------------------------
data::Replay *PlayReplay::GetData(void) const
{
    return m_Replay;
}

// @brief  : カメラの変更
//--------------------------------------------------------------------
void PlayReplay::ChengeCamera(play_replay::ICameraState *_next)
{
    if(m_NextCamera){ delete _next; return; }
    m_NextCamera = _next;
}

// @brief  : プレイヤーの取得
//--------------------------------------------------------------------
Fighter *PlayReplay::GetPlayer(void) const{return m_Player;}




/******************************************************************************
|   End of File
*******************************************************************************/