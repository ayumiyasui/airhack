/*
# enemyManager.h

- 敵マネージャクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_ENEMY_MANAGER_H_
#define AIR_HACK_ENEMY_MANAGER_H_

#include <vector>
#include "base/object.h"
#include "enemy.h"
class Game;
class Player;
class World;
namespace ui {
    class Score;
};

// 敵マネージャクラス
//-----------------------------------------------------------------------------
class EnemyManager : public Object
{
public:
    // 敵AI
    //--------------------------------------------------------------------
    enum TYPE_AI
    {
        TYPE_ROTATION = 0,
        TYPE_EASY,
        TYPE_EASY_SHOOT,

        MAX_TYPE_AI
    };

    // 敵構造体
    //--------------------------------------------------------------------
    struct ENEMY_DATA
    {
        TYPE_AI                 ai_type;    // AIの種類
        int                     score;      // スコア
        Fighter::SETTING        fighter;    // ファイターの設定
    };

    // プロパティ
    //--------------------------------------------------------------------
    int  Count(void) const;             // 現在の敵の数
    bool IsAnnihilation(void) const;    // 全滅したか

    // @brief  : コンストラクタ
    // @param  : 作成する敵リスト
    //         : 親クラス
    //         : プレイヤークラス
    //         : ワールドクラス
    //--------------------------------------------------------------------
    EnemyManager(const std::vector<ENEMY_DATA> &,Game * , Player *, World *, ui::Score *const);

    // @brief  : スコアを足す
    //--------------------------------------------------------------------
    void AddScore(unsigned int);

private:
    // メンバ変数
    //--------------------------------------------------------------------
    ui::Score *const m_Score;

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~EnemyManager();
};



#endif //  AIR_HACK_ENEMY_MANAGER_H_
/******************************************************************************
|   End of File
*******************************************************************************/