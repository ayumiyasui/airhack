/*
# world.h

- ワールドクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_WORLD_H_
#define AIR_HACK_WORLD_H_

#include <list>
#include <functional>
#include "base/object.h"
#include "math/vector3.h"
#include "fighter.h"

class World : public Object
{
public:
    // 定数
    //--------------------------------------------------------------------
    static const Vector3 WORLD_SIZE;       // ワールド範囲

    // @brief  : コンストラクタ
    // @param  : 親クラス
    //--------------------------------------------------------------------
    World(Object *);

    // @brief  : ファイターの作成
    // @param  : 戦闘機の設定
    // @return : 戦闘機
    //--------------------------------------------------------------------
    Fighter *CreateFighter(const Fighter::SETTING &, Object *);

    // @brief  : ファイターの削除
    // @param  : ファイター
    //--------------------------------------------------------------------
    void DeleteFighter(Fighter *);

    // @brief  : リストのファイターをすべて処理する
    //--------------------------------------------------------------------
    void FighterForEach( std::function<void (Fighter *)> );

private:
    // メンバ変数
    //--------------------------------------------------------------------
    std::list<Fighter *> m_FighterList;     // ファイターリスト

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~World();
};



#endif //  AIR_HACK_WORLD_H_
/******************************************************************************
|   End of File
*******************************************************************************/