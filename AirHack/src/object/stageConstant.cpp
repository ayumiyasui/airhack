/*
# stageConstant.cpp

- ステージクラス(定数)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "stageConstant.h"
#include <algorithm>
#include "math/math.h"

// @brief  : コンストラクタ
//--------------------------------------------------------------------
StageConstant::StageConstant()
{
    // プレイヤー
    Fighter::SETTING player;
    {
        player.type = Fighter::TYPE_PLAYER;
        player.id = 0;
        player.InitPos = Vector3(0.0f,5000.0f,5000.0f);
        player.InitRot = Quaternion::IDENTITY;
        m_Player.push_back(player);
    }

    // 敵の作成
    {
        EnemyManager::ENEMY_DATA create_data;

        // ENEMY EASYAI
        create_data.ai_type = EnemyManager::TYPE_EASY;
        create_data.score = 100;
        create_data.fighter.type    = Fighter::TYPE_ENEMY;
        
        create_data.fighter.id      = 1;
        create_data.fighter.InitPos = Vector3(0.0f,5000.0f,0.0f);
        create_data.fighter.InitRot = Quaternion(0.0f,0.0f,0.0f);
        m_Enemy.push_back(create_data);

        create_data.fighter.id      = 2;
        create_data.fighter.InitPos = Vector3(0.0f,7000.0f,2000.0f);
        create_data.fighter.InitRot = Quaternion(0.0f,0.0f,0.0f);
        m_Enemy.push_back(create_data);

        create_data.fighter.id      = 3;
        create_data.fighter.InitPos = Vector3(5000.0f,2000.0f,0.0f);
        create_data.fighter.InitRot = Quaternion(Math::PI*2,0.0f,0.0f);
        m_Enemy.push_back(create_data);

        create_data.fighter.id      = 4;
        create_data.fighter.InitPos = Vector3(-5000.0f,5000.0f,0.0f);
        create_data.fighter.InitRot = Quaternion(-Math::PI*2,0.0f,0.0f);
        m_Enemy.push_back(create_data);

        create_data.fighter.id      = 5;
        create_data.fighter.InitPos = Vector3(100.0f,1000.0f,-2000.0f);
        create_data.fighter.InitRot = Quaternion(0.0f,0.0f,0.0f);
        m_Enemy.push_back(create_data);

        // ENEMY EASYAI SHOOT
        create_data.ai_type = EnemyManager::TYPE_EASY_SHOOT;
        create_data.score = 100;
        create_data.fighter.type    = Fighter::TYPE_ENEMY;
        
        create_data.fighter.id      = 11;
        create_data.fighter.InitPos = Vector3(-1000.0f,5000.0f,2000.0f);
        create_data.fighter.InitRot = Quaternion(0.0f,0.0f,0.0f);
        m_Enemy.push_back(create_data);

        create_data.fighter.id      = 12;
        create_data.fighter.InitPos = Vector3(1000.0f,5000.0f,2000.0f);
        create_data.fighter.InitRot = Quaternion(0.0f,0.0f,0.0f);
        m_Enemy.push_back(create_data);

        // UAV
        create_data.score = 500;
        create_data.fighter.id      = 21;
        create_data.ai_type         = EnemyManager::TYPE_ROTATION;
        create_data.fighter.type    = Fighter::TYPE_UAV;

        create_data.fighter.id      = 22;
        create_data.fighter.InitPos = Vector3(0.0f,1000.0f,0.0f);
        create_data.fighter.InitRot = Quaternion(0.0f,0.0f,0.0f);
        m_Enemy.push_back(create_data);

        create_data.fighter.id      = 23;
        create_data.fighter.InitPos = Vector3(25000.0f,1000.0f,25000.0f);
        create_data.fighter.InitRot = Quaternion(0.0f,Math::PI * 0.5f,0.0f);
        m_Enemy.push_back(create_data);

        create_data.fighter.id      = 24;
        create_data.fighter.InitPos = Vector3(-25000.0f,1000.0f,25000.0f);
        create_data.fighter.InitRot = Quaternion(0.0f,Math::PI,0.0f);
        m_Enemy.push_back(create_data);

        create_data.fighter.id      = 25;
        create_data.fighter.InitPos = Vector3(25000.0f,1000.0f,-25000.0f);
        create_data.fighter.InitRot = Quaternion(0.0f,Math::PI *-0.5f,0.0f);
        m_Enemy.push_back(create_data);
        
        create_data.fighter.id      = 26;
        create_data.fighter.InitPos = Vector3(-25000.0f,1000.0f,-25000.0f);
        create_data.fighter.InitRot = Quaternion(0.0f,-Math::PI,0.0f);
        m_Enemy.push_back(create_data);
}

}

// @brief  : デストラクタ
//--------------------------------------------------------------------
StageConstant::~StageConstant()
{
}
// @brief  : プレイヤー情報の取得
//--------------------------------------------------------------------
const std::vector<Fighter::SETTING> &StageConstant::GetPlayer(void) const
{
    return m_Player;
}

// @brief  : エネミー情報の取得
//--------------------------------------------------------------------
const std::vector<EnemyManager::ENEMY_DATA> &StageConstant::GetEnemy(void) const
{
    return m_Enemy;
}


/******************************************************************************
|   End of File
*******************************************************************************/