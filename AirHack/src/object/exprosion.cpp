/*
# exprosion.cpp

- 爆発

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "exprosion.h"

#include <string>
#include <time.h>
#include <stdlib.h>
#include "../helper/scene.h"
#include "../helper/drawLayerManager.h"
#include "app/app.h"
#include "math/math.h"
#include "math/vector3.h"
#include "math/quaternion.h"
#include "math/matrix3d.h"
#include "math/quad.h"
#include "base/color.h"
#include "renderer/renderer.h"
#include "renderer/3d/factory.h"
#include "renderer/3d/camera.h"
#include "renderer/3d/transLookCamera3d.h"
#include "renderer/3d/plate.h"
#include "renderer/3d/material.h"
#include "renderer/effect/factory.h"

// 爆発
//-----------------------------------------------------------------------------
// 変数
//--------------------------------------------------------------------
const Quad  Exprosion::SIZE(-100.0f,100.0f,-100.0f,100.0f);
const int   Exprosion::MAX_LIFE = 60*2;

// @brief  : コンストラクタ
// @param  : 位置
//         : 大きさ
//         : ベクトル
//         : ライフ
//         : 親クラス(Scene)
//--------------------------------------------------------------------
Exprosion::Exprosion(const Vector3 &_pos, const Vector3 &_vec, int _life, const Color &_col, Object *_parent) :
    Object(_parent),
    m_DestroyTime(_life),
    m_AddColorAlpha(-1.0f/_life),
    m_Vector(_vec)
{
    // カメラの取得
    auto camera = DrawLayerManager::Instance().GetCamera();

    // 作成
    auto renderer = App::Instance().Renderer();
    auto factory = renderer->Factory3D();
    auto effect  = renderer->Effect();

    // 行列
    m_Trans = factory->CreateTransformLookCamera(camera,camera,true);
    m_Trans->Position = _pos;

    // アルファブレンド
    auto alphablend = effect->CreateAlphaBlend(m_Trans);
    alphablend->Set(material::effect::AlphaBlend::TYPE_ADD);

    // プレート
    m_Plate = factory->CreatePlate("data/TEXTURE/circle.tga",SIZE,alphablend,true);
    m_Plate->Mat()->Diffuse = _col;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Exprosion::~Exprosion()
{
    m_Trans->IsDestroy = true;
}

// @brief  : 爆発の生成
// @param  : 発生位置
//         : 色
//--------------------------------------------------------------------
void Exprosion::Create( const Vector3 &_pos, const Color &_col, int _count, float _length, Object *_parent )
{
    // 1.0 ~ 0.0fまでの数字をランダムに生成する関数
    auto randf = [](){ return (float)rand()/(float)RAND_MAX; };

    // ランダムの初期化
    srand(static_cast<unsigned int>(time(NULL)));

    // 最大数分ループ
    for(auto i = 0; i < _count ; ++i)
    {
        // 回転行列の作成
        const Matrix3D rot(Quaternion(Math::PI*2*randf(),Math::PI*2*randf(),Math::PI*2*randf()));

        // ベクトルの作成
        const Vector3 vec = Vector3(_length * randf(),0.0f,0.0f) * rot;

        // 作成
        new Exprosion(_pos,vec,MAX_LIFE,_col,_parent);
    }
}

// @brief  : 更新
//--------------------------------------------------------------------
void Exprosion::Update( void )
{
    // ライフを減らす
    if(--m_DestroyTime <= 0) IsDestroy = true;

    // 色を減らす
    m_Plate->Mat()->Diffuse->a -= m_AddColorAlpha;

    // 移動する
    m_Trans->Position = (Vector3)m_Trans->Position + m_Vector;
}



/******************************************************************************
|   End of File
*******************************************************************************/