/*
# overlook.cpp

- リプレイ再生クラス - 俯瞰カメラ(フィールド全体)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "overlook.h"
#include "../../helper/drawLayerManager.h"

namespace play_replay {

// @brief  : 定数
//--------------------------------------------------------------------
const Vector3 OverLook::EYE(0.0f,8000.0f,0.0f);

// @brief  : コンストラクタ
//--------------------------------------------------------------------
OverLook::OverLook()
{
    // カメラの取得
    auto camera = DrawLayerManager::Instance().GetCamera();
    camera->Eye = EYE;
    camera->At  = Vector3::ZERO;
    camera->Up  = Vector3::Z;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
OverLook::~OverLook()
{
}

// @brief  : 更新
//--------------------------------------------------------------------
void OverLook::Update(const PlayReplay *const _replay)
{
}



};    // namespace play_replay
/******************************************************************************
|   End of File
*******************************************************************************/