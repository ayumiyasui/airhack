/*
# player.h

- プレイヤークラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "player.h"

#include "math/math.h"
#include "../object/fighter.h"
#include "input/inputKeyboard.h"
#include "renderer/3d/factory.h"
#include "playerCamera.h"
#include "ui/map.h"
#include "../data/manager.h"
#include "../data/dataKeyConfig.h"
#include "../object/fighterEffect/se.h"
#include "app/app.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "../helper/drawLayerManager.h"

// 定数
//--------------------------------------------------------------------
const float Player::ADD_VIRTUAL_CONTROL       = 0.2f; // バーチャルコントロールに入力する値
const float Player::AUTO_VIRTUAL_CONTROL_LEAP = 0.1f;

// @brief  : コンストラクタ
// @param  : 親クラス
//--------------------------------------------------------------------
Player::Player( Object *_parent, Fighter *_fighter ) :
    Object(_parent,true),
    m_Fighter(_fighter)
{
    auto &data_manager = data::Manager::Instance();
    m_Camera = new PlayerCameraTPS(this);
    new ui::Map::Icon(Color(0.1f,0.1f,1.0f,1.0f),_fighter);
    m_KeyConfig = data_manager.Get<data::KeyConfig>();
    m_Replay = data_manager.Get<data::Replay>()->AddFighter(_fighter);

    m_Fighter->ChengeEffect(new fighter::SE);

    auto renderer = App::Instance().Renderer();
    auto factory  = renderer->Factory2D();

    m_Focus = factory->CreateTransform(DrawLayerManager::Instance().Get2DLayer(),true);
    m_Focus->Position = Vector2(renderer->Width * 0.5f,renderer->Height*0.5f);

    auto sprite = factory->CreateSprite(Quad(-25.0f,25.0f,-25.0f,25.0f),Quad(0.0f,1.0f,0.0f,1.0f),"data/TEXTURE/focus.tga",m_Focus,true);
    sprite->PolygonColor = Color(0.0f,1.0f,0.0f,1.0f);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Player::~Player()
{
    m_Focus->IsDestroy = true;
}

// プロパティ
//--------------------------------------------------------------------
const Matrix3D   &Player::Matrix(void)   const { return m_Fighter->Matrix(); }
const Quaternion &Player::Rotation(void) const { return m_Fighter->Rotation(); }
const Vector3    &Player::Position(void) const { return m_Fighter->Position(); }

// @brief  : 更新
//--------------------------------------------------------------------
void Player::Update(void)
{
    if(m_KeyConfig->Is(data::KeyConfig::PITCH_DOWN))
    {
        m_VirtualController.x -= ADD_VIRTUAL_CONTROL;
    }
    if(m_KeyConfig->Is(data::KeyConfig::PITCH_UP))
    {
        m_VirtualController.x += ADD_VIRTUAL_CONTROL;
    }
    if(m_KeyConfig->Is(data::KeyConfig::ROLL_LEFT))
    {
        m_VirtualController.z += ADD_VIRTUAL_CONTROL;
    }
    if(m_KeyConfig->Is(data::KeyConfig::ROLL_RIGHT))
    {
        m_VirtualController.z -= ADD_VIRTUAL_CONTROL;
    }
    if(m_KeyConfig->Is(data::KeyConfig::YAW_LEFT))
    {
        m_VirtualController.y -= ADD_VIRTUAL_CONTROL;
    }
    if(m_KeyConfig->Is(data::KeyConfig::YAW_RIGHT))
    {
        m_VirtualController.y += ADD_VIRTUAL_CONTROL;
    }
    if(m_KeyConfig->Is(data::KeyConfig::SHOOT))
    {
        m_Fighter->Shoot();
        m_Replay->SetShoot();
    }
    if(m_KeyConfig->Is(data::KeyConfig::ACCELE))
    {
        m_Fighter->Accele(1.0f);
        m_Replay->SetAccele(1.0f);
    }
    if(m_KeyConfig->Is(data::KeyConfig::BRAKE))
    {
        m_Fighter->Brake(1.0f);
        m_Replay->SetBrake(1.0f);
    }

    if(m_VirtualController.x < -1.0f) m_VirtualController.x = -1.0f;
    if(m_VirtualController.x >  1.0f) m_VirtualController.x =  1.0f;
    m_Fighter->Pitch(m_VirtualController.x);
    m_Replay->SetPitch(m_VirtualController.x);

    if(m_VirtualController.z < -1.0f) m_VirtualController.z = -1.0f;
    if(m_VirtualController.z >  1.0f) m_VirtualController.z =  1.0f;
    m_Fighter->Roll(m_VirtualController.z);
    m_Replay->SetRoll(m_VirtualController.z);

    if(m_VirtualController.y < -1.0f) m_VirtualController.y = -1.0f;
    if(m_VirtualController.y >  1.0f) m_VirtualController.y =  1.0f;
    m_Fighter->Yaw(m_VirtualController.y);
    m_Replay->SetYaw(m_VirtualController.y);

    m_VirtualController = Math::Leap(m_VirtualController,Vector3::ZERO,AUTO_VIRTUAL_CONTROL_LEAP);
}



/******************************************************************************
|   End of File
*******************************************************************************/