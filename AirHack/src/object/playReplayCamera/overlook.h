/*
# overlook.h

- リプレイ再生クラス - 俯瞰カメラ(フィールド全体)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_PLAY_REPLAY_OVERLOOK_H_
#define AIR_HACK_PLAY_REPLAY_OVERLOOK_H_

#include "../playReplay.h"
#include "math/vector3.h"

namespace play_replay {

class OverLook : public ICameraState
{
public:
    // @brief  : 定数
    //--------------------------------------------------------------------
    static const Vector3 EYE;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    OverLook();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~OverLook();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(const PlayReplay *const);
};

};    // namespace play_replay


#endif //  AIR_HACK_PLAY_REPLAY_OVERLOOK_H_
/******************************************************************************
|   End of File
*******************************************************************************/