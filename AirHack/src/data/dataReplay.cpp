/*
# dataReplay.cpp

- リプレイデータ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#define  _CRT_SECURE_NO_WARNINGS
#include "dataReplay.h"
#include <algorithm>
#include <stdio.h>
#include "../object/fighter.h"
#include "variable/f8Normal.h"
using namespace material;

namespace data {

// 定数
//--------------------------------------------------------------------
const std::string Replay::FILE_NAME("data/replay.airhack"); // ファイル名

// @brief  : コンストラクタ
//--------------------------------------------------------------------
Replay::Replay()
{
    Reset();
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Replay::~Replay()
{
    Reset();
}

// @brief  : リセット
//--------------------------------------------------------------------
void Replay::Reset(void)
{
    m_Time = 0;
    m_NowTime = 0;
    std::for_each(m_Fighter.begin(),m_Fighter.end(), [](FighterData *_data) {
        delete _data;
    });
    m_Fighter.clear();
}

// @brief  : ステージデータの保存
// @param  : 初期時間
//--------------------------------------------------------------------
void Replay::SetStage(material::variable::u16 _time)
{
    m_Time = _time;
}

// @brief  : ファイターの保存
// @param  : ファイターデータ
// @return : リプレイ用データ管理クラス
//--------------------------------------------------------------------
Replay::FighterData *const Replay::AddFighter(const Fighter *const _fighter)
{
    FighterData::SETTING setting;
    setting.id   = _fighter->ID;
    setting.type = _fighter->Type;

    auto new_fighter = new FighterData(setting,m_Time,this);
    m_Fighter.push_back(new_fighter);
    return new_fighter;
}

// @brief  : データのセーブ
//--------------------------------------------------------------------
void Replay::Save(void)
{
    FILE *fp = fopen(FILE_NAME.c_str(),"wb");
    if(!fp) return;

    // 時間の保存
    std::fwrite((const void *)&m_Time,sizeof(m_Time),1,fp);

    // ファイター数の保存
    const material::variable::u16 max_fighter = m_Fighter.size();
    std::fwrite((const void *)&max_fighter,sizeof(max_fighter),1,fp);

    // ファイターデータの保存
    std::for_each(m_Fighter.begin(),m_Fighter.end(),[&](FighterData *_data){
        const auto &data = _data->GetSetting();
        std::fwrite((const void *)&data,sizeof(data),1,fp);
    });

    // リプレイデータの保存
    std::for_each(m_Fighter.begin(),m_Fighter.end(),[&](FighterData *_data){
        auto data = _data->GetData();
        std::fwrite((const void *)data,sizeof(Replay::FighterData::FRAME_DATA),m_Time,fp);
    });

    fclose(fp);
}

// @brief  : データのロード
//--------------------------------------------------------------------
void Replay::Load(void)
{
    Reset();

    FILE *fp = fopen(FILE_NAME.c_str(),"rb");
    if(!fp) return;

    // 時間の読み込み
    std::fread((void *)&m_Time,sizeof(m_Time),1,fp);

    // ファイター数の読み込み
    material::variable::u16 max_fighter = 0;
    std::fread((void *)&max_fighter,sizeof(max_fighter),1,fp);

    // ファイターデータの読み込み
    for(int i=0; i < (int)(max_fighter) ; ++i)
    {
        FighterData::SETTING data;
        std::fread((void *)&data,sizeof(data),1,fp);
        m_Fighter.push_back(new FighterData(data,m_Time,this));
    }

    // リプレイデータの読み込み
    std::for_each(m_Fighter.begin(),m_Fighter.end(),[&](FighterData *_data){
        m_NowTime = 0;
        for(material::variable::u16 i=0; i < m_Time ; ++i)
        {
            Replay::FighterData::FRAME_DATA data;
            std::fread((void *)&data,sizeof(data),1,fp);
            _data->Set(data);
            ++m_NowTime;
        }
    });
    m_NowTime = 0;
    fclose(fp);
}

// @brief  : 更新
//--------------------------------------------------------------------
void Replay::Update(void)
{
    ++m_NowTime;
}

// ファイターデータ
//-----------------------------------------------------------------------------
// @brief  : コンストラクタ
// @param  : 初期設定
//         : ステージ時間
//         : リプレイクラス
//--------------------------------------------------------------------
Replay::FighterData::FighterData(const SETTING &_setting,material::variable::u16 _time,const Replay *const _parent) :
    m_Setting(_setting),
    m_Data(new FRAME_DATA[_time]),
    m_Replay(_parent)
{
    std::memset(&m_Data[0],0,sizeof(FRAME_DATA)*_time);
}

Replay::FighterData::~FighterData()
{
    delete[] m_Data;
}

const Replay::FighterData::SETTING &Replay::FighterData::GetSetting(void) const{return m_Setting;}
template <> const Replay::FighterData::FRAME_DATA &Replay::FighterData::Get<Replay::FighterData::FRAME_DATA>(void) const{return m_Data[m_Replay->m_NowTime];}

void Replay::FighterData::Set(const Replay::FighterData::FRAME_DATA &_value){if(!m_Replay->IsEndUpdate()) m_Data[m_Replay->m_NowTime] = _value;}
void Replay::FighterData::SetAccele(variable::f8uNormal _value){if(!m_Replay->IsEndUpdate()) m_Data[m_Replay->m_NowTime].accele = _value;};
void Replay::FighterData::SetBrake(variable::f8uNormal _value){if(!m_Replay->IsEndUpdate()) m_Data[m_Replay->m_NowTime].brake = _value;}
void Replay::FighterData::SetPitch(variable::f8sNormal _value){if(!m_Replay->IsEndUpdate()) m_Data[m_Replay->m_NowTime].pitch = _value;}
void Replay::FighterData::SetRoll(variable::f8sNormal _value){if(!m_Replay->IsEndUpdate()) m_Data[m_Replay->m_NowTime].roll = _value;}
void Replay::FighterData::SetYaw(variable::f8sNormal _value){if(!m_Replay->IsEndUpdate()) m_Data[m_Replay->m_NowTime].yaw = _value;}
void Replay::FighterData::SetFallDown(void){if(!m_Replay->IsEndUpdate()) m_Data[m_Replay->m_NowTime].byte = m_Data[m_Replay->m_NowTime].byte | 0x1;}
void Replay::FighterData::SetShoot(void){if(!m_Replay->IsEndUpdate()) m_Data[m_Replay->m_NowTime].byte = m_Data[m_Replay->m_NowTime].byte | 0x2;}

float Replay::FighterData::GetAccele(void) const{return variable::f8uNormal(m_Data[m_Replay->m_NowTime].accele);}
float Replay::FighterData::GetBrake(void) const{return variable::f8uNormal(m_Data[m_Replay->m_NowTime].brake);}
float Replay::FighterData::GetPitch(void) const{return variable::f8sNormal(m_Data[m_Replay->m_NowTime].pitch);}
float Replay::FighterData::GetRoll(void) const{return variable::f8sNormal(m_Data[m_Replay->m_NowTime].roll);}
float Replay::FighterData::GetYaw(void) const{return variable::f8sNormal(m_Data[m_Replay->m_NowTime].yaw);}
bool Replay::FighterData::GetFallDown(void) const{return (m_Data[m_Replay->m_NowTime].byte & 0x1) ? true : false;}
bool Replay::FighterData::GetShoot(void) const{return (m_Data[m_Replay->m_NowTime].byte & 0x2) ? true : false;}




};  // namespace data
/******************************************************************************
|   End of File
*******************************************************************************/