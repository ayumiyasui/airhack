/*
# number.h

- 数字表示クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __NUMBER_H__
#define __NUMBER_H__

#include <string>
#include "renderer/2d/draw2d.h"
#include "base/color.h"
#include "base/tempGetSet.h"

class Quad;
NAMESPACE_2D
    class Transform;
    class Sprite;
END_NAMESPACE_2D

class Number : public IDraw
{
public:
    // @brief  : コンストラクタ
    // @param  : テクスチャ名
    //         : 数字の大きさ
    //         : 親
    //         : 描画するのか？
    //--------------------------------------------------------------------
    Number( const std::string &_texture_name,
            const Quad &_size,
            IDraw *_parent, bool _is_draw );

    // @brief  : 数字のセット
    // @param  : 数字
    //--------------------------------------------------------------------
    void Set(unsigned);
    Number &operator = (unsigned);

    // @brief  : Transformの取得
    //--------------------------------------------------------------------
    _2d::Transform *Transform(void) const;

    // @brief  : カラーの変更
    //--------------------------------------------------------------------
    void SetColor(const Color &);

private:
    // メンバ変数
    //--------------------------------------------------------------------
    _2d::Transform *m_Trans;
    _2d::Sprite    *m_Sprite;

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Number();
};


class DigitWithNumbers : public IDraw
{
public:
    // メンバ定数
    //--------------------------------------------------------------------
    const unsigned DIGIT;   // 桁数

    // @brief  : コンストラクタ
    // @param  : テクスチャ名
    //         : 数字描画範囲
    //         : 桁数
    //         : 親
    //         : 描画するのか？
    //--------------------------------------------------------------------
    DigitWithNumbers( const std::string &_texture_name,
            const Quad &_div,
            const unsigned &_digit,
            IDraw *_parent, bool _is_draw );

    // @brief  : 数字のセット
    // @param  : 数字
    //--------------------------------------------------------------------
    void Set(unsigned);
    DigitWithNumbers &operator = (unsigned);

    // @brief  : Transformの取得
    //--------------------------------------------------------------------
    _2d::Transform *Transform(void) const;

    // @brief  : カラーの変更
    //--------------------------------------------------------------------
    void SetColor(const Color &);

private:
    // メンバ変数
    //--------------------------------------------------------------------
    _2d::Transform *m_Trans;

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~DigitWithNumbers();
};



#endif //  __NUMBER_H__
/******************************************************************************
|   End of File
*******************************************************************************/