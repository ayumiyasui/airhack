/*
# uiScore.h

- スコアデータ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "score.h"

#include "../game.h"
#include "../../helper/drawLayerManager.h"
#include "../../helper/number.h"
#include "../../data/data.h"
#include "../../data/manager.h"
#include "../../data/score.h"
#include "app/app.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "renderer/2d/transfrom.h"
#include "renderer/2d/sprite2d.h"


namespace ui {
// 定数
//--------------------------------------------------------------------
const int         Score::NUMBER_DIGIT = 5;
const std::string Score::LABEL_TEXTURE_FILE("data/TEXTURE/score.tga");
const std::string Score::NUMBER_TEXTURE_FILE("data/TEXTURE/number.tga");
const Vector2     Score::LABEL_POS(20.0f,20.0f);
const Vector2     Score::LABEL_SIZE(100.0f*0.7f,24.0f*0.7f);
const Vector2     Score::NUMBER_SIZE((LABEL_SIZE.y*(80.0f/128.0f))*NUMBER_DIGIT * 1.4f,LABEL_SIZE.y * 1.4f);
const Vector2     Score::NUMBER_POS( 5.0f + LABEL_SIZE.x+LABEL_POS.x+NUMBER_SIZE.x*0.5f, LABEL_POS.y+NUMBER_SIZE.y*0.4f);

// @brief  : コンストラクタ
// @param  : Gameクラス(親クラス)
//--------------------------------------------------------------------
Score::Score(Game *_parent) :
    Object(_parent,true),
    m_UserData(0),
    m_Label(nullptr),
    m_Number(nullptr)
{
    auto factory  = App::Instance().Renderer()->Factory2D();
    auto layer = DrawLayerManager::Instance().Get2DLayer();
    const Color col_ui(0.0f,1.0f,0.0f,0.8f);

    // ラベルの作成
    m_Label = factory->CreateTransform(layer,true);
    m_Label->Position = LABEL_POS;
    auto sprite = factory->CreateSprite(
        Quad(0.0f,LABEL_SIZE.x,0.0f,LABEL_SIZE.y),
        Quad((1.0f/LABEL_SIZE.x)*0.5f, 1.0f+(1.0f/LABEL_SIZE.x)*0.5f, (1.0f/LABEL_SIZE.y)*0.5f, 1.0f+(1.0f/LABEL_SIZE.y)*0.5f),
        LABEL_TEXTURE_FILE,
        m_Label,true);
    sprite->PolygonColor = col_ui;

    // 数字の作成
    m_Number = new DigitWithNumbers(NUMBER_TEXTURE_FILE,
        Quad(-NUMBER_SIZE.x*0.5f,NUMBER_SIZE.x*0.5f,-NUMBER_SIZE.y*0.5f,NUMBER_SIZE.y*0.5f),
        NUMBER_DIGIT,
        layer,true);
    m_Number->Transform()->Position = NUMBER_POS;
    m_Number->SetColor(col_ui);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Score::~Score()
{
    m_Label->IsDestroy  = true;
    m_Number->IsDestroy = true;
}

// @brief  : スコアを加算する
//--------------------------------------------------------------------
void Score::Add(unsigned int _add)
{
    m_UserData += _add;
    m_Number->Set(m_UserData);

    // スコアデータの保存
    auto score = data::Manager::Instance().Get<data::Score>();
    score->SetUserData(m_UserData);
}



};  // namespace ui
/******************************************************************************
|   End of File
*******************************************************************************/