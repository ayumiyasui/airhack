/*
# exprosion.h

- 爆発

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef _EXPROSION_H_
#define _EXPROSION_H_

#include <string>
#include "base/object.h"
#include "renderer/3d/draw3d.h"
#include "math/vector3.h"

class Quad;
class Color;
class Scene;
namespace _3d {
    class TransLookCamera;
    class Plate;
};

// 爆発
//-----------------------------------------------------------------------------
class Exprosion : public Object
{
public:
    // @brief  : 爆発の生成
    // @param  : 発生位置
    //         : 爆発の色
    //         : スプライト数
    //         : 範囲
    //         : シーン
    //--------------------------------------------------------------------
    static void Create(const Vector3 &_pos, const Color &_col, int _count, float _length, Object *_scene);

private:
    // 変数
    //--------------------------------------------------------------------
    static const Quad        SIZE;              // プレートのサイズ
    static const int         MAX_LIFE;          // 最大ライフ

    // メンバ変数
    //--------------------------------------------------------------------
    _3d::TransLookCamera *m_Trans;         // 変換行列
    _3d::Plate           *m_Plate;         // プレート
    int                   m_DestroyTime;   // 爆発が消えるまでの時間
    float                 m_AddColorAlpha; // アルファを足す量
    Vector3               m_Vector;        // ベクトル

    // @brief  : コンストラクタ
    // @param  : 位置
    //         : ベクトル
    //         : ライフ
    //         : カラー
    //         : 親クラス(Scene)
    //--------------------------------------------------------------------
    Exprosion(const Vector3 &,const Vector3 &, int, const Color &, Object *);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Exprosion();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update( void ) override;
};



#endif //  _EXPROSION_H_
/******************************************************************************
|   End of File
*******************************************************************************/