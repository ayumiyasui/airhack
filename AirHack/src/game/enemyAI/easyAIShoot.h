/*
# easyAIShoot.h

- 敵クラス(簡易AI - 弾を発射する敵)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_ENEMY_EASY_AI_SHOOT_H_
#define AIR_HACK_ENEMY_EASY_AI_SHOOT_H_

#include "../enemy.h"

class Player;

namespace enemy {
// 簡易AI
//-----------------------------------------------------------------------------
class EasyAIShoot : public IAI
{
public:
    // @brief  : コンストラクタ
    // @param  : プレイヤーのポインタ
    //--------------------------------------------------------------------
    EasyAIShoot(const Player *);

    // @brief  : 更新
    // @param  : 親(enemy)
    //--------------------------------------------------------------------
    void Update(Enemy *) override;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    const Player *m_Player; // プレイヤへのポインタ

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~EasyAIShoot();

    // @brief  : 追いかける
    // @param  : 自分の位置
    //         : 相手の位置
    //         : 操縦する戦闘機
    //--------------------------------------------------------------------
    void Chase(const Vector3 &, const Vector3 &_chase_pos, Fighter * const, data::Replay::FighterData *const);

    // @brief  : プレイヤーに向かって撃つ
    //--------------------------------------------------------------------
    void Shoot(Enemy * _enemy);
};


};  // namespace enemy
#endif //  AIR_HACK_ENEMY_EASY_AI_SHOOT_H_
/******************************************************************************
|   End of File
*******************************************************************************/