/*
# fighterEffectSe.cpp

- 戦闘機演出クラス(SE)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "se.h"
#include "app/app.h"

namespace fighter {

// @brief  : コンストラクタ
//--------------------------------------------------------------------
SE::SE()
{
    auto app   = &App::Instance();
    auto audio = app->Audio();

    m_Shoot = audio->LoadSE("data/SOUND/shoot.wav");
    m_HitBullet = audio->LoadSE("data/SOUND/hit.wav");
    m_FallDown = audio->LoadSE("data/SOUND/falldown.wav");
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
SE::~SE()
{
    m_Shoot->Stop();
    m_HitBullet->Stop();
    m_FallDown->Stop();
}

// @brief  : 機銃を撃つ
//--------------------------------------------------------------------
void SE::Shoot(void)
{
    m_Shoot->Play();
}

// @brief  : 機銃に当てられた
//--------------------------------------------------------------------
void SE::HitBullet(void)
{
    m_HitBullet->Play();
}

// @brief  : やられる
//--------------------------------------------------------------------
void SE::FallDown(void)
{
    m_FallDown->Play();
}



};  // namespace fighter
/******************************************************************************
|   End of File
*******************************************************************************/