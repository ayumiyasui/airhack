/*
# stageConstant.h

- ステージクラス(定数)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_STAGE_CONSTANT_H_
#define AIR_HACK_STAGE_CONSTANT_H_

#include "iStage.h"
#include "base/tempSingleton.h"

class StageConstant : public IStage, public TempSingleton<StageConstant>
{
public:
    // @brief  : 作戦時間の取得
    //--------------------------------------------------------------------
    material::variable::u16 GetTime(void) const override{return 60*60;};
    
    // @brief  : プレイヤー情報の取得
    //--------------------------------------------------------------------
    const std::vector<Fighter::SETTING> &GetPlayer(void) const override;

    // @brief  : エネミー情報の取得
    //--------------------------------------------------------------------
    const std::vector<EnemyManager::ENEMY_DATA> &GetEnemy(void) const override;

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempSingleton<StageConstant>;

    // メンバ変数
    //--------------------------------------------------------------------
    std::vector<Fighter::SETTING>         m_Player;  // プレイヤーデータ
    std::vector<EnemyManager::ENEMY_DATA> m_Enemy;   // 敵データ

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    StageConstant();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~StageConstant();
};

#endif // AIR_HACK_STAGE_CONSTANT_H_
/******************************************************************************
|   End of File
*******************************************************************************/