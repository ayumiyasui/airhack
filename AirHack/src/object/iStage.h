/*
# iStage.h

- ステージクラス(抽象)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_I_STAGE_H_
#define AIR_HACK_I_STAGE_H_

#include <vector>
#include "variable/bitName.h"
#include "math/vector3.h"
#include "fighter.h"
#include "../game/enemyManager.h"

class IStage
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    IStage(){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~IStage(){};

    // @brief  : 作戦時間の取得
    //--------------------------------------------------------------------
    virtual material::variable::u16 GetTime(void) const = 0;
    
    // @brief  : プレイヤー情報の取得
    //--------------------------------------------------------------------
    virtual const std::vector<Fighter::SETTING> &GetPlayer(void) const = 0;

    // @brief  : エネミー情報の取得
    //--------------------------------------------------------------------
    virtual const std::vector<EnemyManager::ENEMY_DATA> &GetEnemy(void) const = 0;
};

#endif // AIR_HACK_I_STAGE_H_
/******************************************************************************
|   End of File
*******************************************************************************/