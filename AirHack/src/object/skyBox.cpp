/*
# sky.cpp

- 空クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "skyBox.h"

#include "app/app.h"
#include "math/math.h"
#include "renderer/renderer.h"
#include "renderer/3d/factory.h"
#include "renderer/3d/camera.h"
#include "renderer/3d/plate.h"
#include "renderer/3d/transform.h"
#include "renderer/3d/transLookCamera3d.h"
#include "renderer/3d/directionalLight.h"
#include "renderer/effect/factory.h"
#include "../helper/drawLayerManager.h"
#include "world.h"


// @brief  : コンストラクタ
// @param  : 親クラス
//--------------------------------------------------------------------
SkyBox::SkyBox( World *_parent ) :
    Object(_parent)
{
    auto       *app      = &App::Instance();
    auto   *renderer = app->Renderer();
    auto *factory = renderer->Factory3D();
    auto *effect  = renderer->Effect();
    auto *camera = DrawLayerManager::Instance().GetCamera();

    // 背景色の設定
    app->Renderer()->BackColor = Color(39,176,255);

    // ライトの設定
    m_Light  = factory->CreateDirLight(camera,true);
    m_Light->Diffuse   = Color(1.0f,1.0f,1.0f,1.0f);
    m_Light->Ambient   = Color(1.0f,1.0f,1.0f,0.4f);
    m_Light->Specular  = Color(1.0f,1.0f,1.0f,0.4f);
    m_Light->Direction = Vector3(0.0f,-0.75f,-0.25f);

    // 太陽の設定
    {
        m_Sun = factory->CreateTransformLookCamera(camera,camera,true);
        m_Sun->Position = -Vector3(m_Light->Direction) * 100000.0f;
        auto alpha_add  = effect->CreateAlphaBlend(m_Sun);
        alpha_add->Set(material::effect::AlphaBlend::TYPE_ADD);
        auto sun_plate = factory->CreatePlate("data/TEXTURE/circle.tga",Quad(-2000.0f,2000.0f,-2000.0f,2000.0f),alpha_add,true);
        auto sun_plate_mat = sun_plate->Mat();
        sun_plate_mat->Emissive = Color(1.0f,1.0f,0.1f,1.0f);
    }

    // 雲の設定
    {
        m_Cloud = factory->CreateTransform(camera,true);
        factory->CreatePMDNoBone("data/MODEL/cloudRound.pmd",m_Cloud,true);
    }
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
SkyBox::~SkyBox()
{
    m_Sun->IsDestroy   = true;
    m_Cloud->IsDestroy = true;
    m_Light->IsDestroy = true;
}



/******************************************************************************
|   End of File
*******************************************************************************/