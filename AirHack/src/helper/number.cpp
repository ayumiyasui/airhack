/*
# number.cpp

- 数字表示クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "number.h"

#include <math.h>
#include "app/app.h"
#include "renderer/renderer.h"
#include "renderer/2d/factory.h"
#include "renderer/2d/transfrom.h"
#include "renderer/2d/sprite2d.h"

// @brief  : コンストラクタ
// @param  : テクスチャ名
//         : 親
//         : 描画するのか？
//--------------------------------------------------------------------
Number::Number( const std::string &_texture_name,
                const Quad &_size,
                IDraw *_parent, bool _is_draw ) :
    IDraw(_parent,_is_draw)
{
    auto factory = App::Instance().Renderer()->Factory2D();

    const Quad TEX_SIZE(0.0f,0.1f,0.0f,1.0f);

    m_Trans  = factory->CreateTransform(this,true);
    m_Sprite = factory->CreateSprite(_size,TEX_SIZE,_texture_name,m_Trans,true);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Number::~Number()
{
}

// @brief  : 数字のセット
// @param  : 数字
//--------------------------------------------------------------------
void Number::Set(unsigned _value)
{
    assert(_value < 10);
    const float max_x = _value * 0.1f;
    const Quad TEX_SIZE (
        max_x,
        max_x + 0.1f,
        0.0f,
        1.0f
    );

    m_Sprite->SetTextureQuad(TEX_SIZE);
}

Number &Number::operator = (unsigned _value)
{
    Set(_value);
    return *this;
}

// @brief  : Transformの取得
//--------------------------------------------------------------------
_2d::Transform *Number::Transform(void) const
{
    return m_Trans;
}

// @brief  : カラーの変更
//--------------------------------------------------------------------
void Number::SetColor(const Color &_col)
{
    m_Sprite->PolygonColor = _col;
}



// @brief  : コンストラクタ
// @param  : テクスチャ名
//         : 数字描画範囲
//         : 桁数
//         : 親
//         : 描画するのか？
//--------------------------------------------------------------------
DigitWithNumbers::DigitWithNumbers( const std::string &_texture_name,
        const Quad &_div,
        const unsigned &_digit,
        IDraw *_parent, bool _is_draw ) :
    IDraw(_parent,_is_draw),
    DIGIT(_digit)
{
    _2d::IFactory *factory = App::Instance().Renderer()->Factory2D();
    m_Trans  = factory->CreateTransform(this,true);

    const float width  = (_div.right  - _div.left) / DIGIT;
    const Quad size(-width*0.5f,width*0.5f,_div.top,_div.bottom);
    Vector2 pos(_div.left + width*0.5f,0.0f);

    for(unsigned i = 0; i < DIGIT ; ++i)
    {
        Number *p = new Number(_texture_name,size,m_Trans,true);
        p->Transform()->Position = pos;
        pos.x += width;
    }
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
DigitWithNumbers::~DigitWithNumbers()
{
}

// @brief  : 数字のセット
// @param  : 数字
//--------------------------------------------------------------------
void DigitWithNumbers::Set(unsigned _value)
{
    // 桁数毎に表示する数字を計算
    unsigned mask = 1;
    for(unsigned i = 0 ; i < DIGIT ; ++i)
    {
        mask *= 10;
    }

    // カンスト表示
    if( _value > mask )
    {
        _value = mask-1;
    }

    // 桁数に数字を入れる
    m_Trans->ForEach([&](Node *_node){
        Number *num = (Number *)_node;
        mask /= 10;
        num->Set( _value / mask );
        _value %= mask;
    });
}

DigitWithNumbers &DigitWithNumbers::operator = (unsigned _value)
{
    Set(_value);
    return *this;
}

// @brief  : Transformの取得
//--------------------------------------------------------------------
_2d::Transform *DigitWithNumbers::Transform(void) const
{
    return m_Trans;
}

// @brief  : カラーの変更
//--------------------------------------------------------------------
void DigitWithNumbers::SetColor(const Color &_col)
{
    m_Trans->ForEach([&](Node *_node){
        Number *num = (Number *)_node;
        num->SetColor(_col);
    });
}



/******************************************************************************
|   End of File
*******************************************************************************/