/*
# game.h

- ゲームシーンクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_GAME_SCENE_H_
#define AIR_HACK_GAME_SCENE_H_

#include <string>
#include "../helper/scene.h"
#include "renderer/2d/draw2d.h"
#include "../data/dataReplay.h"
#include "audio/iAudio.h"

class Player;
class EnemyManager;
class SkyBox;
class Quad;
class Vector2;

namespace data {
    class KeyConfig;
};
namespace ui {
    class Time;
};

NAMESPACE_2D
    class Transform;
END_NAMESPACE_2D

// ゲームクラス
//-----------------------------------------------------------------------------
class Game : public Scene
{
public:
    // @brief  : コンストラクタ
    // @param  : 親オブジェクト
    //         : フェード
    //--------------------------------------------------------------------
    Game( Object *, Fade * );

protected:
    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Game();

private:
    // 内部クラス - ポーズクラス
    //--------------------------------------------------------------------
    class Pause : public Object
    {
    public:
        bool m_GameIsDestroy;
        Pause(Game *);

    private:
        enum MENU
        {
            CONTINUE = 0,
            RETRY,
            BACK_TITLE,

            MAX_MENU
        };

        static const std::string BACK_TEXTURE_FILE;
        static const std::string CURSOR_TEXTURE_FILE;
        static const Vector2 WINDOW_POS;
        static const float   CURSOR_POS_Y[MAX_MENU];
        static const float   CURSOR_POS_X;
        static const Quad    BG_SIZE;
        static const Quad    CURSOR_SIZE;

        Game           *m_Game;        // ゲームシーン
        int             m_CursorNum;   // カーソル
        bool            m_SceneIsNext;
        _2d::Transform *m_Back;        // 背景
        _2d::Transform *m_Cursor;      // カーソル
        data::KeyConfig *m_KeyConfig;     // キーコンフィング
        material::IAudio::ISource *m_SECursor;    // カーソル音

        ~Pause();
        void Update(void) override;
        void OnEnable(void) override;
        void OnDisable(void) override;
    };

    // メンバ変数
    //--------------------------------------------------------------------
    Pause *m_Pause;       // ポーズクラス
    bool   m_IsPausing;   // ポーズになったばかりか
    bool   m_PauseIsDestory;    // ポーズが消えたか
    EnemyManager *m_Enemy;
    ui::Time     *m_Time;
    data::Replay *m_Replay;
    data::KeyConfig *m_KeyConfig;
    material::IAudio::ISource *m_BGM;   // BGM

    // @brief  : Enableになった時の初期化
    //--------------------------------------------------------------------
    void OnEnable( void ) override;

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update( void ) override;
};



#endif //  AIR_HACK_GAME_SCENE_H_
/******************************************************************************
|   End of File
*******************************************************************************/