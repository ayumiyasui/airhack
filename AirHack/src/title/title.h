/*
# title.h

- タイトルシーンクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef AIR_HACK_TITLE_SCENE_H_
#define AIR_HACK_TITLE_SCENE_H_

#include <string>
#include "../helper/scene.h"
#include "renderer/2d/draw2d.h"
#include "audio/iAudio.h"

namespace _2d {
    class Transform;
};

namespace data {
    class KeyConfig;
};

class PlayReplay;
class Title : public Scene
{
public:
    // @brief  : コンストラクタ
    // @param  : 親オブジェクト
    //         : フェード
    //--------------------------------------------------------------------
    Title( Object *,Fade * );

protected:
    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Title();

private:
    // メンバ変数
    //--------------------------------------------------------------------
    data::KeyConfig *m_KeyConfig;
    PlayReplay      *m_PlayReplay;
    material::IAudio::ISource *m_BGM;   // BGM

    // @brief  : Enableになった時の初期化
    //--------------------------------------------------------------------
    void OnEnable( void ) override;

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update( void ) override;
};



#endif //  AIR_HACK_TITLE_SCENE_H_
/******************************************************************************
|   End of File
*******************************************************************************/