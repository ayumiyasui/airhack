/*
# enemy.h

- 敵クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef _ENEMY_H_
#define _ENEMY_H_

#include "base/object.h"
#include "renderer/3d/draw3d.h"
#include "../data/dataReplay.h"
#include "ui/map.h"

class Fighter;
class Player;
namespace enemy {
    class IAI;
};

// 敵クラス
//-----------------------------------------------------------------------------
class Enemy : public Object
{
public:
    // プロパティ
    //--------------------------------------------------------------------
    Fighter                   *const Airframe(void) const;
    data::Replay::FighterData *const Replay(void) const;

    // @brief  : コンストラクタ
    // @param  : 最初のState
    //         : 選択した機体
    //         : スコア
    //         : オブジェクトクラス
    //--------------------------------------------------------------------
    Enemy(enemy::IAI *, Fighter *,int, Object *);

    // @brief  : 落下
    //--------------------------------------------------------------------
    void FallDown(void);

private:
    // メンバ変数
    //--------------------------------------------------------------------
    unsigned int        m_Score;        // 足すスコア数
    Fighter            *m_Fighter;      // 操縦する飛行機
    enemy::IAI         *m_AI;           // AI
    bool                m_IsFallDown;   // 落下
    data::Replay::FighterData       *m_Replay;       // リプレイ
    ui::Map::Icon      *m_Icon;     // 自分を示すアイコン

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Enemy();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update(void) override;

    // @brief  : 削除されるフラグが立ったとき
    //--------------------------------------------------------------------
    void OnDestory(void) override;
};


namespace enemy {

// 敵AIクラス
//-----------------------------------------------------------------------------
class IAI
{
public:
    // @brief  : 更新
    // @param  : 親(enemy)
    //--------------------------------------------------------------------
    virtual void Update( Enemy * ) = 0;

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IAI(){};

protected:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    IAI(){};
};


};  // namespace enemy

#endif //  _ENEMY_H_
/******************************************************************************
|   End of File
*******************************************************************************/